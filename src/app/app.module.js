"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var angular2_http_file_upload_1 = require("angular2-http-file-upload");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var app_component_1 = require("./app.component");
var cinema_component_1 = require("./components/cinema/cinema.component");
var cinemas_component_1 = require("./components/cinemas/cinemas.component");
var movie_component_1 = require("./components/movie/movie.component");
var movies_component_1 = require("./components/movies/movies.component");
var shows_component_1 = require("./components/shows/shows.component");
/* Admin modules */
var admin_cinemas_component_1 = require("./admin/admin.cinema/admin.cinemas.component");
var admin_addCinema_component_1 = require("./admin/admin.cinema/admin.addCinema.component");
var admin_editCinema_component_1 = require("./admin/admin.cinema/admin.editCinema.component");
var admin_main_component_1 = require("./admin/admin.main/admin.main.component");
var admin_navbar_component_1 = require("./admin/admin.navbar/admin.navbar.component");
var admin_rooms_component_1 = require("./admin/admin.rooms/admin.rooms.component");
var admin_movies_component_1 = require("./admin/admin.movie/admin.movies.component");
var admin_editMovie_component_1 = require("./admin/admin.movie/admin.editMovie.component");
var admin_addMovie_component_1 = require("./admin/admin.movie/admin.addMovie.component");
var admin_shows_component_1 = require("./admin/admin.show/admin.shows.component");
var admin_editShow_component_1 = require("./admin/admin.show/admin.editShow.component");
var admin_addShow_component_1 = require("./admin/admin.show/admin.addShow.component");
var admin_cassa_component_1 = require("./admin/admin.cassa/admin.cassa.component");
var admin_reservation_component_1 = require("./admin/admin.reservation/admin.reservation.component");
var admin_editReservation_component_1 = require("./admin/admin.reservation/admin.editReservation.component");
/**/
var app_routing_module_1 = require("./app-routing.module");
var frontpage_component_1 = require("./components/frontpage/frontpage.component");
var header_component_1 = require("./components/header/header.component");
var footer_component_1 = require("./components/footer/footer.component");
var reservation_component_1 = require("./components/reservation/reservation.component");
var reservation_booking_component_1 = require("./components/reservation_booking/reservation_booking.component");
var steps_component_1 = require("./components/steps/steps.component");
var booking_component_1 = require("./components/booking/booking.component");
var seatreservation_component_1 = require("./components/seatreservation/seatreservation.component");
var verify_component_1 = require("./components/verify/verify.component");
var confirmation_component_1 = require("./components/confirmation/confirmation.component");
var finish_component_1 = require("./components/finish/finish.component");
var forgotMailVerify_component_1 = require("./components/forgotMailVerify/forgotMailVerify.component");
var cinema_service_1 = require("./services/cinema.service");
var movie_service_1 = require("./services/movie.service");
var show_service_1 = require("./services/show.service");
var jquery_service_1 = require("./services/jquery.service");
var data_service_1 = require("./services/data.service");
var window_service_1 = require("./services/window.service");
var reservation_service_1 = require("./services/reservation.service");
var user_service_1 = require("./services/user.service");
var row_service_1 = require("./services/row.service");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [http_1.HttpModule, platform_browser_1.BrowserModule, app_routing_module_1.AppRoutingModule, forms_1.FormsModule],
        providers: [jquery_service_1.JqueryService, data_service_1.DataService, window_service_1.WindowRef, window_service_1.Scroll, movie_service_1.MovieService, cinema_service_1.CinemaService, show_service_1.ShowService,
            user_service_1.UserService, reservation_service_1.ReservationService, row_service_1.RowService, angular2_http_file_upload_1.Uploader],
        declarations: [app_component_1.AppComponent, cinema_component_1.CinemaComponent, cinemas_component_1.CinemasComponent, movie_component_1.MovieComponent, movies_component_1.MoviesComponent,
            shows_component_1.ShowsComponent, frontpage_component_1.FrontpageComponent, header_component_1.HeaderComponent, footer_component_1.FooterComponent, reservation_component_1.ReservationComponent,
            reservation_booking_component_1.ReservationBookingComponent, steps_component_1.StepsComponent, booking_component_1.BookingComponent, seatreservation_component_1.SeatReservationComponent, verify_component_1.VerifyComponent,
            confirmation_component_1.ConfirmationComponent, admin_cinemas_component_1.AdminCinemasComponent, admin_editCinema_component_1.AdminEditCinemaComponent, admin_main_component_1.AdminMainComponent, admin_addCinema_component_1.AdminAddCinemaComponent,
            admin_navbar_component_1.AdminNavbarComponent, admin_rooms_component_1.AdminRoomsComponent, admin_movies_component_1.AdminMoviesComponent, admin_editMovie_component_1.AdminEditMovieComponent,
            admin_addMovie_component_1.AdminAddMovieComponent, admin_shows_component_1.AdminShowsComponent, admin_editShow_component_1.AdminEditShowComponent, admin_addShow_component_1.AdminAddShowComponent,
            admin_cassa_component_1.AdminCassaComponent, admin_reservation_component_1.AdminReservationComponent, admin_editReservation_component_1.AdminEditReservationComponent, finish_component_1.FinishComponent, forgotMailVerify_component_1.ForgotMailVerifyComponent],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map