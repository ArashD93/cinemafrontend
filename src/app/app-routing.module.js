"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var cinema_component_1 = require("./components/cinema/cinema.component");
var cinemas_component_1 = require("./components/cinemas/cinemas.component");
var movie_component_1 = require("./components/movie/movie.component");
var movies_component_1 = require("./components/movies/movies.component");
var shows_component_1 = require("./components/shows/shows.component");
var admin_cinemas_component_1 = require("./admin/admin.cinema/admin.cinemas.component");
var admin_editCinema_component_1 = require("./admin/admin.cinema/admin.editCinema.component");
var admin_main_component_1 = require("./admin/admin.main/admin.main.component");
var frontpage_component_1 = require("./components/frontpage/frontpage.component");
var header_component_1 = require("./components/header/header.component");
var footer_component_1 = require("./components/footer/footer.component");
var admin_addCinema_component_1 = require("./admin/admin.cinema/admin.addCinema.component");
var admin_rooms_component_1 = require("./admin/admin.rooms/admin.rooms.component");
var admin_movies_component_1 = require("./admin/admin.movie/admin.movies.component");
var admin_editMovie_component_1 = require("./admin/admin.movie/admin.editMovie.component");
var admin_addMovie_component_1 = require("./admin/admin.movie/admin.addMovie.component");
var admin_shows_component_1 = require("./admin/admin.show/admin.shows.component");
var admin_addShow_component_1 = require("./admin/admin.show/admin.addShow.component");
var admin_editShow_component_1 = require("./admin/admin.show/admin.editShow.component");
var admin_cassa_component_1 = require("./admin/admin.cassa/admin.cassa.component");
var admin_reservation_component_1 = require("./admin/admin.reservation/admin.reservation.component");
var admin_editReservation_component_1 = require("./admin/admin.reservation/admin.editReservation.component");
var reservation_booking_component_1 = require("./components/reservation_booking/reservation_booking.component");
var booking_component_1 = require("./components/booking/booking.component");
var verify_component_1 = require("./components/verify/verify.component");
var forgotMailVerify_component_1 = require("./components/forgotMailVerify/forgotMailVerify.component");
var routes = [
    { path: '', pathMatch: 'full', component: frontpage_component_1.FrontpageComponent },
    { path: 'frontpage', component: frontpage_component_1.FrontpageComponent },
    { path: 'header', component: header_component_1.HeaderComponent },
    { path: 'footer', component: footer_component_1.FooterComponent },
    { path: 'cinema/:cinema_id', component: cinema_component_1.CinemaComponent },
    { path: 'cinemas', component: cinemas_component_1.CinemasComponent },
    { path: 'movie/:id', component: movie_component_1.MovieComponent },
    { path: 'movies', component: movies_component_1.MoviesComponent },
    { path: 'shows', component: shows_component_1.ShowsComponent },
    /* Admin Urls */
    { path: 'admin', redirectTo: 'admin/main' },
    { path: 'admin/main', component: admin_main_component_1.AdminMainComponent },
    { path: 'admin/cinemas', component: admin_cinemas_component_1.AdminCinemasComponent },
    { path: 'admin/cinemas/add', component: admin_addCinema_component_1.AdminAddCinemaComponent },
    { path: 'admin/cinemas/:id', component: admin_editCinema_component_1.AdminEditCinemaComponent },
    //{ path: 'admin/showRooms/add', component: AdminRoomsComponent },
    { path: 'admin/showRooms/:id', component: admin_rooms_component_1.AdminRoomsComponent, data: { mode: 'show' } },
    { path: 'admin/addRoom/:id', component: admin_rooms_component_1.AdminRoomsComponent, data: { mode: 'add' } },
    { path: 'admin/movies', component: admin_movies_component_1.AdminMoviesComponent },
    { path: 'admin/movies/add', component: admin_addMovie_component_1.AdminAddMovieComponent },
    { path: 'admin/movies/:id', component: admin_editMovie_component_1.AdminEditMovieComponent },
    { path: 'admin/shows', component: admin_shows_component_1.AdminShowsComponent },
    { path: 'admin/shows/add', component: admin_addShow_component_1.AdminAddShowComponent },
    { path: 'admin/shows/:id', component: admin_editShow_component_1.AdminEditShowComponent },
    { path: 'admin/cassa', component: admin_cassa_component_1.AdminCassaComponent },
    { path: 'admin/reservation', component: admin_reservation_component_1.AdminReservationComponent },
    { path: 'admin/reservation/:id', component: admin_editReservation_component_1.AdminEditReservationComponent },
    { path: 'reservation_booking', component: reservation_booking_component_1.ReservationBookingComponent },
    { path: 'booking/:id', component: booking_component_1.BookingComponent },
    { path: 'verify/:userid/:hash', component: verify_component_1.VerifyComponent },
    { path: 'users/settings/modifyPassword/:userid/:hash', component: forgotMailVerify_component_1.ForgotMailVerifyComponent }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forRoot(routes)],
        exports: [router_1.RouterModule]
    })
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map