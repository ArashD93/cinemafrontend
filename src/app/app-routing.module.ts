import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CinemaComponent }  from './components/cinema/cinema.component';
import { CinemasComponent }  from './components/cinemas/cinemas.component';
import { MovieComponent }  from './components/movie/movie.component';
import { MoviesComponent }  from './components/movies/movies.component';
import { ShowsComponent }  from './components/shows/shows.component';
import { AdminCinemasComponent }  from './admin/admin.cinema/admin.cinemas.component';
import { AdminEditCinemaComponent }  from './admin/admin.cinema/admin.editCinema.component';
import { AdminMainComponent }  from './admin/admin.main/admin.main.component';
import { FrontpageComponent }  from './components/frontpage/frontpage.component';
import { HeaderComponent }  from './components/header/header.component';
import { FooterComponent }  from './components/footer/footer.component';
import { AdminAddCinemaComponent } from './admin/admin.cinema/admin.addCinema.component';
import { AdminRoomsComponent } from './admin/admin.rooms/admin.rooms.component';
import { AdminMoviesComponent }  from './admin/admin.movie/admin.movies.component';
import { AdminEditMovieComponent }  from './admin/admin.movie/admin.editMovie.component';
import { AdminAddMovieComponent }  from './admin/admin.movie/admin.addMovie.component';
import { AdminShowsComponent } from './admin/admin.show/admin.shows.component';
import { AdminAddShowComponent } from './admin/admin.show/admin.addShow.component';
import { AdminEditShowComponent } from './admin/admin.show/admin.editShow.component';
import { AdminCassaComponent } from './admin/admin.cassa/admin.cassa.component';
import { AdminReservationComponent } from './admin/admin.reservation/admin.reservation.component';
import { AdminEditReservationComponent } from './admin/admin.reservation/admin.editReservation.component';
import { ReservationComponent } from './components/reservation/reservation.component';
import { ReservationBookingComponent } from './components/reservation_booking/reservation_booking.component';
import { BookingComponent } from './components/booking/booking.component';
import { VerifyComponent } from './components/verify/verify.component';
import { ForgotMailVerifyComponent } from './components/forgotMailVerify/forgotMailVerify.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: FrontpageComponent },
  { path: 'frontpage', component: FrontpageComponent },
  { path: 'header',  component: HeaderComponent },
  { path: 'footer',  component: FooterComponent },
  { path: 'cinema/:cinema_id',  component: CinemaComponent },
  { path: 'cinemas', component: CinemasComponent },
  { path: 'movie/:id', component: MovieComponent },
  { path: 'movies',  component: MoviesComponent },
  { path: 'shows', component: ShowsComponent },
  /* Admin Urls */
  { path: 'admin', redirectTo: 'admin/main' },
  { path: 'admin/main', component: AdminMainComponent },
  { path: 'admin/cinemas', component: AdminCinemasComponent },
  { path: 'admin/cinemas/add', component: AdminAddCinemaComponent },
  { path: 'admin/cinemas/:id', component: AdminEditCinemaComponent },
  //{ path: 'admin/showRooms/add', component: AdminRoomsComponent },
  { path: 'admin/showRooms/:id', component: AdminRoomsComponent, data: { mode : 'show' } },
  { path: 'admin/addRoom/:id', component: AdminRoomsComponent, data: { mode : 'add' } },
  { path: 'admin/movies', component: AdminMoviesComponent },
  { path: 'admin/movies/add', component: AdminAddMovieComponent },
  { path: 'admin/movies/:id', component: AdminEditMovieComponent },
  { path: 'admin/shows', component: AdminShowsComponent },
  { path: 'admin/shows/add', component: AdminAddShowComponent },
  { path: 'admin/shows/:id', component: AdminEditShowComponent},
  { path: 'admin/cassa', component: AdminCassaComponent },
  { path: 'admin/reservation', component: AdminReservationComponent },
  { path: 'admin/reservation/:id', component: AdminEditReservationComponent },
  { path: 'reservation_booking', component: ReservationBookingComponent},
  { path: 'booking/:id', component: BookingComponent },
  { path: 'verify/:userid/:hash', component: VerifyComponent },
  { path: 'users/settings/modifyPassword/:userid/:hash', component: ForgotMailVerifyComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})


export class AppRoutingModule {}  