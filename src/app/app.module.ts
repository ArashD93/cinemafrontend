import { NgModule }      from '@angular/core';
import { FormsModule} from '@angular/forms';
import { Uploader } from 'angular2-http-file-upload';

import { BrowserModule } from '@angular/platform-browser';
import { RouterModule }   from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppComponent }  from './app.component';
import { CinemaComponent }  from './components/cinema/cinema.component';
import { CinemasComponent }  from './components/cinemas/cinemas.component';
import { MovieComponent }  from './components/movie/movie.component';
import { MoviesComponent }  from './components/movies/movies.component';
import { ShowsComponent }  from './components/shows/shows.component';
/* Admin modules */
import { AdminCinemasComponent }  from './admin/admin.cinema/admin.cinemas.component';
import { AdminAddCinemaComponent }  from './admin/admin.cinema/admin.addCinema.component';
import { AdminEditCinemaComponent }  from './admin/admin.cinema/admin.editCinema.component';
import { AdminMainComponent }  from './admin/admin.main/admin.main.component';
import { AdminNavbarComponent }  from './admin/admin.navbar/admin.navbar.component';
import { AdminRoomsComponent }  from './admin/admin.rooms/admin.rooms.component';
import { AdminMoviesComponent }  from './admin/admin.movie/admin.movies.component';
import { AdminEditMovieComponent }  from './admin/admin.movie/admin.editMovie.component';
import { AdminAddMovieComponent }  from './admin/admin.movie/admin.addMovie.component';
import { AdminShowsComponent } from './admin/admin.show/admin.shows.component';
import { AdminEditShowComponent } from './admin/admin.show/admin.editShow.component';
import { AdminAddShowComponent } from './admin/admin.show/admin.addShow.component';
import { AdminCassaComponent } from './admin/admin.cassa/admin.cassa.component';
import { AdminReservationComponent } from './admin/admin.reservation/admin.reservation.component';
import { AdminEditReservationComponent } from './admin/admin.reservation/admin.editReservation.component';

/**/
import { AppRoutingModule } from './app-routing.module'
import { FrontpageComponent }  from './components/frontpage/frontpage.component';
import { HeaderComponent }  from './components/header/header.component';
import { FooterComponent }  from './components/footer/footer.component';
import { ReservationComponent } from './components/reservation/reservation.component';
import { ReservationBookingComponent } from './components/reservation_booking/reservation_booking.component';
import { StepsComponent } from './components/steps/steps.component';
import { BookingComponent } from './components/booking/booking.component';
import { SeatReservationComponent } from './components/seatreservation/seatreservation.component';
import { VerifyComponent } from './components/verify/verify.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { FinishComponent } from './components/finish/finish.component';
import { ForgotMailVerifyComponent } from './components/forgotMailVerify/forgotMailVerify.component';


import { CinemaService } from './services/cinema.service';
import { MovieService }  from './services/movie.service';
import { ShowService } from './services/show.service';
import { JqueryService }  from './services/jquery.service';
import { DataService } from './services/data.service';
import { WindowRef, Scroll } from './services/window.service';
import { ReservationService } from './services/reservation.service';
import { UserService } from './services/user.service';
import { RowService } from './services/row.service';

@NgModule({
  imports:      [ HttpModule, BrowserModule, AppRoutingModule, FormsModule ],
  providers:    [ JqueryService, DataService, WindowRef, Scroll, MovieService, CinemaService, ShowService, 
                 UserService, ReservationService, RowService, Uploader ],
  declarations: [ AppComponent, CinemaComponent, CinemasComponent, MovieComponent, MoviesComponent, 
        					ShowsComponent, FrontpageComponent, HeaderComponent, FooterComponent, ReservationComponent,
        					ReservationBookingComponent, StepsComponent, BookingComponent, SeatReservationComponent, VerifyComponent, 
                  ConfirmationComponent, AdminCinemasComponent, AdminEditCinemaComponent, AdminMainComponent, AdminAddCinemaComponent, 
                  AdminNavbarComponent, AdminRoomsComponent, AdminMoviesComponent, AdminEditMovieComponent, 
                  AdminAddMovieComponent, AdminShowsComponent, AdminEditShowComponent, AdminAddShowComponent,
                  AdminCassaComponent, AdminReservationComponent, AdminEditReservationComponent, FinishComponent, ForgotMailVerifyComponent   ],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }
