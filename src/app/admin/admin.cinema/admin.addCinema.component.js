"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var jquery_service_1 = require("../../services/jquery.service");
var cinema_service_1 = require("../../services/cinema.service");
var AdminAddCinemaComponent = (function () {
    function AdminAddCinemaComponent(cinemaService, route, jqueryService) {
        this.cinemaService = cinemaService;
        this.route = route;
        this.jqueryService = jqueryService;
        this.id = this.route.snapshot.params['id'];
    }
    ;
    AdminAddCinemaComponent.prototype.addCinema = function () {
        var _this = this;
        var newCinema = {
            name: this.name,
            description: this.description,
            address: this.adress,
            gps_lat: this.gps_lat,
            gps_lng: this.gps_lng,
            email: this.email,
            phone: this.phone,
            base_ticket_price: this.base_ticket_price
        };
        this.cinemaService.createCinema(newCinema).then(function (newCinema) {
            _this.jqueryService.showAddCinemaPopup();
        });
    };
    AdminAddCinemaComponent.prototype.hideAddCinemaPopup = function () {
        this.jqueryService.hideAddCinemaPopup();
    };
    return AdminAddCinemaComponent;
}());
AdminAddCinemaComponent = __decorate([
    core_1.Component({
        selector: 'admin-addCinema',
        templateUrl: './admin.addCinema.component.html',
        providers: [cinema_service_1.CinemaService],
    }),
    __metadata("design:paramtypes", [cinema_service_1.CinemaService, router_1.ActivatedRoute, jquery_service_1.JqueryService])
], AdminAddCinemaComponent);
exports.AdminAddCinemaComponent = AdminAddCinemaComponent;
//# sourceMappingURL=admin.addCinema.component.js.map