"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var cinema_service_1 = require("../../services/cinema.service");
var AdminCinemasComponent = (function () {
    function AdminCinemasComponent(cinemaService, route) {
        this.cinemaService = cinemaService;
        this.route = route;
        this.cinemas = [];
        this.id = this.route.snapshot.params['id'];
    }
    AdminCinemasComponent.prototype.getCinemasFromService = function () {
        var _this = this;
        this.cinemaService.getCinemas()
            .then(function (cinemas) {
            _this.cinemas = cinemas;
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    AdminCinemasComponent.prototype.ngOnInit = function () {
        this.getCinemasFromService();
    };
    AdminCinemasComponent.prototype.deleteThisCinema = function (id) {
        var _this = this;
        this.cinemaService.deleteCinema(id)
            .then(function () {
            _this.cinemas.splice(id, 1);
        });
    };
    return AdminCinemasComponent;
}());
AdminCinemasComponent = __decorate([
    core_1.Component({
        selector: 'admin-cinemas',
        templateUrl: './admin.cinemas.component.html',
        providers: [cinema_service_1.CinemaService],
    }),
    __metadata("design:paramtypes", [cinema_service_1.CinemaService, router_1.ActivatedRoute])
], AdminCinemasComponent);
exports.AdminCinemasComponent = AdminCinemasComponent;
//# sourceMappingURL=admin.cinemas.component.js.map