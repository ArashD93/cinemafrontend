"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var cinema_interface_1 = require("../../common/cinema.interface");
var cinema_service_1 = require("../../services/cinema.service");
var jquery_service_1 = require("../../services/jquery.service");
var AdminEditCinemaComponent = (function () {
    function AdminEditCinemaComponent(cinemaService, route, jqueryService) {
        this.cinemaService = cinemaService;
        this.route = route;
        this.jqueryService = jqueryService;
        this.cinema = cinema_interface_1.emptyCinema;
        this.id = this.route.snapshot.params['id'];
        this.rooms = [];
    }
    AdminEditCinemaComponent.prototype.getCinemaAndRooms = function () {
        var _this = this;
        var cinemaPromise = this.cinemaService.getCinema(this.id)
            .then(function (cinema) {
            _this.cinema = cinema;
            _this.cinema.rooms = [];
        });
        var roomsPromise = this.cinemaService.getRoomsByCinemaId(this.id)
            .then(function (rooms) {
            _this.rooms = rooms;
        });
        Promise.all([cinemaPromise, roomsPromise]).then(function () {
            _this.cinema.rooms = _this.rooms;
        }).catch(function (error) {
            console.log('Error to get cinema and rooms:' + error);
        });
    };
    AdminEditCinemaComponent.prototype.ngOnInit = function () {
        this.getCinemaAndRooms();
    };
    AdminEditCinemaComponent.prototype.editCinemaData = function (cinema) {
        var _this = this;
        this.cinemaService.editCinema(this.cinema).then(function () {
            _this.jqueryService.growlSuccess("Cinema", "Edit successful");
        });
    };
    return AdminEditCinemaComponent;
}());
AdminEditCinemaComponent = __decorate([
    core_1.Component({
        selector: 'admin-editCinema',
        templateUrl: './admin.editCinema.component.html',
        providers: [cinema_service_1.CinemaService],
    }),
    __metadata("design:paramtypes", [cinema_service_1.CinemaService, router_1.ActivatedRoute, jquery_service_1.JqueryService])
], AdminEditCinemaComponent);
exports.AdminEditCinemaComponent = AdminEditCinemaComponent;
//# sourceMappingURL=admin.editCinema.component.js.map