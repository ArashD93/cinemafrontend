import { Component, OnInit } from '@angular/core';
import { ActivatedRoute }   from '@angular/router';

import { Cinema, emptyCinema } from '../../common/cinema.interface';
import { Room } from '../../common/room.interface';
import { Row } from '../../common/row.interface';

import { CinemaService } from '../../services/cinema.service';
import { JqueryService } from '../../services/jquery.service';

@Component({
  selector: 'admin-editCinema',
  templateUrl: './admin.editCinema.component.html',
  providers: [CinemaService],
})


export class AdminEditCinemaComponent implements OnInit{
	
	cinema : Cinema;
	id:number;
	rooms : Room[];
	cinema_id : number;

	constructor(private cinemaService : CinemaService, private route : ActivatedRoute, private jqueryService : JqueryService) {
		this.cinema = emptyCinema;
		this.id = this.route.snapshot.params['id'];
		this.rooms = [];
	}

	getCinemaAndRooms() : void{
		let cinemaPromise = this.cinemaService.getCinema(this.id)
		.then( (cinema : Cinema) => {
			this.cinema = cinema;
			this.cinema.rooms = [];
		} );
		let roomsPromise = this.cinemaService.getRoomsByCinemaId(this.id)
		.then( (rooms: Room[]) => {
			this.rooms = rooms;
		});
		Promise.all([cinemaPromise, roomsPromise]).then( () => {
			this.cinema.rooms = this.rooms;
		}).catch( (error)=>{
			console.log('Error to get cinema and rooms:' + error);	
		});
	}

	ngOnInit() : void {
		this.getCinemaAndRooms();
	}

	private editCinemaData(cinema : Cinema) : void {
		this.cinemaService.editCinema(this.cinema).then(() => {
			this.jqueryService.growlSuccess("Cinema", "Edit successful");
		});
	}

	
}

