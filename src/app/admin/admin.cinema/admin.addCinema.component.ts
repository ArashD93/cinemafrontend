import { Component, OnInit } from '@angular/core';	
import { ActivatedRoute } from '@angular/router';
import { Cinema, emptyCinema } from '../../common/cinema.interface';
import { Room } from '../../common/room.interface';
import { Row } from '../../common/row.interface';
import { JqueryService } from '../../services/jquery.service'

import { CinemaService } from '../../services/cinema.service';

@Component({
  selector: 'admin-addCinema',
  templateUrl: './admin.addCinema.component.html',
  providers: [CinemaService],
})

export class AdminAddCinemaComponent {
		newCinema:Cinema;
		id:number;
		private name: string;
		private description: string;
		private adress:string;
		private gps_lat:number;
		private gps_lng:number;
		private phone:string;
		private base_ticket_price:number;
		private email:string;
		private showPopup:boolean;


	constructor(private cinemaService:CinemaService,private route:ActivatedRoute, private jqueryService:JqueryService){
		this.id = this.route.snapshot.params['id'];
		

	};
	
	addCinema() : void {
		let newCinema:Cinema = {
			name: this.name,
			description: this.description,
			address: this.adress,
			gps_lat: this.gps_lat,
			gps_lng: this.gps_lng,
			email: this.email,
			phone: this.phone,
			base_ticket_price: this.base_ticket_price
		};
		this.cinemaService.createCinema(newCinema).then( newCinema =>{
			this.jqueryService.showAddCinemaPopup();

		});
	}
	hideAddCinemaPopup(): void{
		this.jqueryService.hideAddCinemaPopup();
	}
	
}