import { Component } from '@angular/core';
import { ActivatedRoute }   from '@angular/router';

import { Cinema, emptyCinema } from '../../common/cinema.interface';
import { Room } from '../../common/room.interface';
import { Row } from '../../common/row.interface';

import { CinemaService } from '../../services/cinema.service';

@Component({
  selector: 'admin-cinemas',
  templateUrl: './admin.cinemas.component.html',
  providers: [CinemaService],
})


export class AdminCinemasComponent {
	cinemas: Cinema[];
	id : number;

	constructor(private cinemaService : CinemaService, private route : ActivatedRoute) {
		this.cinemas = [];
		this.id = this.route.snapshot.params['id'];
	}

	getCinemasFromService() : void {
		this.cinemaService.getCinemas()
		.then( (cinemas : Cinema[]) => {
			this.cinemas = cinemas;
		} )
		.catch( response => {
			console.log(response);
		});
	}

	ngOnInit(): void {
		this.getCinemasFromService();
  	}

	deleteThisCinema(id : number) : void {
		this.cinemaService.deleteCinema(id)
		.then(() => {
			this.cinemas.splice(id, 1);
		});
	}
	
}
