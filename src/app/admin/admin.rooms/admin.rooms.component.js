"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var room_interface_1 = require("../../common/room.interface");
var cinema_service_1 = require("../../services/cinema.service");
var jquery_service_1 = require("../../services/jquery.service");
var AdminRoomsComponent = (function () {
    function AdminRoomsComponent(cinemaService, route, jqueryService, router) {
        this.cinemaService = cinemaService;
        this.route = route;
        this.jqueryService = jqueryService;
        this.router = router;
        this.id = route.snapshot.params['cinema_id'];
        this.roomId = route.snapshot.params['id'];
        this.room_id = 0;
        this.room = room_interface_1.emptyRoom;
        this.rows = [];
        this.addMode = false;
    }
    /*-------------------a terem és a hozzá tartozó sorok lekérése--------------------*/
    AdminRoomsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getRoomAndRows();
        this.route.data.subscribe(function (data) {
            if (data['mode'] == 'add') {
                _this.addMode = true;
            }
            else {
                _this.addMode = false;
            }
        });
    };
    ;
    AdminRoomsComponent.prototype.getRoomAndRows = function () {
        var _this = this;
        //console.log('roomid' + this.roomId)
        if (this.addMode) {
            var roomPromise = this.cinemaService.getRoomByRoomId(+this.roomId)
                .then(function (room) {
                _this.room = room;
                _this.room.rows = [];
            });
            var rowsPromise = this.cinemaService.getRowsByRoomId(+this.roomId)
                .then(function (rows) {
                _this.rows = rows;
            });
            Promise.all([roomPromise, rowsPromise]).then(function () {
                _this.room.rows = _this.rows;
            })
                .catch(function (error) {
                console.log('Error to get room:' + error);
                _this.jqueryService.growlError("Getting Room", "Can't get rooms and rows");
            });
        }
        else {
            this.room = room_interface_1.emptyRoom;
        }
    };
    /*-----------------------teremhez tartozó függvények---------------------*/
    AdminRoomsComponent.prototype.addOrEditRoom = function () {
        var _this = this;
        if (this.addMode) {
            var newRoom = {
                name: this.room.name,
                cinema_id: this.roomId,
                rows: this.rows,
            };
            var addRoomPromise = this.cinemaService.addRoom(this.roomId, newRoom).then(function (newRoom) {
                _this.room = newRoom;
                _this.room.rows = [];
                _this.jqueryService.growlSuccess("Add Room", "New room added");
            })
                .catch(function (error) {
                console.log('Error to add room:' + error);
                _this.jqueryService.growlError("Add Room", "Can't add a room");
            });
        }
        else {
            this.cinemaService.editRoom(this.room).then(function (room) {
                _this.jqueryService.growlSuccess("Edit room", "Update successful");
            })
                .catch(function (error) {
                console.log('Error to update room:' + error);
                _this.jqueryService.growlError("Edit Room", "Can't update the room");
            });
        }
    };
    /*
    addNewRoom() : void {
        let newRoom:Room = {
            name:this.name,
            cinema_id: this.id,
        };
        this.cinemaService.addRoom(this.id, newRoom).then( newRoom => {
            this.jqueryService.growlSuccess("Add Room", "New room added");
        })
        .catch((error)=>{
            console.log('Error to add room:' + error);
            this.jqueryService.growlError("Add Room", "Can't add a room");
        });
    }

    editRoomData(room:Room) : void {
        this.cinemaService.editRoom(this.room).then(() => {
            this.jqueryService.growlSuccess("Edit room", "Update successful");
        })
        .catch((error)=>{
            console.log('Error to update room:' + error);
            this.jqueryService.growlError("Edit Room", "Can't update the room");
        });
    }
    */
    AdminRoomsComponent.prototype.deleteThisRoom = function (roomId) {
        var _this = this;
        this.cinemaService.deleteRoom(roomId)
            .then(function () {
            _this.jqueryService.growlSuccess("Delete room", "Delete successful");
            _this.router.navigateByUrl('cinemas');
        })
            .catch(function (error) {
            console.log('Error to delete room:' + error);
            _this.jqueryService.growlError("Delete Room", "Can't delete a room");
        });
    };
    /*------------------sorokhoz tartozó függvények--------------------*/
    AdminRoomsComponent.prototype.addNewRow = function () {
        var _this = this;
        var newRow = {
            room_id: +this.roomId,
            seats: 0,
            is_sofa: false
        };
        this.cinemaService.addRow(this.room_id, newRow).then(function (newRow) {
            _this.jqueryService.growlSuccess("Add Row", "New row added");
        })
            .catch(function (error) {
            console.log('Error to add row:' + error);
            _this.jqueryService.growlError("Add Row", "Can't add a row");
        });
    };
    AdminRoomsComponent.prototype.editThisRow = function (row) {
        var _this = this;
        this.cinemaService.editRow(this.rows).then(function () {
            _this.jqueryService.growlSuccess("Edit row", "Update successful");
        })
            .catch(function (error) {
            console.log('Error to update room:' + error);
            _this.jqueryService.growlError("Edit Row", "Can't update the row");
        });
    };
    AdminRoomsComponent.prototype.deleteThisRow = function (rowId) {
        var _this = this;
        this.cinemaService.deleteRow(rowId)
            .then(function () {
            _this.jqueryService.growlSuccess("Delete row", "Delete successful");
        })
            .catch(function (error) {
            console.log('Error to delete room:' + error);
            _this.jqueryService.growlError("Delete row", "Can't delete a row");
        });
    };
    AdminRoomsComponent.prototype.arrayOfLength = function (len) {
        var li = parseInt(len);
        return new Array(isNaN(li) ? 0 : li);
    };
    return AdminRoomsComponent;
}());
AdminRoomsComponent = __decorate([
    core_1.Component({
        selector: 'admin-rooms',
        templateUrl: './admin.rooms.component.html',
        providers: [cinema_service_1.CinemaService],
    }),
    __metadata("design:paramtypes", [cinema_service_1.CinemaService, router_1.ActivatedRoute,
        jquery_service_1.JqueryService, router_1.Router])
], AdminRoomsComponent);
exports.AdminRoomsComponent = AdminRoomsComponent;
//# sourceMappingURL=admin.rooms.component.js.map