import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router }   from '@angular/router';

import { Cinema} from '../../common/cinema.interface';
import { Room, emptyRoom } from '../../common/room.interface';
import { Row } from '../../common/row.interface';

import { CinemaService } from '../../services/cinema.service';
import { JqueryService } from '../../services/jquery.service';

@Component({
  selector: 'admin-rooms',
  templateUrl: './admin.rooms.component.html',
  providers: [CinemaService],
})


export class AdminRoomsComponent implements OnInit {
	id:number;
	roomId:number;
	room_id:number;
	addMode : boolean;
	
	//Room változók
	room : Room;
	newRoom : Room;
	private name:string;
	private cinema_id : number;
	
	//Row változók 
	row: Row;
	rows : Row[];
	newRow : Row;
	private is_sofa: boolean;

	constructor(private cinemaService : CinemaService, private route : ActivatedRoute, 
				private jqueryService: JqueryService, private router: Router) {
		this.id = route.snapshot.params['cinema_id'];
		this.roomId = route.snapshot.params['id'];
		this.room_id = 0;
		this.room = emptyRoom;
		this.rows = [];
		this.addMode = false;
	}

	/*-------------------a terem és a hozzá tartozó sorok lekérése--------------------*/
	ngOnInit() : void {
		this.getRoomAndRows();
		this.route.data.subscribe( (data) => {
			if (data['mode'] == 'add') {
				this.addMode = true;
			} else {
				this.addMode = false;
			}
		} );
	};

	getRoomAndRows() : void {
		//console.log('roomid' + this.roomId)
		if(this.addMode){
			let roomPromise = this.cinemaService.getRoomByRoomId(+this.roomId)
				.then((room : Room) => { 
					this.room = room;
					this.room.rows = [];
			});
			let rowsPromise = this.cinemaService.getRowsByRoomId(+this.roomId)
				.then((rows : Row[]) =>{
					this.rows = rows;
			});
			Promise.all([roomPromise, rowsPromise]).then(() =>{
				this.room.rows = this.rows;
			})
			.catch((error)=>{
				console.log('Error to get room:' + error);
				this.jqueryService.growlError("Getting Room", "Can't get rooms and rows");
			});
		} else {
			this.room = emptyRoom;
		}
	}

	/*-----------------------teremhez tartozó függvények---------------------*/
	addOrEditRoom() : void {
		if(this.addMode){
			let newRoom:Room = {
				name : this.room.name,
				cinema_id: this.roomId,
				rows : this.rows,
			};
			let addRoomPromise = this.cinemaService.addRoom(this.roomId, newRoom).then( newRoom => {
				this.room = newRoom;
				this.room.rows = [];
				this.jqueryService.growlSuccess("Add Room", "New room added");
			})
			.catch((error)=>{
				console.log('Error to add room:' + error);
				this.jqueryService.growlError("Add Room", "Can't add a room");
			});
		}else{
			this.cinemaService.editRoom(this.room).then((room) => {
				this.jqueryService.growlSuccess("Edit room", "Update successful");
			})
			.catch((error)=>{
				console.log('Error to update room:' + error);
				this.jqueryService.growlError("Edit Room", "Can't update the room");
			});
		}
	}

	/*
	addNewRoom() : void {
		let newRoom:Room = {
			name:this.name,
			cinema_id: this.id,
		};
		this.cinemaService.addRoom(this.id, newRoom).then( newRoom => {
			this.jqueryService.growlSuccess("Add Room", "New room added");
		})
		.catch((error)=>{
			console.log('Error to add room:' + error);
			this.jqueryService.growlError("Add Room", "Can't add a room");
		});
	}

	editRoomData(room:Room) : void {
		this.cinemaService.editRoom(this.room).then(() => {
			this.jqueryService.growlSuccess("Edit room", "Update successful");
		})
		.catch((error)=>{
			console.log('Error to update room:' + error);
			this.jqueryService.growlError("Edit Room", "Can't update the room");
		});
	}
	*/

	deleteThisRoom(roomId:number): void {
		this.cinemaService.deleteRoom(roomId)
		.then(() => {
			this.jqueryService.growlSuccess("Delete room", "Delete successful");
			this.router.navigateByUrl('cinemas');
		})
		.catch((error)=>{
			console.log('Error to delete room:' + error);
			this.jqueryService.growlError("Delete Room", "Can't delete a room");
		});
	}

	/*------------------sorokhoz tartozó függvények--------------------*/
	addNewRow() : void {
		let newRow : Row = {
			room_id: +this.roomId,
			seats: 0,
			is_sofa: false
		};
		this.cinemaService.addRow(this.room_id, newRow).then(newRow => {
			this.jqueryService.growlSuccess("Add Row", "New row added");
		})
		.catch((error)=>{
			console.log('Error to add row:' + error);
			this.jqueryService.growlError("Add Row", "Can't add a row");
		});
	}

	editThisRow(row:Row) : void {
		this.cinemaService.editRow(this.rows).then(() =>{
			this.jqueryService.growlSuccess("Edit row", "Update successful");
		})
		.catch((error)=>{
			console.log('Error to update room:' + error);
			this.jqueryService.growlError("Edit Row", "Can't update the row");
		});
	}

	deleteThisRow(rowId:number): void {
		this.cinemaService.deleteRow(rowId)
		.then(() => {
			this.jqueryService.growlSuccess("Delete row", "Delete successful");
		})
		.catch((error)=>{
			console.log('Error to delete room:' + error);
			this.jqueryService.growlError("Delete row", "Can't delete a row");
		});
	}
	

	private arrayOfLength(len : string) : any[] {
		let li = parseInt(len);
		return new Array( isNaN(li) ? 0 : li );
	}
	
}
	