"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var cinema_interface_1 = require("../../common/cinema.interface");
var show_service_1 = require("../../services/show.service");
var cinema_service_1 = require("../../services/cinema.service");
var movie_service_1 = require("../../services/movie.service");
var AdminShowsComponent = (function () {
    function AdminShowsComponent(movieService, cinemaService, route, showServices) {
        this.movieService = movieService;
        this.cinemaService = cinemaService;
        this.route = route;
        this.showServices = showServices;
        this.cinemas = [];
        this.movie = [];
        this.cinema = cinema_interface_1.emptyCinema;
        this.date = new Date();
        this.shows = [];
        this.rooms = [];
        this.selectedCinemaId = 0;
    }
    AdminShowsComponent.prototype.getCinemasFromService = function () {
        var _this = this;
        this.cinemaService.getCinemas()
            .then(function (cinemas) {
            _this.cinemas = cinemas;
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    AdminShowsComponent.prototype.ngOnInit = function () {
        this.getCinemasFromService();
    };
    AdminShowsComponent.prototype.moviesByCinema = function (c) {
        this.cinema = c;
        this.reloadMovies();
    };
    AdminShowsComponent.prototype.reloadMovies = function () {
        var _this = this;
        this.movieService.getMoviesByCinemaId(this.cinema.id)
            .then(function (movie) {
            _this.movie = movie;
        }).catch(function (error) {
            console.log("RÁBASZTÁL, már megint :D még mindig ÁÁÁNDÍÍFÁJND TE VAK HÜLYE!");
        });
    };
    AdminShowsComponent.prototype.getDistinctDates = function () {
        var dateSet = new Set();
        for (var _i = 0, _a = this.shows; _i < _a.length; _i++) {
            var x = _a[_i];
            var movieDate = new Date(x.timestamp);
            dateSet.add(movieDate.getFullYear() + "-" + (movieDate.getMonth() + 1) + "-" + movieDate.getDate());
        }
        return Array.from(dateSet);
    };
    AdminShowsComponent.prototype.getShowsForDate = function (date) {
        var shows = [];
        for (var _i = 0, _a = this.shows; _i < _a.length; _i++) {
            var times = _a[_i];
            shows.push(times);
        }
        return shows;
    };
    AdminShowsComponent.prototype.chooseRoom = function () {
        var _this = this;
        this.cinemaService.getRooms().then(function (rooms) {
            _this.rooms = rooms;
        });
    };
    return AdminShowsComponent;
}());
AdminShowsComponent = __decorate([
    core_1.Component({
        selector: 'admin-shows',
        templateUrl: './admin.shows.component.html',
        providers: [cinema_service_1.CinemaService, show_service_1.ShowService, movie_service_1.MovieService],
    }),
    __metadata("design:paramtypes", [movie_service_1.MovieService, cinema_service_1.CinemaService, router_1.ActivatedRoute, show_service_1.ShowService])
], AdminShowsComponent);
exports.AdminShowsComponent = AdminShowsComponent;
//# sourceMappingURL=admin.shows.component.js.map