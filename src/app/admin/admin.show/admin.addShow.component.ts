import { Component, OnInit} from '@angular/core';
import { ActivatedRoute }   from '@angular/router';




import { Movie } from '../../common/movie.interface';
import { Show } from '../../common/show.interface';
import { Cinema } from '../../common/cinema.interface';
import { Room } from '../../common/room.interface';




import { JqueryService } from '../../services/jquery.service';
import { ShowService } from '../../services/show.service';



import { CinemaService } from '../../services/cinema.service';

@Component({
  selector: 'admin-addshow',
  templateUrl: './admin.addShow.component.html',
  providers: [CinemaService, ShowService],
})


export class AdminAddShowComponent {
	constructor(private cinemaService : CinemaService, private route : ActivatedRoute, private showServices : ShowService ){
		
	}
}