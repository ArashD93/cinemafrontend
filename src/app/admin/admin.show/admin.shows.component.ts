import { Component, OnInit, Input} from '@angular/core';
import { ActivatedRoute }   from '@angular/router';



import { Movie } from '../../common/movie.interface';
import { Show, emptyShow } from '../../common/show.interface';
import { Cinema, emptyCinema } from '../../common/cinema.interface';
import { Room } from '../../common/room.interface';
import { openingHour, closingHour } from '../../common/settings'
import { ReservationService } from '../../services/reservation.service';



import { JqueryService } from '../../services/jquery.service';
import { ShowService } from '../../services/show.service';
import { CinemaService } from '../../services/cinema.service';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'admin-shows',
  templateUrl: './admin.shows.component.html',
  providers: [CinemaService, ShowService, MovieService],
})

	
export class AdminShowsComponent implements OnInit {
	cinemas:Cinema[];
	movie:Movie[];
	cinema:Cinema;
	date : Date;
	shows : Show[];
	rooms : Room [];
	selectedCinemaId : number;

	constructor(private movieService : MovieService, private cinemaService : CinemaService, private route : ActivatedRoute, private showServices : ShowService ){
		this.cinemas = [];
		this.movie = [];
		this.cinema = emptyCinema;
		this.date = new Date();
		this.shows = [];
		this.rooms = [];
		this.selectedCinemaId = 0;
	}

	getCinemasFromService() : void {
		this.cinemaService.getCinemas()
		.then( (cinemas : Cinema[]) => {
			this.cinemas = cinemas;
		} )
		.catch( response => {
			console.log(response);
		});
	}

	ngOnInit(): void {
		this.getCinemasFromService();
  	}

  	moviesByCinema( c : Cinema ) : void {
  		this.cinema = c;
  		this.reloadMovies();
  	}

  	reloadMovies() : void {
  		this.movieService.getMoviesByCinemaId(this.cinema.id)
  		.then((movie:Movie[])=>{
  			this.movie = movie;
  		}).catch((error)=>{
  			console.log("RÁBASZTÁL, már megint :D még mindig ÁÁÁNDÍÍFÁJND TE VAK HÜLYE!")
  		});
  	}
  	private getDistinctDates() : string[]{
		let dateSet = new Set<string>();
		for(let x of this.shows){
			let movieDate = new Date(x.timestamp);
			dateSet.add( movieDate.getFullYear() + "-" + (movieDate.getMonth() + 1) + "-" + movieDate.getDate() );
		}
		return Array.from(dateSet);	

	}
	private getShowsForDate( date : string ) : Show[] {
		let shows : Show[] = [];
		for(let times of this.shows){
			shows.push(times);
		}
		return shows;

	}
	private chooseRoom(		) : void {
		this.cinemaService.getRooms().then((rooms : Room[]) =>{
			this.rooms = rooms;
		})
	}

}