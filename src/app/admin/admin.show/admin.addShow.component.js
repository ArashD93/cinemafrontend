"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var show_service_1 = require("../../services/show.service");
var cinema_service_1 = require("../../services/cinema.service");
var AdminAddShowComponent = (function () {
    function AdminAddShowComponent(cinemaService, route, showServices) {
        this.cinemaService = cinemaService;
        this.route = route;
        this.showServices = showServices;
    }
    return AdminAddShowComponent;
}());
AdminAddShowComponent = __decorate([
    core_1.Component({
        selector: 'admin-addshow',
        templateUrl: './admin.addShow.component.html',
        providers: [cinema_service_1.CinemaService, show_service_1.ShowService],
    }),
    __metadata("design:paramtypes", [cinema_service_1.CinemaService, router_1.ActivatedRoute, show_service_1.ShowService])
], AdminAddShowComponent);
exports.AdminAddShowComponent = AdminAddShowComponent;
//# sourceMappingURL=admin.addShow.component.js.map