import { Component, OnInit } from '@angular/core';	
import { ActivatedRoute } from '@angular/router';

import { Movie } from '../../common/movie.interface';
import { Show } from '../../common/show.interface';
import { Room } from '../../common/room.interface';
import { Cinema, emptyCinema } from '../../common/cinema.interface';

import { MovieService } from '../../services/movie.service';
import { ShowService } from '../../services/show.service';
import { CinemaService } from '../../services/cinema.service';


@Component ({
	selector: 'admin-cassa',
	templateUrl: './admin.cassa.component.html',
	providers: [ CinemaService, ShowService, MovieService ],
})

export class AdminCassaComponent implements OnInit{

	shows: Show[];
	selectedShow: Show;
	chosenCinemaId : number;
	chosenMovieId : number;
	chosenShowId : number;
	chosenRoomId : number;
	cinemas : Cinema[];
	cinema : Cinema[];
	hours : number[];
	months : number[]; 
	showDate : number;
	movies: Movie[];
	today: number;
	rooms : Room[];
	isDisabled: boolean = true;

	

	constructor(private showService: ShowService, 
				private movieService: MovieService, 
				private cinService : CinemaService) {
		this.shows = [];
		this.movies = [];
		this.chosenCinemaId = 0;
		this.chosenMovieId = 0;
		this.chosenShowId = 0;
		this.chosenRoomId = 0;
		this.cinemas = [];
		this.cinema = [];
		this.hours = [];
		this.months = [];
		this.movies = [];
		this.rooms = [];
		this.today = Date.now();

		
		
	}

	getMovieShowsInHour( movie : Movie, hour : number, date: Date ) : any {
		for(var i = 0; i < movie.shows.length; i++) {
			if (movie.shows[i].timestamp !== null) {
				var year = new Date(movie.shows[i].timestamp).getFullYear()
				var month = new Date(movie.shows[i].timestamp).getMonth();
				var day = new Date(movie.shows[i].timestamp).getDate();
				var hours = new Date(movie.shows[i].timestamp).getHours();
				var minutes = new Date(movie.shows[i].timestamp).getMinutes();
				if (month == month) {
					if (month < 10) {
						return { Date : month + ":0" + day, showId : movie.shows[i].id };
					} else {
						return { Date : month + ":" + day, showId : movie.shows[i].id };
					}
				}
				if (hour == hours) {
					if (minutes < 10) {
						return { time : hours + ":0" + minutes, showId : movie.shows[i].id };
					} else {
						return { time : hours + ":" + minutes, showId : movie.shows[i].id };
					}
				}
				
			} else {
				return {};
			}
		}
	}

	getCinemasforTimetableService () : void {
		this.cinService.getCinemas()
		.then((cinemaForTimeTable : Cinema[]) => {
			this.cinemas = cinemaForTimeTable;
		} )
		.catch(response => {
			console.log(response);
		})

	}

	private filterMovies ( ) : Movie[] {
		let ms = [];
		for (var i = 0; i < this.movies.length; i++) {
			if (this.movies[i].shows && this.movies[i].shows.length) {
				ms.push(this.movies[i]);
			}
			this.isDisabled = false;
		}
		return ms;
	}

	private reloadMovies() : void {
		this.movieService.getMoviesByCinemaId(this.chosenCinemaId).then( (movies) => {
			this.movies = movies;

		} );
	}

	private loadRooms() : void {
		this.showService.getRooms(this.chosenRoomId).then((rooms) => {
			this.rooms = rooms;
		})
	}

	 
	// TODO
	private reloadShows() : void {
		let ss:any = [];
		this.showService.getShowByMovieId(this.chosenMovieId).then( (shows) => {
			/*for (var j = 0; j < shows.length; j++){
				//console.log(shows[j].room_id ); 
				// room id-t kapcsoljunk cinema-id-hoz
				// shows[j].cinema-id-je legyen = vmi
			}*/
			shows.sort(function(a, b){
				if (a.timestamp < b.timestamp) return -1;
			    else if (a.timestamp > b.timestamp) return 1;
			    else return 0;
			});
			for (var j = 0; j < shows.length; j++){
				if (this.today <= +shows[j].timestamp && shows[j].movie_id == this.chosenMovieId ) {
					for(var i = 0; i < this.rooms.length; ++i) {
						if(this.rooms[i].id  == shows[j].room_id) {
							shows[j].cinema_id = this.rooms[i].cinema_id;
						}
					}
					ss.push(shows[j]);
				}
			}
			this.shows = ss;
		});	
	}
	
	ngOnInit() : void {
        
        this.getCinemasforTimetableService();
        this.reloadMovies();
        this.reloadShows();
        this.loadRooms();
         
    }

	/*
	private reloadShows() : void {
		let ss:any = [];
		this.showService.getShowByMovieId(this.chosenMovieId).then( (shows) => {
			shows.sort(function(a, b){
				if (a.timestamp < b.timestamp) return -1;
			    else if (a.timestamp > b.timestamp) return 1;
			    else return 0;
			});
			
			for (var j = 0; j < shows.length; j++){
				if (this.today <= +shows[j].timestamp) {
					ss.push(shows[j]);
				}
			}
			this.shows = ss;
		});
		
	}
	*/
	/*
	private setDate(): void {
		this.today = Date.now();
	}
	*/
	/*
	movies/id/
	*/
	
	/*
	szobák lekérése, és az alapján össze lehet kötni a szobát a mozikkal
	*/


	

}

