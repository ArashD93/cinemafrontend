"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var movie_service_1 = require("../../services/movie.service");
var show_service_1 = require("../../services/show.service");
var cinema_service_1 = require("../../services/cinema.service");
var AdminCassaComponent = (function () {
    function AdminCassaComponent(showService, movieService, cinService) {
        this.showService = showService;
        this.movieService = movieService;
        this.cinService = cinService;
        this.isDisabled = true;
        this.shows = [];
        this.movies = [];
        this.chosenCinemaId = 0;
        this.chosenMovieId = 0;
        this.chosenShowId = 0;
        this.chosenRoomId = 0;
        this.cinemas = [];
        this.cinema = [];
        this.hours = [];
        this.months = [];
        this.movies = [];
        this.rooms = [];
        this.today = Date.now();
    }
    AdminCassaComponent.prototype.getMovieShowsInHour = function (movie, hour, date) {
        for (var i = 0; i < movie.shows.length; i++) {
            if (movie.shows[i].timestamp !== null) {
                var year = new Date(movie.shows[i].timestamp).getFullYear();
                var month = new Date(movie.shows[i].timestamp).getMonth();
                var day = new Date(movie.shows[i].timestamp).getDate();
                var hours = new Date(movie.shows[i].timestamp).getHours();
                var minutes = new Date(movie.shows[i].timestamp).getMinutes();
                if (month == month) {
                    if (month < 10) {
                        return { Date: month + ":0" + day, showId: movie.shows[i].id };
                    }
                    else {
                        return { Date: month + ":" + day, showId: movie.shows[i].id };
                    }
                }
                if (hour == hours) {
                    if (minutes < 10) {
                        return { time: hours + ":0" + minutes, showId: movie.shows[i].id };
                    }
                    else {
                        return { time: hours + ":" + minutes, showId: movie.shows[i].id };
                    }
                }
            }
            else {
                return {};
            }
        }
    };
    AdminCassaComponent.prototype.getCinemasforTimetableService = function () {
        var _this = this;
        this.cinService.getCinemas()
            .then(function (cinemaForTimeTable) {
            _this.cinemas = cinemaForTimeTable;
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    AdminCassaComponent.prototype.filterMovies = function () {
        var ms = [];
        for (var i = 0; i < this.movies.length; i++) {
            if (this.movies[i].shows && this.movies[i].shows.length) {
                ms.push(this.movies[i]);
            }
            this.isDisabled = false;
        }
        return ms;
    };
    AdminCassaComponent.prototype.reloadMovies = function () {
        var _this = this;
        this.movieService.getMoviesByCinemaId(this.chosenCinemaId).then(function (movies) {
            _this.movies = movies;
        });
    };
    AdminCassaComponent.prototype.loadRooms = function () {
        var _this = this;
        this.showService.getRooms(this.chosenRoomId).then(function (rooms) {
            _this.rooms = rooms;
        });
    };
    // TODO
    AdminCassaComponent.prototype.reloadShows = function () {
        var _this = this;
        var ss = [];
        this.showService.getShowByMovieId(this.chosenMovieId).then(function (shows) {
            /*for (var j = 0; j < shows.length; j++){
                //console.log(shows[j].room_id );
                // room id-t kapcsoljunk cinema-id-hoz
                // shows[j].cinema-id-je legyen = vmi
            }*/
            shows.sort(function (a, b) {
                if (a.timestamp < b.timestamp)
                    return -1;
                else if (a.timestamp > b.timestamp)
                    return 1;
                else
                    return 0;
            });
            for (var j = 0; j < shows.length; j++) {
                if (_this.today <= +shows[j].timestamp && shows[j].movie_id == _this.chosenMovieId) {
                    for (var i = 0; i < _this.rooms.length; ++i) {
                        if (_this.rooms[i].id == shows[j].room_id) {
                            shows[j].cinema_id = _this.rooms[i].cinema_id;
                        }
                    }
                    ss.push(shows[j]);
                }
            }
            _this.shows = ss;
        });
    };
    AdminCassaComponent.prototype.ngOnInit = function () {
        this.getCinemasforTimetableService();
        this.reloadMovies();
        this.reloadShows();
        this.loadRooms();
    };
    return AdminCassaComponent;
}());
AdminCassaComponent = __decorate([
    core_1.Component({
        selector: 'admin-cassa',
        templateUrl: './admin.cassa.component.html',
        providers: [cinema_service_1.CinemaService, show_service_1.ShowService, movie_service_1.MovieService],
    }),
    __metadata("design:paramtypes", [show_service_1.ShowService,
        movie_service_1.MovieService,
        cinema_service_1.CinemaService])
], AdminCassaComponent);
exports.AdminCassaComponent = AdminCassaComponent;
//# sourceMappingURL=admin.cassa.component.js.map