import { Component } from '@angular/core';
import { ActivatedRoute }   from '@angular/router';

import { Cinema } from '../../common/cinema.interface';
import { Room } from '../../common/room.interface';
import { Row } from '../../common/row.interface';

import { CinemaService } from '../../services/cinema.service';

@Component({
  selector: 'admin-main',
  templateUrl: './admin.main.component.html',
  providers: [CinemaService],
})


export class AdminMainComponent {
	constructor(private cinemaService : CinemaService, private route : ActivatedRoute){
		
	}
}