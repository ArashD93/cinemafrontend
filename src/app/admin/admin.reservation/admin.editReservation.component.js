"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var cinema_service_1 = require("../../services/cinema.service");
var movie_service_1 = require("../../services/movie.service");
var show_service_1 = require("../../services/show.service");
var jquery_service_1 = require("../../services/jquery.service");
var AdminEditReservationComponent = (function () {
    function AdminEditReservationComponent(cinemaService, movieService, showService, route, jqueryService) {
        this.cinemaService = cinemaService;
        this.movieService = movieService;
        this.showService = showService;
        this.route = route;
        this.jqueryService = jqueryService;
    }
    return AdminEditReservationComponent;
}());
AdminEditReservationComponent = __decorate([
    core_1.Component({
        selector: 'admin-editReservation',
        templateUrl: './admin.editReservation.component.html',
        providers: [cinema_service_1.CinemaService, movie_service_1.MovieService, show_service_1.ShowService],
    }),
    __metadata("design:paramtypes", [cinema_service_1.CinemaService, movie_service_1.MovieService,
        show_service_1.ShowService, router_1.ActivatedRoute, jquery_service_1.JqueryService])
], AdminEditReservationComponent);
exports.AdminEditReservationComponent = AdminEditReservationComponent;
//# sourceMappingURL=admin.editReservation.component.js.map