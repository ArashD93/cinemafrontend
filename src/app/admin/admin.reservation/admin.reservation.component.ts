import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Cinema} from '../../common/cinema.interface';
import { Room } from '../../common/room.interface';
import { Row } from '../../common/row.interface';
import { Movie } from '../../common/movie.interface';
import { Show } from '../../common/show.interface';
import { User, emptyUser } from '../../common/user.interface';
import { Reservation } from '../../common/reservation.interface';
import { Ticket } from '../../common/ticket.interface';


import { CinemaService } from '../../services/cinema.service';
import { MovieService } from '../../services/movie.service';
import { ShowService } from '../../services/show.service';
import { ReservationService } from '../../services/reservation.service';
import { UserService } from '../../services/user.service';
import { JqueryService } from '../../services/jquery.service';

@Component({
  selector: 'admin-reservation',
  templateUrl: './admin.reservation.component.html',
  providers: [JqueryService, CinemaService, MovieService, ShowService, ReservationService, UserService],
})

export class AdminReservationComponent {	

	@Input()
	movie_id: number;
	selectedDate : Date;
	selectedCinemaId : number;
	reservation: Reservation;
	reservations : Reservation[];
	shows : Show[];
	movies : Movie[];
	selectedRoomId : number;
	selectedShowId : number;
	selectedMovieId : number;
	selectedUserId : number;
	cinemas : Cinema[];
	rooms : Room[];
	hours : number[];
	months : number[]; 
	showDate : number;
	today : number;
	givenEmail : string;
	user : User;
	tickets : Ticket[];

	constructor(
		private jqueryService: JqueryService,
		private cinemaService: CinemaService,
		private movieService : MovieService,
		private showService : ShowService,
		private reservationService: ReservationService,
		private userService : UserService,
		private router: Router ){
			
			this.givenEmail = '';
			this.shows = [];
			this.reservation;
			this.reservations= [];
			this.selectedCinemaId = 0;
			this.selectedRoomId = 0;
			this.selectedShowId = 0;
			this.selectedMovieId = 0;
			this.selectedUserId = 0;
			this.movies = [];
			this.cinemas = [];
			this.tickets = [];
			this.rooms = [];
			this.hours = [];
			this.months = [];
			this.today = Date.now();
			this.user = emptyUser;
	}


	getMovieShowsInHour( movie : Movie, hour : number, date: Date ) : any {
		for(var i = 0; i < movie.shows.length; i++) {
			if (movie.shows[i].timestamp !== null) {
				var year = new Date(movie.shows[i].timestamp).getFullYear()
				var month = new Date(movie.shows[i].timestamp).getMonth();
				var day = new Date(movie.shows[i].timestamp).getDate();
				var hours = new Date(movie.shows[i].timestamp).getHours();
				var minutes = new Date(movie.shows[i].timestamp).getMinutes();
				if (month == month) {
					if (month < 10) {
						return { Date : month + ":0" + day, showId : movie.shows[i].id };
					} else {
						return { Date : month + ":" + day, showId : movie.shows[i].id };
					}
				}
				if (hour == hours) {
					if (minutes < 10) {
						return { time : hours + ":0" + minutes, showId : movie.shows[i].id };
					} else {
						return { time : hours + ":" + minutes, showId : movie.shows[i].id };
					}
				}
			} else {
				return {};
			}
		}
	}

	getCinemasforReservations () : void {
		this.cinemaService.getCinemas()
		.then((cinemaForTimeTable : Cinema[]) => {
			this.cinemas = cinemaForTimeTable;
		} )
		.catch(response => {
			console.log(response);
		})

	}

	private filterMovies ( ) : Movie[] {
		let ms = [];
		for (var i = 0; i < this.movies.length; i++) {
			if (this.movies[i].shows && this.movies[i].shows.length) {
				ms.push(this.movies[i]);
			}
		}
		return ms;
	}

	private reloadMovies() : void {
		this.movieService.getMoviesByCinemaId(this.selectedCinemaId).then( (movies) => {
			this.movies = movies;

		} );
	}

	private loadRooms() : void {
		this.showService.getRooms(this.selectedRoomId).then((rooms) => {
			this.rooms = rooms;
		})
	}

	private reloadShows() : void {
		let ss:any = [];
		this.showService.getShowByMovieId(this.selectedMovieId).then( (shows) => {
			shows.sort(function(a, b){
				if (a.timestamp < b.timestamp) return -1;
			    else if (a.timestamp > b.timestamp) return 1;
			    else return 0;
			});
			for (var j = 0; j < shows.length; j++){
				if (this.today <= +shows[j].timestamp && shows[j].movie_id == this.selectedMovieId ) {
					for(var i = 0; i < this.rooms.length; ++i) {
						if(this.rooms[i].id  == shows[j].room_id) {
							shows[j].cinema_id = this.rooms[i].cinema_id;
						}
					}
					ss.push(shows[j]);
				}
			}
			this.shows = ss;
		});	
	}

	private getThisUserAndReservations() : void {
		if (this.givenEmail != ''){
			let userPromise = this.userService.getUserByEmail(this.givenEmail).then((user) => {
				this.user = user;
			});
			let reservationPromise = this.reservationService.getReservationByUserId(this.selectedUserId).then((reservation) =>{
				this.reservation = reservation;
			});
			Promise.all([userPromise, reservationPromise]).then( () => {
				//this.user = user;
				//this.reservation = reservation;
			}).catch( (error)=> {
				console.log('Error to get user and reservation:' + error);	
			});
		}else{
			this.jqueryService.growlError("Get the user", "Can't get the user");
		}
	}

	private getReservations() : void {
		if (this.selectedShowId != null) {
			this.reservationService.getReservedSeats(this.selectedShowId).then((tickets:Ticket[]) => {
				this.tickets = tickets;
			});
		}
	}
	
	
	ngOnInit() : void {
        this.getCinemasforReservations();
        this.reloadMovies();
        this.reloadShows();
        this.loadRooms();
    }

    /*
	
	private getUserAndReservedSeatsByShowId(showId:number) : void{
		let reservationPromise = this.reservationService.getReservedSeats(showID)
		.then(() => {

		})
		let 
	}
	*/

}	