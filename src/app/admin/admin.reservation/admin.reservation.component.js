"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var user_interface_1 = require("../../common/user.interface");
var cinema_service_1 = require("../../services/cinema.service");
var movie_service_1 = require("../../services/movie.service");
var show_service_1 = require("../../services/show.service");
var reservation_service_1 = require("../../services/reservation.service");
var user_service_1 = require("../../services/user.service");
var jquery_service_1 = require("../../services/jquery.service");
var AdminReservationComponent = (function () {
    function AdminReservationComponent(jqueryService, cinemaService, movieService, showService, reservationService, userService, router) {
        this.jqueryService = jqueryService;
        this.cinemaService = cinemaService;
        this.movieService = movieService;
        this.showService = showService;
        this.reservationService = reservationService;
        this.userService = userService;
        this.router = router;
        this.givenEmail = '';
        this.shows = [];
        this.reservation;
        this.reservations = [];
        this.selectedCinemaId = 0;
        this.selectedRoomId = 0;
        this.selectedShowId = 0;
        this.selectedMovieId = 0;
        this.selectedUserId = 0;
        this.movies = [];
        this.cinemas = [];
        this.tickets = [];
        this.rooms = [];
        this.hours = [];
        this.months = [];
        this.today = Date.now();
        this.user = user_interface_1.emptyUser;
    }
    AdminReservationComponent.prototype.getMovieShowsInHour = function (movie, hour, date) {
        for (var i = 0; i < movie.shows.length; i++) {
            if (movie.shows[i].timestamp !== null) {
                var year = new Date(movie.shows[i].timestamp).getFullYear();
                var month = new Date(movie.shows[i].timestamp).getMonth();
                var day = new Date(movie.shows[i].timestamp).getDate();
                var hours = new Date(movie.shows[i].timestamp).getHours();
                var minutes = new Date(movie.shows[i].timestamp).getMinutes();
                if (month == month) {
                    if (month < 10) {
                        return { Date: month + ":0" + day, showId: movie.shows[i].id };
                    }
                    else {
                        return { Date: month + ":" + day, showId: movie.shows[i].id };
                    }
                }
                if (hour == hours) {
                    if (minutes < 10) {
                        return { time: hours + ":0" + minutes, showId: movie.shows[i].id };
                    }
                    else {
                        return { time: hours + ":" + minutes, showId: movie.shows[i].id };
                    }
                }
            }
            else {
                return {};
            }
        }
    };
    AdminReservationComponent.prototype.getCinemasforReservations = function () {
        var _this = this;
        this.cinemaService.getCinemas()
            .then(function (cinemaForTimeTable) {
            _this.cinemas = cinemaForTimeTable;
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    AdminReservationComponent.prototype.filterMovies = function () {
        var ms = [];
        for (var i = 0; i < this.movies.length; i++) {
            if (this.movies[i].shows && this.movies[i].shows.length) {
                ms.push(this.movies[i]);
            }
        }
        return ms;
    };
    AdminReservationComponent.prototype.reloadMovies = function () {
        var _this = this;
        this.movieService.getMoviesByCinemaId(this.selectedCinemaId).then(function (movies) {
            _this.movies = movies;
        });
    };
    AdminReservationComponent.prototype.loadRooms = function () {
        var _this = this;
        this.showService.getRooms(this.selectedRoomId).then(function (rooms) {
            _this.rooms = rooms;
        });
    };
    AdminReservationComponent.prototype.reloadShows = function () {
        var _this = this;
        var ss = [];
        this.showService.getShowByMovieId(this.selectedMovieId).then(function (shows) {
            shows.sort(function (a, b) {
                if (a.timestamp < b.timestamp)
                    return -1;
                else if (a.timestamp > b.timestamp)
                    return 1;
                else
                    return 0;
            });
            for (var j = 0; j < shows.length; j++) {
                if (_this.today <= +shows[j].timestamp && shows[j].movie_id == _this.selectedMovieId) {
                    for (var i = 0; i < _this.rooms.length; ++i) {
                        if (_this.rooms[i].id == shows[j].room_id) {
                            shows[j].cinema_id = _this.rooms[i].cinema_id;
                        }
                    }
                    ss.push(shows[j]);
                }
            }
            _this.shows = ss;
        });
    };
    AdminReservationComponent.prototype.getThisUserAndReservations = function () {
        var _this = this;
        if (this.givenEmail != '') {
            var userPromise = this.userService.getUserByEmail(this.givenEmail).then(function (user) {
                _this.user = user;
            });
            var reservationPromise = this.reservationService.getReservationByUserId(this.selectedUserId).then(function (reservation) {
                _this.reservation = reservation;
            });
            Promise.all([userPromise, reservationPromise]).then(function () {
                //this.user = user;
                //this.reservation = reservation;
            }).catch(function (error) {
                console.log('Error to get user and reservation:' + error);
            });
        }
        else {
            this.jqueryService.growlError("Get the user", "Can't get the user");
        }
    };
    AdminReservationComponent.prototype.getReservations = function () {
        var _this = this;
        if (this.selectedShowId != null) {
            this.reservationService.getReservedSeats(this.selectedShowId).then(function (tickets) {
                _this.tickets = tickets;
            });
        }
    };
    AdminReservationComponent.prototype.ngOnInit = function () {
        this.getCinemasforReservations();
        this.reloadMovies();
        this.reloadShows();
        this.loadRooms();
    };
    return AdminReservationComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], AdminReservationComponent.prototype, "movie_id", void 0);
AdminReservationComponent = __decorate([
    core_1.Component({
        selector: 'admin-reservation',
        templateUrl: './admin.reservation.component.html',
        providers: [jquery_service_1.JqueryService, cinema_service_1.CinemaService, movie_service_1.MovieService, show_service_1.ShowService, reservation_service_1.ReservationService, user_service_1.UserService],
    }),
    __metadata("design:paramtypes", [jquery_service_1.JqueryService,
        cinema_service_1.CinemaService,
        movie_service_1.MovieService,
        show_service_1.ShowService,
        reservation_service_1.ReservationService,
        user_service_1.UserService,
        router_1.Router])
], AdminReservationComponent);
exports.AdminReservationComponent = AdminReservationComponent;
//# sourceMappingURL=admin.reservation.component.js.map