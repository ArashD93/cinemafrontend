import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Cinema} from '../../common/cinema.interface';
import { Room } from '../../common/room.interface';
import { Row } from '../../common/row.interface';
import { Movie } from '../../common/movie.interface';
import { Show } from '../../common/show.interface';


import { CinemaService } from '../../services/cinema.service';
import { MovieService } from '../../services/movie.service';
import { ShowService } from '../../services/show.service';
import { JqueryService } from '../../services/jquery.service';

@Component({
  selector: 'admin-editReservation',
  templateUrl: './admin.editReservation.component.html',
  providers: [CinemaService, MovieService, ShowService],
})


export class AdminEditReservationComponent {

	constructor(private cinemaService : CinemaService, private movieService : MovieService, 
		private showService : ShowService, private route : ActivatedRoute, private jqueryService: JqueryService) {
		


	}
	
}	