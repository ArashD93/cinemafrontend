import { Component, OnInit } from '@angular/core';
import { ActivatedRoute }   from '@angular/router';

import { Movie,emptyMovie } from '../../common/movie.interface';

import { MovieService } from '../../services/movie.service';
import { JqueryService } from '../../services/jquery.service';

@Component({
  selector: 'admin-movies',
  templateUrl: './admin.movies.component.html',
  providers: [MovieService],
})


export class AdminMoviesComponent implements OnInit {
	
	id : number;
	movies : Movie[];
	title : string;

	constructor(private movieService : MovieService, private route : ActivatedRoute) {
		
		this.id = route.snapshot.params['id'];
		this.movies = [];

	}

	getMoviesFromServiec() : void {
		this.movieService.getMovies()
		.then( (movies : Movie[])=> {
			this.movies = movies;
		})
		.catch(response => {
			console.log(response);
		});
	}

	ngOnInit() : void {
		this.getMoviesFromServiec();
	}

	deleteThisMovie(id : number) : void {
		this.movieService.deleteMovie(id)
		.then(() => {
			this.movies.splice(id, 1);
		});
	}

}