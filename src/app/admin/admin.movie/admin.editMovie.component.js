"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var movie_interface_1 = require("../../common/movie.interface");
var movie_service_1 = require("../../services/movie.service");
var jquery_service_1 = require("../../services/jquery.service");
var AdminEditMovieComponent = (function () {
    function AdminEditMovieComponent(movieService, route, jqueryService) {
        this.movieService = movieService;
        this.route = route;
        this.jqueryService = jqueryService;
        this.id = route.snapshot.params['id'];
        this.movie = movie_interface_1.emptyMovie;
        this.ratings = movie_interface_1.ratings;
    }
    AdminEditMovieComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.movieService.getMovieById(this.id)
            .then(function (movie) {
            _this.movie = movie;
        }).catch(function (error) {
            console.log('Error to get movie:' + error);
        });
    };
    AdminEditMovieComponent.prototype.editThisMovie = function (movie) {
        var _this = this;
        this.movieService.editMovie(this.movie).then(function () {
            _this.jqueryService.growlSuccess("Edit movie", "Update successful");
        });
    };
    return AdminEditMovieComponent;
}());
AdminEditMovieComponent = __decorate([
    core_1.Component({
        selector: 'admin-editMovie',
        templateUrl: './admin.editMovie.component.html',
        providers: [movie_service_1.MovieService],
    }),
    __metadata("design:paramtypes", [movie_service_1.MovieService, router_1.ActivatedRoute, jquery_service_1.JqueryService])
], AdminEditMovieComponent);
exports.AdminEditMovieComponent = AdminEditMovieComponent;
//# sourceMappingURL=admin.editMovie.component.js.map