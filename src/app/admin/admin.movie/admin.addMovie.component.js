"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_1 = require("@angular/http");
var movie_interface_1 = require("../../common/movie.interface");
var movie_service_1 = require("../../services/movie.service");
var jquery_service_1 = require("../../services/jquery.service");
var AdminAddMovieComponent = (function () {
    function AdminAddMovieComponent(movieService, route, jqueryService, http) {
        this.movieService = movieService;
        this.route = route;
        this.jqueryService = jqueryService;
        this.http = http;
        this.id = route.snapshot.params['id'];
        this.movie = movie_interface_1.emptyMovie;
        this.tmdbResults = [];
        this.ratings = movie_interface_1.ratings;
    }
    AdminAddMovieComponent.prototype.ngOnInit = function () {
        this.jqueryService.initAddMoviePopup();
    };
    AdminAddMovieComponent.prototype.addNewMovie = function () {
        var _this = this;
        this.movieService.addMovie(this.movie).then(function (movie) {
            _this.jqueryService.growlSuccess("Add movie", "Add successful");
        }).catch(function () {
            _this.jqueryService.growlError("Add movie", "Can't add movie");
        });
    };
    AdminAddMovieComponent.prototype.fetchFromTMDB = function () {
        var _this = this;
        var url = 'https://api.themoviedb.org/3/search/movie?api_key=d5aa952e734358071b9dcb1f47b320a5&query=';
        url += encodeURI(this.movie.title);
        this.http.get(url).toPromise().then(function (response) {
            _this.tmdbResults = response.json().results;
            if (_this.tmdbResults.length == 0) {
                _this.jqueryService.growlError("TMDB", "No such film");
            }
            else if (_this.tmdbResults.length == 1) {
                _this.getMovie(_this.tmdbResults[0].id);
                _this.jqueryService.growlSuccess("TMDB", "Fetch successful");
            }
            else {
                _this.jqueryService.showAddMoviePopup();
            }
        }).catch(function () {
            _this.jqueryService.growlError("EMPTY TITLE", "Type any title!");
        });
    };
    AdminAddMovieComponent.prototype.getMovie = function (id) {
        var _this = this;
        this.movieService.getMovieFromTmdbById(id).then(function (movie) {
            _this.movie.description = movie.description;
            _this.movie.length = movie.length;
            _this.movie.rated = movie.rated;
            _this.movie.director = movie.director;
            _this.movie.actors = movie.actors;
            _this.movie.categoryList = movie.categoryList;
            _this.movie.imdb_rating = movie.imdb_rating;
            _this.movie.trailer_url = movie.trailer_url;
            _this.movie.thumbnail_url = movie.thumbnail_url;
            _this.movie.hero_image_url = movie.hero_image_url;
            _this.movie.background_image_url = movie.background_image_url;
            _this.movie.is_featured_in_slider = movie.is_featured_in_slider;
            _this.movie.release_date = movie.release_date;
        }).catch(function () {
            _this.jqueryService.growlError("Get movie", "Can't get the movie");
        });
    };
    AdminAddMovieComponent.prototype.getMovieAndClosePopup = function (id) {
        this.getMovie(id);
        this.jqueryService.hideAddMoviePopup();
    };
    return AdminAddMovieComponent;
}());
AdminAddMovieComponent = __decorate([
    core_1.Component({
        selector: 'admin-addMovie',
        templateUrl: './admin.addMovie.component.html',
        providers: [movie_service_1.MovieService],
    }),
    __metadata("design:paramtypes", [movie_service_1.MovieService, router_1.ActivatedRoute, jquery_service_1.JqueryService, http_1.Http])
], AdminAddMovieComponent);
exports.AdminAddMovieComponent = AdminAddMovieComponent;
//# sourceMappingURL=admin.addMovie.component.js.map