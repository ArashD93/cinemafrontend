import { Component, OnInit } from '@angular/core';
import { ActivatedRoute }   from '@angular/router';
import { Http }   from '@angular/http';

import { Cinema} from '../../common/cinema.interface';
import { Room } from '../../common/room.interface';
import { Row } from '../../common/row.interface';
import { Movie, emptyMovie, ratings } from '../../common/movie.interface';

import { MovieService } from '../../services/movie.service';
import { JqueryService } from '../../services/jquery.service';

@Component({
  selector: 'admin-addMovie',
  templateUrl: './admin.addMovie.component.html',
  providers: [MovieService],
})


export class AdminAddMovieComponent implements OnInit {
	
	id : number;
	movie : Movie;
	tmdbResults : any[];
	ratings: any[];

	constructor(private movieService : MovieService, private route : ActivatedRoute, private jqueryService : JqueryService, private http : Http) {
		
		this.id = route.snapshot.params['id'];
		this.movie = emptyMovie;
		this.tmdbResults = [];
		this.ratings = ratings;
	}

	ngOnInit() {
		this.jqueryService.initAddMoviePopup();
	}
	
	addNewMovie() : void {
		this.movieService.addMovie(this.movie).then( movie => {
			this.jqueryService.growlSuccess("Add movie", "Add successful");
		}).catch(() =>{
			this.jqueryService.growlError("Add movie", "Can't add movie");
		});
	}

	private fetchFromTMDB () : void {
		let url = 'https://api.themoviedb.org/3/search/movie?api_key=d5aa952e734358071b9dcb1f47b320a5&query=';
		url += encodeURI( this.movie.title );
		this.http.get(url).toPromise().then( response => {
			this.tmdbResults = response.json().results;
			if (this.tmdbResults.length == 0) {
				this.jqueryService.growlError("TMDB", "No such film")
			} else if (this.tmdbResults.length == 1) {
				this.getMovie(this.tmdbResults[0].id);
				this.jqueryService.growlSuccess("TMDB", "Fetch successful");
			} else {
				this.jqueryService.showAddMoviePopup();
			}
		}).catch(() => {
			this.jqueryService.growlError("EMPTY TITLE", "Type any title!")
		});
	}

	private getMovie(id : number): void {
		this.movieService.getMovieFromTmdbById(id).then( (movie) => {
			this.movie.description = movie.description;
			this.movie.length = movie.length;
			this.movie.rated = movie.rated;
			this.movie.director = movie.director;
			this.movie.actors = movie.actors;
			this.movie.categoryList = movie.categoryList;
			this.movie.imdb_rating = movie.imdb_rating;
			this.movie.trailer_url = movie.trailer_url;
			this.movie.thumbnail_url = movie.thumbnail_url;
			this.movie.hero_image_url = movie.hero_image_url;
			this.movie.background_image_url = movie.background_image_url;
			this.movie.is_featured_in_slider = movie.is_featured_in_slider;
			this.movie.release_date = movie.release_date;
		}).catch( ()=> {
			this.jqueryService.growlError("Get movie", "Can't get the movie");
		});
	}
	private getMovieAndClosePopup (id : number): void {
		this.getMovie(id);
		this.jqueryService.hideAddMoviePopup();
	}
	
}