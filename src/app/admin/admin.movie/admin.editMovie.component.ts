import { Component, OnInit } from '@angular/core';
import { ActivatedRoute }   from '@angular/router';

import { Cinema} from '../../common/cinema.interface';
import { Room } from '../../common/room.interface';
import { Row } from '../../common/row.interface';
import { Movie, emptyMovie, ratings } from '../../common/movie.interface';

import { MovieService } from '../../services/movie.service';
import { JqueryService } from '../../services/jquery.service';

@Component({
  selector: 'admin-editMovie',
  templateUrl: './admin.editMovie.component.html',
  providers: [MovieService],
})


export class AdminEditMovieComponent implements OnInit {
	id : number;
	movie: Movie;
	ratings : any[];

	constructor(private movieService : MovieService, private route : ActivatedRoute, private jqueryService : JqueryService) {
		
		this.id = route.snapshot.params['id'];
		this.movie = emptyMovie;
		this.ratings = ratings;
	}

	ngOnInit() : void {
		this.movieService.getMovieById(this.id)
			.then((movie : Movie) => {
				this.movie = movie;
			}).catch((error)=>{
				console.log('Error to get movie:' + error);
			});
	}
	
	editThisMovie(movie : Movie) : void {
		this.movieService.editMovie(this.movie).then(() =>{
			this.jqueryService.growlSuccess("Edit movie", "Update successful");
		});
	}

}