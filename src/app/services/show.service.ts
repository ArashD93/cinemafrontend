import { Injectable } from '@angular/core';
import { Http, URLSearchParams, RequestOptions } from '@angular/http';

import { Show } from '../common/show.interface';
import { backendBaseUrl } from '../common/settings';
import { Room } from '../common/room.interface';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class ShowService {

  	constructor(private http: Http ) { }

  	url = "api/shows";

    public getShowsByDateAndCinema(day?: Date, cinema_id?: number) : Promise<Show[]> {

    	let showParams = new URLSearchParams();
    	if (day) {
	    	showParams.append( 'day', "" + day.getFullYear() + 
	    		( (day.getMonth() + 1) < 10 ? '0' + (day.getMonth() + 1) : (day.getMonth() + 1) ) + 
	    		( day.getDate() < 10 ? '0' + day.getDate() : day.getDate() ) );
	    	
	    }
	    if (cinema_id) {
    		showParams.append( 'cinemaid', "" + cinema_id );
    	}
    	let showOptions = new RequestOptions({ params: showParams });

    	return this.http.get( backendBaseUrl + 'shows', showOptions ).toPromise()
    		.then(response => response.json() as Show[]);
    }

	public getShows() : Promise<Show[]> {
		return this.http.get(backendBaseUrl + 'shows').toPromise()
           .then(response => response.json() as Show[]);
	}

    public getRooms(id: number) : Promise<Room[]> {
        return this.http.get(backendBaseUrl + 'cinemas/showRooms').toPromise()
            .then(response => response.json() as Room[]);
    }

    public getShowByMovieId(id: number) : Promise<Show[]>{
        return this.http.get(backendBaseUrl + 'shows').toPromise()
           .then(response => response.json() as Show[]);
    }
   
    /*
    public getShowsByCinema(cinema_id: number) : Promise<Show[]> {
		return this.http.get(backendBaseUrl + 'shows/cinema/' + cinema_id).toPromise()
		       .then(response => response.json() as Show[]);
    }

    */    

    public getShowsByDate( date: Date )  : Promise<Show[]> {
    	return this.http.get(backendBaseUrl + '?day=' + date.getFullYear() + (date.getMonth() + 1)  + date.getDay() )
            .toPromise()
		    .then(response => response.json() as Show[]);
    }	
    
}