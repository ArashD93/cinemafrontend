"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var settings_1 = require("../common/settings");
require("rxjs/add/operator/toPromise");
var MovieService = (function () {
    function MovieService(http) {
        this.http = http;
        this.startPlayingTrailer = false;
    }
    MovieService.prototype.getMovies = function () {
        return this.http.get(settings_1.backendBaseUrl + 'movies')
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    MovieService.prototype.getSliderFeturedMovies = function () {
        return this.http.get(settings_1.backendBaseUrl + 'movies')
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    MovieService.prototype.getMovieById = function (id) {
        return this.http.get(settings_1.backendBaseUrl + 'movies/' + id, id)
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    MovieService.prototype.getMovieFromTmdbById = function (id) {
        return this.http.get(settings_1.backendBaseUrl + 'movies/tmdb?id=' + id)
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    MovieService.prototype.getMoviesByCinemaId = function (id) {
        return this.http.get(settings_1.backendBaseUrl + 'cinemas/' + id + '/movies').toPromise()
            .then(function (response) { return response.json(); });
    };
    MovieService.prototype.getRoomsByMovieId = function (id) {
        return this.http.get(settings_1.backendBaseUrl + 'movies/' + id + '/showRooms').toPromise()
            .then(function (response) { return response.json(); });
    };
    MovieService.prototype.getRatings = function () {
        return this.http.get(settings_1.backendBaseUrl + 'movies/ratings').toPromise()
            .then(function (response) { return response.json(); });
    };
    /*----admin-----*/
    MovieService.prototype.deleteMovie = function (id) {
        return this.http.delete(settings_1.backendBaseUrl + 'movies/' + id, { withCredentials: true }).toPromise()
            .then(function () { return null; })
            .catch(function (error) {
            console.log('Error to delete movie:' + error);
        });
    };
    MovieService.prototype.editMovie = function (movie) {
        return this.http.put(settings_1.backendBaseUrl + 'movies/' + movie.id, movie, { withCredentials: true }).toPromise()
            .then(function () { return movie; })
            .catch(function (error) {
            console.log('Error to edit movie:' + error);
        });
    };
    MovieService.prototype.addMovie = function (movie) {
        return this.http.post(settings_1.backendBaseUrl + 'movies', movie, { withCredentials: true }).toPromise().then(function (response) { return response.json(); });
    };
    MovieService.prototype.setStartPlayingTrailer = function (b) {
        this.startPlayingTrailer = b;
    };
    return MovieService;
}());
MovieService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], MovieService);
exports.MovieService = MovieService;
//# sourceMappingURL=movie.service.js.map