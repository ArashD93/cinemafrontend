"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var settings_1 = require("../common/settings");
require("rxjs/add/operator/toPromise");
var ShowService = (function () {
    function ShowService(http) {
        this.http = http;
        this.url = "api/shows";
    }
    ShowService.prototype.getShowsByDateAndCinema = function (day, cinema_id) {
        var showParams = new http_1.URLSearchParams();
        if (day) {
            showParams.append('day', "" + day.getFullYear() +
                ((day.getMonth() + 1) < 10 ? '0' + (day.getMonth() + 1) : (day.getMonth() + 1)) +
                (day.getDate() < 10 ? '0' + day.getDate() : day.getDate()));
        }
        if (cinema_id) {
            showParams.append('cinemaid', "" + cinema_id);
        }
        var showOptions = new http_1.RequestOptions({ params: showParams });
        return this.http.get(settings_1.backendBaseUrl + 'shows', showOptions).toPromise()
            .then(function (response) { return response.json(); });
    };
    ShowService.prototype.getShows = function () {
        return this.http.get(settings_1.backendBaseUrl + 'shows').toPromise()
            .then(function (response) { return response.json(); });
    };
    ShowService.prototype.getRooms = function (id) {
        return this.http.get(settings_1.backendBaseUrl + 'cinemas/showRooms').toPromise()
            .then(function (response) { return response.json(); });
    };
    ShowService.prototype.getShowByMovieId = function (id) {
        return this.http.get(settings_1.backendBaseUrl + 'shows').toPromise()
            .then(function (response) { return response.json(); });
    };
    /*
    public getShowsByCinema(cinema_id: number) : Promise<Show[]> {
        return this.http.get(backendBaseUrl + 'shows/cinema/' + cinema_id).toPromise()
               .then(response => response.json() as Show[]);
    }

    */
    ShowService.prototype.getShowsByDate = function (date) {
        return this.http.get(settings_1.backendBaseUrl + '?day=' + date.getFullYear() + (date.getMonth() + 1) + date.getDay())
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    return ShowService;
}());
ShowService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ShowService);
exports.ShowService = ShowService;
//# sourceMappingURL=show.service.js.map