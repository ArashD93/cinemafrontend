import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { backendBaseUrl } from '../common/settings';
import { Reservation } from '../common/reservation.interface';
import { Ticket } from '../common/ticket.interface';
import { TicketBuying } from '../common/ticketBuying.interface';
import { Order } from '../common/order.interface';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ReservationService  {

	constructor(private http: Http){}

	public getCinemaByMovie(movie_id:number) : Promise< Reservation[] > {
		return this.http.get(backendBaseUrl + 'movies/'+ movie_id+ '/reservation/')
		.toPromise()
		.then(response => response.json() as Reservation[]);
	}

	public setConfirmation(reservationDietails : TicketBuying, confirmationEmail: Order) : Promise< number[] > {
		let postConfi = { rowAndSeatList : reservationDietails.seats , payment_mode : reservationDietails.paymentType,
						  showId : reservationDietails.showId, userId : reservationDietails.userId, 
						  title : confirmationEmail.title, 
						  price : confirmationEmail.base_ticket_price }
		return this.http.post((backendBaseUrl + 'reservations'),postConfi)
		.toPromise()
		.then(response => response.json());
	}

	public getReservationByUserId(userId: number) : Promise<Reservation>{
		return this.http.get(backendBaseUrl + 'reservations/ofuser/' + userId)
		.toPromise()
		.then(response => response.json() as Reservation);
	}

	public getReservationsByShowId(showId:number) : Promise<Reservation[]> {
		return this.http.get(backendBaseUrl + 'reservations').toPromise()
		.then(response => response.json() as Reservation[]);
	}

/*---------------Admin függvények-----------------*/

	public editReservation(reservation : Reservation) : Promise<Reservation>{
		return this.http.put(backendBaseUrl, reservation, {withCredentials: true}).toPromise()
			.then(() => reservation)
			.catch((error:string) => {
				console.log('Error to edit reservation:' + error);
			})
	}

	public deleteReservation(id:number) : Promise<void> {
		return this.http.delete(backendBaseUrl + 'reservations/' + id, {withCredentials: true}).toPromise()
		    .then(() => null)
		    .catch((error: string) => {
			console.log('Error to delete reservation:' + error);
	    });
	}
	public getReservedSeats(showID:number) : Promise <Ticket[]> {
		return this.http.get(backendBaseUrl + 'reservations/' + showID)
		.toPromise().then(response => response.json() as Ticket[]);
	}
}
