import { Injectable } from '@angular/core';

function _window() : any {
   // return the global native browser window object
   return window;
}


@Injectable()
export class WindowRef {
   
   get nativeWindow() : any {
      return _window();
   }

}

@Injectable()
export class Scroll {

  win: Window;

  constructor(private windowRef: WindowRef){
       this.win = windowRef.nativeWindow;
  }

  scrollTo(yPoint: number, duration: number) {
    setTimeout(() => {
      this.win.window.scrollTo(0, yPoint)
    }, duration);
    return;
  }

  smoothScroll() {
    var startY = this.currentYPosition();
    var stopY = 0;
    var distance = stopY > startY ? stopY - startY : startY - stopY;
    if (distance < 100) {
      this.win.window.scrollTo(0, stopY); return;
    }
    var speed = Math.round(distance / 300);
    if (speed >= 20) speed = 20;
    var step = Math.round(distance / 100);
    var leapY = stopY > startY ? startY + step : startY - step;
    var timer = 0;
    if (stopY > startY) {
      for (var i = startY; i < stopY; i += step) {
        this.scrollTo(leapY, timer * speed);
        leapY += step; if (leapY > stopY) leapY = stopY; timer++;
      } return;
    }
    for (var i = startY; i > stopY; i -= step) {
      this.scrollTo(leapY, timer * speed);
      leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
    }
  }

  currentYPosition() {
      if (self.pageYOffset) return self.pageYOffset;
      return 0;
  }


}