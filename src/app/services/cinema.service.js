"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var settings_1 = require("../common/settings");
require("rxjs/add/operator/toPromise");
var CinemaService = (function () {
    function CinemaService(http) {
        this.http = http;
    }
    CinemaService.prototype.getCinema = function (id) {
        return this.http.get(settings_1.backendBaseUrl + 'cinemas/' + id).toPromise()
            .then(function (response) { return response.json(); });
    };
    CinemaService.prototype.getCinemas = function () {
        return this.http.get(settings_1.backendBaseUrl + 'cinemas').toPromise()
            .then(function (response) { return response.json(); });
    };
    CinemaService.prototype.getRooms = function () {
        return this.http.get(settings_1.backendBaseUrl + 'cinemas/showRooms').toPromise()
            .then(function (response) { return response.json(); });
    };
    CinemaService.prototype.getRoomsByCinemaId = function (cinema_id) {
        return this.http.get(settings_1.backendBaseUrl + 'cinemas/showRooms/' + cinema_id).toPromise()
            .then(function (response) { return response.json(); });
    };
    CinemaService.prototype.getRoomByRoomId = function (room_id) {
        return this.http.get(settings_1.backendBaseUrl + 'cinemas/showOneRoom/' + room_id).toPromise()
            .then(function (response) { return response.json(); });
    };
    CinemaService.prototype.getRowsByRoomId = function (room_id) {
        return this.http.get(settings_1.backendBaseUrl + 'cinemas/rooms/' + room_id + '/rows').toPromise()
            .then(function (response) { return response.json(); });
    };
    /*-----------------------------------ADMIN FÜGGVÉNYEK-----------------------------------------*/
    //mozikhoz tartozó függvények
    CinemaService.prototype.editCinema = function (cinema) {
        return this.http.put(settings_1.backendBaseUrl + 'cinemas/' + cinema.id, cinema, { withCredentials: true }).toPromise()
            .then(function () { return cinema; })
            .catch(function (error) {
            console.log('Error to edit cinema:' + error);
        });
    };
    CinemaService.prototype.createCinema = function (cinema) {
        return this.http.post(settings_1.backendBaseUrl + 'cinemas', cinema, { withCredentials: true }).toPromise().then(function (response) { return response.json(); });
    };
    CinemaService.prototype.deleteCinema = function (id) {
        return this.http.delete(settings_1.backendBaseUrl + 'cinemas/' + id, { withCredentials: true }).toPromise()
            .then(function () { return null; })
            .catch(function (error) {
            console.log('Error to delete cinema:' + error);
        });
    };
    //Termekhez tartozó függvények
    CinemaService.prototype.addRoom = function (cinema_id, room) {
        return this.http.post(settings_1.backendBaseUrl + 'cinemas/addRoom/' + cinema_id, room, { withCredentials: true })
            .toPromise().then(function (response) { return response.json(); });
    };
    CinemaService.prototype.editRoom = function (room) {
        return this.http.put(settings_1.backendBaseUrl + 'modifyRoom/' + room.id, room, { withCredentials: true }).toPromise()
            .then(function () { return room; })
            .catch(function (error) {
            console.log('Error to edit room:' + error);
        });
    };
    CinemaService.prototype.deleteRoom = function (id) {
        return this.http.delete(settings_1.backendBaseUrl + 'modifyRoom/' + id, { withCredentials: true }).toPromise()
            .then(function () { return null; })
            .catch(function (error) {
            console.log('Error to delete room:' + error);
        });
    };
    //Sorokhoz tartozó függvények
    CinemaService.prototype.addRow = function (room_id, row) {
        return this.http.post(settings_1.backendBaseUrl + 'cinemas/addRow/' + room_id, row, { withCredentials: true }).toPromise()
            .then(function (response) { return response.json(); });
    };
    CinemaService.prototype.editRow = function (rows) {
        return this.http.put(settings_1.backendBaseUrl + 'cinemas/rooms/rows', rows, { withCredentials: true }).toPromise()
            .then(function () { return rows; })
            .catch(function (error) {
            console.log('Error to edit row:' + error);
        });
    };
    CinemaService.prototype.deleteRow = function (id) {
        return this.http.delete(settings_1.backendBaseUrl + 'cinemas/addRow/' + id, { withCredentials: true }).toPromise()
            .then(function () { return null; })
            .catch(function (error) {
            console.log('Error to delete row:' + error);
        });
    };
    return CinemaService;
}());
CinemaService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], CinemaService);
exports.CinemaService = CinemaService;
//# sourceMappingURL=cinema.service.js.map