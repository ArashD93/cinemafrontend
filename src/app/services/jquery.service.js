"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var JqueryService = (function () {
    function JqueryService() {
    }
    JqueryService.prototype.scrollToPositionTop = function (positionTop) {
        $('html, body').animate({ scrollTop: positionTop + "px" }, 500);
    };
    JqueryService.prototype.initStickyMenu = function () {
        var headerBottom = $('#header').offset().top + $('#header').height();
        var lastScrollTop = 0;
        $(window).scroll(function (event) {
            var st = $(this).scrollTop();
            if ($(window).width() < 320) {
                if (st > lastScrollTop) {
                    // downscroll code
                    $("body").removeClass("stick");
                }
                else {
                    // upscroll code
                    $("body").addClass("stick");
                }
            }
            else {
                if (st >= headerBottom) {
                    $("body").addClass("stick");
                }
                else {
                    $("body").removeClass("stick");
                }
            }
            if (st == 0) {
                $("body").removeClass("stick");
            }
            lastScrollTop = st;
        });
    };
    JqueryService.prototype.showAddCinemaPopup = function () {
        $('#addCinemaOk').modal('show');
    };
    JqueryService.prototype.hideAddCinemaPopup = function () {
        $('#addCinemaOk').modal('hide');
    };
    JqueryService.prototype.showAddRoomPopup = function () {
        $('#addRoomOk').modal('show');
    };
    JqueryService.prototype.hideAddRoomPopup = function () {
        $('#addRoomOk').modal('hide');
    };
    JqueryService.prototype.initAddMoviePopup = function () {
        $('#myModal').modal({ show: false });
    };
    JqueryService.prototype.showAddMoviePopup = function () {
        $('#myModal').modal('show');
    };
    JqueryService.prototype.hideAddMoviePopup = function () {
        $('#myModal').modal('hide');
    };
    JqueryService.prototype.growlSuccess = function (title, message) {
        $.growl.notice({ title: title, message: message });
    };
    JqueryService.prototype.growlError = function (title, message) {
        $.growl.error({ title: title, message: message });
    };
    JqueryService.prototype.initOwlCarousel = function () {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: true
        });
    };
    JqueryService.prototype.ratingCircle = function () {
        $('.circle-chart').each(function (ci) {
            var _circle = $(this), _id = 'circle-chart' + ci, _width = _circle.data('circle-width'), _percent = _circle.data('percent'), _text = _circle.data('text');
            _percent = (_percent + '').replace('%', '');
            _width = parseInt(_width, 10);
            _circle.attr('id', _id);
            var _cc = Circles.create({
                id: _id,
                value: _percent,
                text: _text,
                radius: 100,
                width: _width,
                colors: ['rgba(255,255,255, .05)', '#fb802d'],
                duration: 1000
            });
        });
    };
    JqueryService.prototype.mainCarousel = function () {
        var _this = this;
        var $myCarousel = $('#headerslider'), $firstAnimatingElems = $myCarousel.find('.item').find("[data-animation ^= 'animated']");
        $myCarousel.carousel();
        this.doAnimations($firstAnimatingElems);
        setTimeout(function () {
            _this.ratingCircle();
        }, 1400);
        $myCarousel.on('slide.bs.carousel', function (e) {
            var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
            _this.doAnimations($animatingElems);
            setTimeout(function () {
                _this.ratingCircle();
            }, 1400);
        });
    };
    JqueryService.prototype.doAnimations = function (elems) {
        //Cache the animationend event in a variable
        var animEndEv = 'webkitAnimationEnd animationend';
        elems.each(function () {
            var $this = $(this), $animationType = $this.data('animation');
            $this.addClass($animationType).one(animEndEv, function () {
                $this.removeClass($animationType);
            }).removeClass('hidden');
        });
    };
    JqueryService.prototype.carousel = function () {
        $('.ticket-carousel').each(function () {
            var $this = $(this);
            var _col = $this.data('columns');
            //$this.find('.carousel-container').css('width', '100%');
            $this.find('.carousel-container').swiper({
                slidesPerView: '5',
                spaceBetween: 0,
                autoHeight: true,
                nextButton: $this.find('.swiper-button-next'),
                prevButton: $this.find('.swiper-button-prev'),
                breakpoints: {
                    1300: {
                        slidesPerView: 4
                    },
                    996: {
                        slidesPerView: 3
                    },
                    600: {
                        slidesPerView: 1
                    }
                }
            });
        });
    };
    JqueryService.prototype.seatClicked = function (row, seat, tickets) {
        $.growl.notice({ title: "Basket", message: "The " + row + " row / " + seat + " seat added to your basket. "
                + "You have " + tickets + " tickets in your basket." });
    };
    JqueryService.prototype.seatUnclicked = function (row, seat, tickets) {
        $.growl.warning({ title: "Basket", message: "The " + row + " row / " + seat + " seat removed from your basket. \n You have " + tickets + " tickets in your basket." });
    };
    return JqueryService;
}());
JqueryService = __decorate([
    core_1.Injectable()
], JqueryService);
exports.JqueryService = JqueryService;
//# sourceMappingURL=jquery.service.js.map