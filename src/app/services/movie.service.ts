import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Movie, ratings } from '../common/movie.interface';
import { backendBaseUrl } from '../common/settings';
import { Cinema } from '../common/cinema.interface';
import { Room } from '../common/room.interface';


import 'rxjs/add/operator/toPromise';


@Injectable()
export class MovieService {

	startPlayingTrailer : boolean;

	constructor(private http:Http) {
		this.startPlayingTrailer = false;
	}
	
	public getMovies() : Promise< Movie[] > {
		return this.http.get(backendBaseUrl + 'movies')
			.toPromise()
			.then(response => response.json() as Movie[])
	}

	public getSliderFeturedMovies () : Promise<Movie[]> {
		return this.http.get(backendBaseUrl + 'movies')	
			.toPromise()
			.then(response => response.json() as Movie[])
	}
	public getMovieById(id:number) : Promise<Movie> {
		return this.http.get(backendBaseUrl + 'movies/' + id, id)	
			.toPromise()
			.then(response => response.json() as Movie)
	}

	public getMovieFromTmdbById(id:number) : Promise<Movie> {
		return this.http.get(backendBaseUrl + 'movies/tmdb?id=' + id)
			.toPromise()
			.then(response => response.json() as Movie)
	}


	public getMoviesByCinemaId(id:number) : Promise<Movie[]> {
		return this.http.get(backendBaseUrl +'cinemas/' + id + '/movies').toPromise()
		.then(response => response.json() as Movie[])
	}

	public getRoomsByMovieId(id:number) : Promise<Room[]> {
		return this.http.get(backendBaseUrl +'movies/' + id + '/showRooms').toPromise()
		.then(response => response.json() as Room[])
	}
	
	
	public getRatings() : Promise<any[]> {
		return this.http.get(backendBaseUrl + 'movies/ratings').toPromise()
		.then(response => response.json() as any[])
	}

	/*----admin-----*/

	public deleteMovie(id:number) : Promise<void> {
		return this.http.delete(backendBaseUrl + 'movies/' + id, { withCredentials : true }).toPromise()
		    .then(() => null)
		    .catch((error: string) => {
			console.log('Error to delete movie:' + error);
	    });
	}

	public editMovie( movie : Movie ) : Promise<Movie> {
		return this.http.put(backendBaseUrl + 'movies/' + movie.id, movie, { withCredentials : true }).toPromise()
			.then(() => movie)
			.catch((error: string) => {
			console.log('Error to edit movie:' + error);
	    });
	}

	public addMovie( movie : Movie ) : Promise<Movie> {
		return this.http.post(backendBaseUrl + 'movies', movie, { withCredentials : true }).toPromise().then(response => response.json() as Movie);
	}

	public setStartPlayingTrailer( b : boolean ) : void {
		this.startPlayingTrailer = b;
	}

}