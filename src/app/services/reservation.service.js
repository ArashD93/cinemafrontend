"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var settings_1 = require("../common/settings");
require("rxjs/add/operator/toPromise");
var ReservationService = (function () {
    function ReservationService(http) {
        this.http = http;
    }
    ReservationService.prototype.getCinemaByMovie = function (movie_id) {
        return this.http.get(settings_1.backendBaseUrl + 'movies/' + movie_id + '/reservation/')
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    ReservationService.prototype.setConfirmation = function (reservationDietails, confirmationEmail) {
        var postConfi = { rowAndSeatList: reservationDietails.seats, payment_mode: reservationDietails.paymentType,
            showId: reservationDietails.showId, userId: reservationDietails.userId,
            title: confirmationEmail.title,
            price: confirmationEmail.base_ticket_price };
        return this.http.post((settings_1.backendBaseUrl + 'reservations'), postConfi)
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    ReservationService.prototype.getReservationByUserId = function (userId) {
        return this.http.get(settings_1.backendBaseUrl + 'reservations/ofuser/' + userId)
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    ReservationService.prototype.getReservationsByShowId = function (showId) {
        return this.http.get(settings_1.backendBaseUrl + 'reservations').toPromise()
            .then(function (response) { return response.json(); });
    };
    /*---------------Admin függvények-----------------*/
    ReservationService.prototype.editReservation = function (reservation) {
        return this.http.put(settings_1.backendBaseUrl, reservation, { withCredentials: true }).toPromise()
            .then(function () { return reservation; })
            .catch(function (error) {
            console.log('Error to edit reservation:' + error);
        });
    };
    ReservationService.prototype.deleteReservation = function (id) {
        return this.http.delete(settings_1.backendBaseUrl + 'reservations/' + id, { withCredentials: true }).toPromise()
            .then(function () { return null; })
            .catch(function (error) {
            console.log('Error to delete reservation:' + error);
        });
    };
    ReservationService.prototype.getReservedSeats = function (showID) {
        return this.http.get(settings_1.backendBaseUrl + 'reservations/' + showID)
            .toPromise().then(function (response) { return response.json(); });
    };
    return ReservationService;
}());
ReservationService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ReservationService);
exports.ReservationService = ReservationService;
//# sourceMappingURL=reservation.service.js.map