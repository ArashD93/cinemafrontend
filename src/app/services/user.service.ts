import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';

import { User, emptyUser } from '../common/user.interface';
import { backendBaseUrl } from '../common/settings';
import { DataService } from '../services/data.service';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {

	constructor(private http: Http, private dataService: DataService) { }

	public registerUser (user : User) : Promise<User> {
		let postData = {full_name : user.full_name, email : user.email, password : user.password};
		return this.http.post(backendBaseUrl + 'users', postData, {withCredentials: true} ).toPromise()
               .then(response => response.json() as User);
	}
	public signInUser (user : User) : Promise<any> {
		let postData = new URLSearchParams();
		postData.append('email', user.email);
		postData.append('password', user.password);
		return this.http.post(backendBaseUrl + 'login', postData, {withCredentials: true} ).toPromise();
	}
	public verifyUser (userId : number, hash: string) : Promise<User> {
		return this.http.get(backendBaseUrl + 'verify/' + userId + '/' + hash).toPromise()
			.then(response => response.json() as User);
	}
	public sendResetPasswordMail (user: User) : Promise<User> {
		var postData;
		if (this.dataService.signedInUser.email !== '') {
			postData = {email : user.email, full_name: user.full_name};
		} else {
			postData = {email : user.email};
		}
		return this.http.post(backendBaseUrl + 'users/settings/modifyPassword', postData, {withCredentials: true})
		.toPromise()
			.then(response => response.json() as User);
	}
	public forgotMailVerify(userId: number, hash: string) : Promise<User> {
		return this.http.get(backendBaseUrl + 'users/settings/modifyPassword/' + userId + '/' + hash).toPromise()
			.then(response => response.json() as User);
	}
	public putNewPassword(user: User) : Promise<User> {
		let putData = user;
		return this.http.put(backendBaseUrl + 'users/settings/modifyPassword/' + user.id + '/' + user.verify_hash,
																			putData, {withCredentials: true} ).toPromise()
			.then(response => response.json() as User);
	}
	public changeName(user: User) : Promise<User> {
		let postData = {full_name: user.full_name};
		return this.http.post(backendBaseUrl + 'users/settings/modifyName/' + user.id + '/' + user.verify_hash, postData, {withCredentials : true}).toPromise()
			.then(response => response.json() as User);
	}
	public getCurrentUser () : Promise<any> {
		return this.http.get(backendBaseUrl + 'users/current', {withCredentials: true} ).toPromise()
			.then(response => response.json() as User);
	}
	public logOutUser () : Promise<any> {
		return this.http.get(backendBaseUrl + 'logout', {withCredentials: true} ).toPromise();
	}

	public getUsers () : Promise<User[]> {
		return this.http.get(backendBaseUrl + 'users', {withCredentials: true}).toPromise()
            .then(response => response.json() as User[]);
	}

	public getUserByEmail(email:string) : Promise<User> {
		return this.http.get(backendBaseUrl + 'users/byemail', {withCredentials: true}).toPromise()
			.then(response => response.json() as User);
	}
	public getAvatar(user: User) : Promise<any> {
		return this.http.get(backendBaseUrl + 'users/' + user.id + '/getPicture').toPromise();
	}
}