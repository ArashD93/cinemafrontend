import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Cinema } from '../common/cinema.interface';
import { Room } from '../common/room.interface';
import { Row } from '../common/row.interface';
import { backendBaseUrl } from '../common/settings';
import { Movie } from '../common/movie.interface'

import 'rxjs/add/operator/toPromise';

@Injectable()
export class CinemaService {

  	constructor(private http: Http) { }

	public getCinema (id: number) : Promise<Cinema> {
		return this.http.get(backendBaseUrl + 'cinemas/' + id).toPromise()
               .then(response => response.json() as Cinema);
	}

	public getCinemas() : Promise<Cinema[]> {
		return this.http.get(backendBaseUrl + 'cinemas').toPromise()
               .then(response => response.json() as Cinema[]);
	}
	public getRooms () : Promise<Room[]> {
		return this.http.get(backendBaseUrl + 'cinemas/showRooms').toPromise()
               .then(response => response.json() as Room[]);
	}

	public getRoomsByCinemaId (cinema_id: number) : Promise<Room[]> {
		return this.http.get(backendBaseUrl + 'cinemas/showRooms/' + cinema_id).toPromise()
               .then(response => response.json() as Room[]);
	}

	public getRoomByRoomId (room_id : number) : Promise<Room> {
		return this.http.get(backendBaseUrl + 'cinemas/showOneRoom/' + room_id).toPromise()
			.then(response => response.json() as Room);
	}
	public getRowsByRoomId (room_id : number) : Promise<Row[]> {
		return this.http.get(backendBaseUrl + 'cinemas/rooms/' + room_id + '/rows').toPromise()
			.then(response => response.json() as Row[]);
	}
	/*-----------------------------------ADMIN FÜGGVÉNYEK-----------------------------------------*/

	//mozikhoz tartozó függvények

	public editCinema( cinema : Cinema ) : Promise<Cinema> {
		return this.http.put(backendBaseUrl + 'cinemas/' + cinema.id, cinema, { withCredentials : true }).toPromise()
			.then(() => cinema)
			.catch((error: string) => {
			console.log('Error to edit cinema:' + error);
	    });
	}
	public createCinema ( cinema : Cinema ) : Promise<Cinema> {
		return this.http.post(backendBaseUrl + 'cinemas', cinema, { withCredentials : true }).toPromise().then(response => response.json() as Cinema);
	}

	public deleteCinema( id: number ) : Promise<void> {
  		return this.http.delete(backendBaseUrl + 'cinemas/' + id, { withCredentials : true }).toPromise()
		    .then(() => null)
		    .catch((error: string) => {
			console.log('Error to delete cinema:' + error);
	    });
	}	

	//Termekhez tartozó függvények

	public addRoom( cinema_id : number, room : Room ) : Promise<Room> {
		return this.http.post(backendBaseUrl + 'cinemas/addRoom/' + cinema_id, room, { withCredentials : true })
			.toPromise().then(response => response.json() as Room);
	}

	public editRoom( room : Room ) : Promise<Room> {
		return this.http.put(backendBaseUrl + 'modifyRoom/' + room.id, room, { withCredentials : true }).toPromise()
			.then(() => room)
			.catch((error: string) => {
			console.log('Error to edit room:' + error);
	    });
	}

	public deleteRoom( id:number ) : Promise<void> {
		return this.http.delete(backendBaseUrl + 'modifyRoom/' + id, { withCredentials : true }).toPromise()
			.then(() => null)
			.catch((error: string) => {
			console.log('Error to delete room:' + error);
	    });
	}

	//Sorokhoz tartozó függvények

	public addRow( room_id : number, row : Row ) : Promise<Row> {
		return this.http.post(backendBaseUrl + 'cinemas/addRow/' + room_id, row, { withCredentials : true }).toPromise()
			.then(response => response.json() as Row);
	}

	public editRow( rows : Row[] ) : Promise<Row[]> {
		return this.http.put(backendBaseUrl + 'cinemas/rooms/rows', rows, { withCredentials : true }).toPromise()
			.then(() => rows)
			.catch((error: string) => {
			console.log('Error to edit row:' + error);
	    });
	}

	public deleteRow(id:number): Promise<void> {
		return this.http.delete(backendBaseUrl + 'cinemas/addRow/' + id, { withCredentials : true }).toPromise()
			.then(() => null)
			.catch((error: string) => {
			console.log('Error to delete row:' + error);
	    });
	}
	
	
}

