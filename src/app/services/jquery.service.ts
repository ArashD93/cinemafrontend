import { Injectable } from '@angular/core';

declare var $ : any;
declare var Circles : any;
declare var Swiper : any;

@Injectable()
export class JqueryService {

	scrollToPositionTop(positionTop : number) : void {
		$('html, body').animate({scrollTop : positionTop + "px"}, 500);
	}

	initStickyMenu() : void {
		var headerBottom = $('#header').offset().top + $('#header').height();
		var lastScrollTop = 0;
		$(window).scroll(function(event : any){
			var st = $(this).scrollTop();

			if( $(window).width()<320){
				if (st > lastScrollTop){
					// downscroll code
					$("body").removeClass("stick");
				} else {
					// upscroll code
					$("body").addClass("stick");
				}
			}
			else{
				if (st >= headerBottom ) {
			        $("body").addClass("stick");
			    } else {
			    	$("body").removeClass("stick");
			    }
			}
			if( st == 0 ) {
				$("body").removeClass("stick");
			}
			lastScrollTop = st;
		});
	}
	showAddCinemaPopup() : void {
		$('#addCinemaOk').modal('show');
	}
	hideAddCinemaPopup() : void {
		$('#addCinemaOk').modal('hide');
	}

	showAddRoomPopup() : void {
		$('#addRoomOk').modal('show');
	}
	hideAddRoomPopup() : void {
		$('#addRoomOk').modal('hide');
	}

	initAddMoviePopup() : void {
		$('#myModal').modal({show: false});
	}
	showAddMoviePopup() : void {
		$('#myModal').modal('show');
	}
	hideAddMoviePopup() : void {
		$('#myModal').modal('hide');
	}
	growlSuccess(title: string, message : string) : void {
		$.growl.notice({ title, message });
	}

	growlError(title: string, message : string) : void {
		$.growl.error({ title, message });
	}
	initOwlCarousel() : void{
		$('.owl-carousel').owlCarousel({
		    loop:true,
		    margin: 10,
		    nav:true
		});
	}

	ratingCircle() : void {
		$('.circle-chart').each(function(ci:any){
			var _circle = $(this),
				_id = 'circle-chart' + ci,
				_width = _circle.data('circle-width'),
				_percent = _circle.data('percent'),
				_text = _circle.data('text');

			_percent = (_percent + '').replace('%', '');
			_width = parseInt(_width, 10);

			_circle.attr('id', _id);
			var _cc = Circles.create({
				id:         _id,
				value:		_percent,
				text: 		_text,
				radius:     100,
				width:      _width,
				colors:     ['rgba(255,255,255, .05)', '#fb802d'],
				duration: 1000
			});
		});
	}

	mainCarousel () : void {
		var $myCarousel = $('#headerslider'),
		$firstAnimatingElems = $myCarousel.find('.item').find("[data-animation ^= 'animated']");
		$myCarousel.carousel();
		this.doAnimations($firstAnimatingElems);
		setTimeout(() => {
			this.ratingCircle();
		}, 1400);
		$myCarousel.on('slide.bs.carousel', (e : any) => {
			var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
			this.doAnimations($animatingElems);
			setTimeout(() => {
				this.ratingCircle();
			}, 1400);
		});
	}
	doAnimations( elems : any ) : void {
		//Cache the animationend event in a variable
		var animEndEv = 'webkitAnimationEnd animationend';
		elems.each(function () {
			var $this = $(this),
				$animationType = $this.data('animation');
			$this.addClass($animationType).one(animEndEv, function () {
				$this.removeClass($animationType);
			}).removeClass('hidden');
		});
	}

	carousel() : void {
		$('.ticket-carousel').each(function(){
			var $this = $(this);
			var _col = $this.data('columns');
			//$this.find('.carousel-container').css('width', '100%');
			$this.find('.carousel-container').swiper({
				slidesPerView: '5',
				spaceBetween: 0,
				autoHeight: true,
				nextButton: $this.find('.swiper-button-next'),
			    prevButton: $this.find('.swiper-button-prev'),
			    breakpoints: {
			    	1300: {
			    		slidesPerView: 4
			    	},
			    	996: {
			    		slidesPerView: 3
			    	},
			    	600: {
			    		slidesPerView: 1
			    	}
			    }
			});
		});
	}
	seatClicked(row: number, seat: number, tickets: number) : void {
		$.growl.notice({ title: "Basket", message: "The " + row + " row / " + seat + " seat added to your basket. "
			+ "You have " + tickets + " tickets in your basket." });
	}

	seatUnclicked(row: number, seat: number, tickets: number) : void {
		$.growl.warning({ title: "Basket",message: "The " + row + " row / " + seat + " seat removed from your basket. \n You have " + tickets + " tickets in your basket."  });
	}
}