import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';

import { User, emptyUser } from '../common/user.interface';
import { backendBaseUrl } from '../common/settings';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class DataService {

	signedInUser: User;
	scrollToBuy: boolean; 
	newPassAsker: User;

	constructor () {
		this.signedInUser = JSON.parse(JSON.stringify(emptyUser));
		this.scrollToBuy = false;
		this.newPassAsker = JSON.parse(JSON.stringify(emptyUser));
	}

}