import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { backendBaseUrl } from '../common/settings';
import { Row } from '../common/row.interface'

import 'rxjs/add/operator/toPromise';

@Injectable()
export class RowService {


	constructor( private http : Http){

	}
	public getRowsByRoomId (room_id:number) : Promise<Row[]>{
		return this.http.get(backendBaseUrl + 'cinemas/rooms/'+ room_id + '/rows')
		.toPromise()
		.then(respose => respose.json() as Row[]);
	}
}

