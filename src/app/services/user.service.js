"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var settings_1 = require("../common/settings");
var data_service_1 = require("../services/data.service");
require("rxjs/add/operator/toPromise");
var UserService = (function () {
    function UserService(http, dataService) {
        this.http = http;
        this.dataService = dataService;
    }
    UserService.prototype.registerUser = function (user) {
        var postData = { full_name: user.full_name, email: user.email, password: user.password };
        return this.http.post(settings_1.backendBaseUrl + 'users', postData, { withCredentials: true }).toPromise()
            .then(function (response) { return response.json(); });
    };
    UserService.prototype.signInUser = function (user) {
        var postData = new http_1.URLSearchParams();
        postData.append('email', user.email);
        postData.append('password', user.password);
        return this.http.post(settings_1.backendBaseUrl + 'login', postData, { withCredentials: true }).toPromise();
    };
    UserService.prototype.verifyUser = function (userId, hash) {
        return this.http.get(settings_1.backendBaseUrl + 'verify/' + userId + '/' + hash).toPromise()
            .then(function (response) { return response.json(); });
    };
    UserService.prototype.sendResetPasswordMail = function (user) {
        var postData;
        if (this.dataService.signedInUser.email !== '') {
            postData = { email: user.email, full_name: user.full_name };
        }
        else {
            postData = { email: user.email };
        }
        return this.http.post(settings_1.backendBaseUrl + 'users/settings/modifyPassword', postData, { withCredentials: true })
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    UserService.prototype.forgotMailVerify = function (userId, hash) {
        return this.http.get(settings_1.backendBaseUrl + 'users/settings/modifyPassword/' + userId + '/' + hash).toPromise()
            .then(function (response) { return response.json(); });
    };
    UserService.prototype.putNewPassword = function (user) {
        var putData = user;
        return this.http.put(settings_1.backendBaseUrl + 'users/settings/modifyPassword/' + user.id + '/' + user.verify_hash, putData, { withCredentials: true }).toPromise()
            .then(function (response) { return response.json(); });
    };
    UserService.prototype.changeName = function (user) {
        var postData = { full_name: user.full_name };
        return this.http.post(settings_1.backendBaseUrl + 'users/settings/modifyName/' + user.id + '/' + user.verify_hash, postData, { withCredentials: true }).toPromise()
            .then(function (response) { return response.json(); });
    };
    UserService.prototype.getCurrentUser = function () {
        return this.http.get(settings_1.backendBaseUrl + 'users/current', { withCredentials: true }).toPromise()
            .then(function (response) { return response.json(); });
    };
    UserService.prototype.logOutUser = function () {
        return this.http.get(settings_1.backendBaseUrl + 'logout', { withCredentials: true }).toPromise();
    };
    UserService.prototype.getUsers = function () {
        return this.http.get(settings_1.backendBaseUrl + 'users', { withCredentials: true }).toPromise()
            .then(function (response) { return response.json(); });
    };
    UserService.prototype.getUserByEmail = function (email) {
        return this.http.get(settings_1.backendBaseUrl + 'users/byemail', { withCredentials: true }).toPromise()
            .then(function (response) { return response.json(); });
    };
    UserService.prototype.getAvatar = function (user) {
        return this.http.get(settings_1.backendBaseUrl + 'users/' + user.id + '/getPicture').toPromise();
    };
    return UserService;
}());
UserService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, data_service_1.DataService])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map