"use strict";
var ReservationMethod;
(function (ReservationMethod) {
    ReservationMethod[ReservationMethod["reservation"] = 1] = "reservation";
    ReservationMethod[ReservationMethod["buy"] = 0] = "buy";
})(ReservationMethod = exports.ReservationMethod || (exports.ReservationMethod = {}));
exports.emptyTicketBuying = {
    seats: [],
    paymentType: 0,
    showId: 1,
    userId: 7
};
//# sourceMappingURL=ticketBuying.interface.js.map