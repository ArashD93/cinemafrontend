import { Movie } from './movie.interface';

export interface Show {
	id: number;
	is_premiere: boolean;
	timestamp: string;
	movie_id: number;
	movie?: Movie;
	room_id: number;
	cinema_id? : number;
}

export const emptyShow : Show = {
	id: 0,
	is_premiere: true ,
	timestamp: null,
	movie_id: 0,
	room_id: 0,	
};