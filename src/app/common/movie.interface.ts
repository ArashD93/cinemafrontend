import { Show } from './show.interface';

export interface Movie {
	id: number;
	title: string;
	description: string;
	length: number;
	rated : string;
	director : string;
	actors: string;
	categoryList: string[];
	imdb_rating : number;
	trailer_url : string;
	thumbnail_url : string;
	hero_image_url : string;
	background_image_url : string;
	is_featured_in_slider : boolean;
	release_date: number;
	shows?: Show[];
} 

export const emptyMovie : Movie = {
	id: 0,
	title: "",
	description: "",
	length: 0,
	rated : "",
	director : "",
	actors: "",
	categoryList:[],
	imdb_rating : 0,
	trailer_url : '',
	thumbnail_url : '',
	hero_image_url : '',
	background_image_url : '',
	is_featured_in_slider : false,
	release_date: 0
};

export const ratings : any[] = [
	{ key : 'A12', value : 'Child movie (12+)' },
	{ key : 'A16', value : 'Young adult movie (16+)' },
	{ key : 'A18', value : 'Adult movie (18+)' }
];
