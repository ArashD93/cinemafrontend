"use strict";
exports.emptyRow = {
    id: 0,
    room_id: 0,
    seats: 0,
    is_sofa: false
};
exports.dummyRow = {
    id: 1,
    room_id: 1,
    seats: 26,
    is_sofa: false
};
exports.dummyRow2 = {
    id: 2,
    room_id: 1,
    seats: 17,
    is_sofa: false
};
//# sourceMappingURL=row.interface.js.map