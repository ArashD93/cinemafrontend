export interface Row {
	id?: number;
	room_id: number;
	seats: number;
	is_sofa: boolean;
	rowNumber?: number;
}
export const emptyRow : Row = {
	id : 0,
	room_id: 0,
	seats: 0,
	is_sofa: false
}
export const dummyRow : Row = {
	id: 1,
	room_id: 1,
	seats: 26,
	is_sofa: false
}
export const dummyRow2 : Row = {
	id: 2,
	room_id: 1,
	seats: 17,
	is_sofa: false
};