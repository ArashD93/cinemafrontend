import { Row } from './row.interface'

export interface Room {
	id?: number;
	name: string;
	cinema_id: number;
	rows?: Row[];
}

export const emptyRoom : Room = {
	id : 0,
	name : '',
	cinema_id: 0,
	rows: []
};