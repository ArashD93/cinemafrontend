import { Room } from './room.interface'

export interface Cinema {
	id?: number;
	name: string;
	description: string;
	address: string;
	gps_lat: number;
	gps_lng: number;
	email: string;
	phone: string;
	base_ticket_price: number;
	rooms?: Room[];
}

export const emptyCinema : Cinema = {
	id : 0,
	name : '',
	description : '',
	address : '',
	gps_lat : 0,
	gps_lng : 0,
	email : '',
	phone : '',
	base_ticket_price : 0,
	rooms : []
};