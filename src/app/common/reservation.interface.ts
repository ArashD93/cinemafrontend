import { Show } from './show.interface'
import { Cinema } from './cinema.interface'

export interface Reservation {
	cinemaDTO: Cinema,
	showsDTO: Show[]
}