"use strict";
exports.emptyCinema = {
    id: 0,
    name: '',
    description: '',
    address: '',
    gps_lat: 0,
    gps_lng: 0,
    email: '',
    phone: '',
    base_ticket_price: 0,
    rooms: []
};
//# sourceMappingURL=cinema.interface.js.map