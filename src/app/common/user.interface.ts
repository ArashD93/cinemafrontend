export interface User {
	id: number;
	email: string;
	password: string;
	full_name: string;
	verify_hash: string;
	file?: any;
}

export const emptyUser : User = {
	id : 0,
	email : '',
	password : '',
	full_name : '',
	verify_hash : ''
};