import { Ticket } from '../common/ticket.interface';
import { Cinema } from '../common/cinema.interface';
import { Movie } from '../common/movie.interface';

export interface Order {

	seats: Ticket[];
	title: string;
	base_ticket_price: number;

}

export const emptyOrder : Order = {
	seats : [],
	title : "",
	base_ticket_price: 0
}

