import { Row } from '../common/row.interface';
import { Show } from '../common/show.interface';
import { Ticket } from '../common/ticket.interface';

export enum ReservationMethod {
	reservation = 1,
	buy = 0,
}

export interface TicketBuying {

	seats: Ticket[];
	paymentType: ReservationMethod;
	showId: number;
	userId: number;

}

export const emptyTicketBuying : TicketBuying = {
	seats: [],
	paymentType: 0,
	showId: 1,
	userId: 7

}