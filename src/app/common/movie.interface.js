"use strict";
exports.emptyMovie = {
    id: 0,
    title: "",
    description: "",
    length: 0,
    rated: "",
    director: "",
    actors: "",
    categoryList: [],
    imdb_rating: 0,
    trailer_url: '',
    thumbnail_url: '',
    hero_image_url: '',
    background_image_url: '',
    is_featured_in_slider: false,
    release_date: 0
};
exports.ratings = [
    { key: 'A12', value: 'Child movie (12+)' },
    { key: 'A16', value: 'Young adult movie (16+)' },
    { key: 'A18', value: 'Adult movie (18+)' }
];
//# sourceMappingURL=movie.interface.js.map