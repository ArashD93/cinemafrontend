import { Component, OnInit } from '@angular/core';

import { Cinema, emptyCinema } from '../../common/cinema.interface';

import { Room } from '../../common/room.interface';

import { CinemaService } from '../../services/cinema.service';
import { JqueryService } from '../../services/jquery.service';
import { googleMapsStyle } from '../../common/settings';

declare var google: any;

@Component({
  selector: 'cinemas',
  templateUrl: './cinemas.component.html'
})
export class CinemasComponent implements OnInit {
	cinemas: Cinema[];
	roomsTemp : Room[];

	constructor(private cinService: CinemaService, private jqueryService: JqueryService) {
		this.cinemas = [];
		this.roomsTemp = [];
	}

	getCinemasFromService() : void {
		let cinemasPromise = this.cinService.getCinemas()
		.then( (cinemas : Cinema[]) => {
			this.cinemas = cinemas;
		} );
		let roomsPromise = this.cinService.getRooms()
		.then( (rooms : Room[]) => {
			this.roomsTemp = rooms;
		});
		Promise.all([cinemasPromise, roomsPromise]).then(() => {
			for(var i = 0; i < this.cinemas.length; i++) {
				this.cinemas[i].rooms = [];
				for(var j = 0; j < this.roomsTemp.length; j++) {
					if (this.cinemas[i].id == this.roomsTemp[j].cinema_id) {
						this.cinemas[i].rooms.push(this.roomsTemp[j]);
					}
				}
			}
			setTimeout(()=>{
				for(var i = 0; i < this.cinemas.length; i++) {
					let mapDiv = document.getElementById('cinema-map-' + this.cinemas[i].id);
					let cinemaPosition = { lat: this.cinemas[i].gps_lat, lng: this.cinemas[i].gps_lng };
					let map = new google.maps.Map(mapDiv, {
						zoom: 14,
						center: cinemaPosition,
						mapType : 'roadmap',
						styles : googleMapsStyle
					});
					let marker = new google.maps.Marker({
          				position: cinemaPosition,
          				map: map
        			});
				}
			}, 20);
		}).catch( (error) => {
			console.log('error when getting cinemas:' + error);
		});
	}


	ngOnInit(): void {
		this.jqueryService.scrollToPositionTop(0);
		this.getCinemasFromService();
  	}
}
