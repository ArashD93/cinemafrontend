"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var cinema_service_1 = require("../../services/cinema.service");
var jquery_service_1 = require("../../services/jquery.service");
var settings_1 = require("../../common/settings");
var CinemasComponent = (function () {
    function CinemasComponent(cinService, jqueryService) {
        this.cinService = cinService;
        this.jqueryService = jqueryService;
        this.cinemas = [];
        this.roomsTemp = [];
    }
    CinemasComponent.prototype.getCinemasFromService = function () {
        var _this = this;
        var cinemasPromise = this.cinService.getCinemas()
            .then(function (cinemas) {
            _this.cinemas = cinemas;
        });
        var roomsPromise = this.cinService.getRooms()
            .then(function (rooms) {
            _this.roomsTemp = rooms;
        });
        Promise.all([cinemasPromise, roomsPromise]).then(function () {
            for (var i = 0; i < _this.cinemas.length; i++) {
                _this.cinemas[i].rooms = [];
                for (var j = 0; j < _this.roomsTemp.length; j++) {
                    if (_this.cinemas[i].id == _this.roomsTemp[j].cinema_id) {
                        _this.cinemas[i].rooms.push(_this.roomsTemp[j]);
                    }
                }
            }
            setTimeout(function () {
                for (var i = 0; i < _this.cinemas.length; i++) {
                    var mapDiv = document.getElementById('cinema-map-' + _this.cinemas[i].id);
                    var cinemaPosition = { lat: _this.cinemas[i].gps_lat, lng: _this.cinemas[i].gps_lng };
                    var map = new google.maps.Map(mapDiv, {
                        zoom: 14,
                        center: cinemaPosition,
                        mapType: 'roadmap',
                        styles: settings_1.googleMapsStyle
                    });
                    var marker = new google.maps.Marker({
                        position: cinemaPosition,
                        map: map
                    });
                }
            }, 20);
        }).catch(function (error) {
            console.log('error when getting cinemas:' + error);
        });
    };
    CinemasComponent.prototype.ngOnInit = function () {
        this.jqueryService.scrollToPositionTop(0);
        this.getCinemasFromService();
    };
    return CinemasComponent;
}());
CinemasComponent = __decorate([
    core_1.Component({
        selector: 'cinemas',
        templateUrl: './cinemas.component.html'
    }),
    __metadata("design:paramtypes", [cinema_service_1.CinemaService, jquery_service_1.JqueryService])
], CinemasComponent);
exports.CinemasComponent = CinemasComponent;
//# sourceMappingURL=cinemas.component.js.map