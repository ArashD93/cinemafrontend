import { Component } from '@angular/core';

import { JqueryService } from '../../services/jquery.service';

@Component({
	selector: 'cinema-footer',
	templateUrl: './footer.component.html'
})

export class FooterComponent{

	constructor(private jqueryService:JqueryService) {}
}

