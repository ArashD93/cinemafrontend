"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var cinema_service_1 = require("../../services/cinema.service");
var movie_service_1 = require("../../services/movie.service");
var reservation_service_1 = require("../../services/reservation.service");
var router_1 = require("@angular/router");
var ReservationComponent = (function () {
    function ReservationComponent(cinemaService, reservationService, movieService, router) {
        this.cinemaService = cinemaService;
        this.reservationService = reservationService;
        this.movieService = movieService;
        this.router = router;
        this.isDisabled = true;
        this.shows = [];
        this.reservations = [];
        this.selectedShowId = 0;
    }
    ReservationComponent.prototype.getRegularToReservation = function () {
        var _this = this;
        this.reservationService.getCinemaByMovie(this.movie_id)
            .then(function (reservations) {
            _this.reservations = reservations;
        })
            .catch(function (reponse) {
            console.log('Error when getting reservations...');
        });
    };
    ReservationComponent.prototype.ngOnInit = function () {
        this.getRegularToReservation();
    };
    ReservationComponent.prototype.onSelectedItem = function () {
        for (var _i = 0, _a = this.reservations; _i < _a.length; _i++) {
            var reservation = _a[_i];
            if (this.selectedCinemaId == reservation.cinemaDTO.id) {
                this.shows = reservation.showsDTO;
                this.isDisabled = false;
            }
        }
    };
    ReservationComponent.prototype.getDistinctDates = function () {
        var dateSet = new Set();
        for (var _i = 0, _a = this.shows; _i < _a.length; _i++) {
            var x = _a[_i];
            var movieDate = new Date(x.timestamp);
            dateSet.add(movieDate.getFullYear() + "-" + (movieDate.getMonth() + 1) + "-" + movieDate.getDate());
        }
        return Array.from(dateSet);
    };
    ReservationComponent.prototype.getShowsForDate = function (date) {
        var shows = [];
        for (var _i = 0, _a = this.shows; _i < _a.length; _i++) {
            var times = _a[_i];
            shows.push(times);
        }
        return shows;
    };
    return ReservationComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], ReservationComponent.prototype, "movie_id", void 0);
ReservationComponent = __decorate([
    core_1.Component({
        selector: 'reservation',
        templateUrl: './reservation.component.html'
    }),
    __metadata("design:paramtypes", [cinema_service_1.CinemaService,
        reservation_service_1.ReservationService,
        movie_service_1.MovieService,
        router_1.Router])
], ReservationComponent);
exports.ReservationComponent = ReservationComponent;
//# sourceMappingURL=reservation.component.js.map