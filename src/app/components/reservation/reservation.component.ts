import { Component,OnInit,Input } from '@angular/core';
import { Cinema } from '../../common/cinema.interface';
import { CinemaService } from '../../services/cinema.service';
import { Movie } from '../../common/movie.interface';
import { MovieService } from '../../services/movie.service';
import { Reservation } from '../../common/reservation.interface';
import { ReservationService } from '../../services/reservation.service';
import { Show, emptyShow } from '../../common/show.interface'
import { Router } from '@angular/router';

@Component({
  selector: 'reservation',
  templateUrl: './reservation.component.html'
})

export class ReservationComponent implements OnInit {
	@Input()
	movie_id: number;
	selectedDate : Date;
	selectedCinemaId : number;
	reservations : Reservation[];
	shows : Show[];
	isDisabled : boolean=true;
	selectedShowId : number;

	constructor(
		private cinemaService: CinemaService,
		private reservationService: ReservationService,
		private movieService: MovieService,
		private router: Router ){
			this.shows = [];
			this.reservations= [];
			this.selectedShowId = 0;
		}
	
	getRegularToReservation() : void {
		this.reservationService.getCinemaByMovie(this.movie_id)
		.then( (reservations: Reservation[]) => {
			this.reservations = reservations;
		})
		.catch(reponse =>{
			console.log('Error when getting reservations...');
		});
	}
	

	ngOnInit() : void {
		this.getRegularToReservation();
	}

	private onSelectedItem() : void {
		for (let reservation of this.reservations){
			if(this.selectedCinemaId == reservation.cinemaDTO.id ){
				this.shows = reservation.showsDTO;
				this.isDisabled = false;
			}
		}
	}

	private getDistinctDates() : string[]{
		let dateSet = new Set<string>();
		for(let x of this.shows){
			let movieDate = new Date(x.timestamp);
			dateSet.add( movieDate.getFullYear() + "-" + (movieDate.getMonth() + 1) + "-" + movieDate.getDate() );
		}
		return Array.from(dateSet);	

	}

	private getShowsForDate( date : string ) : Show[] {
		let shows : Show[] = [];
		for(let times of this.shows){
			shows.push(times);
		}
		return shows;

	} 

}

