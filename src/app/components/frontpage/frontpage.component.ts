import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Movie } from '../../common/movie.interface';
import { MovieService } from '../../services/movie.service';
import { Cinema } from '../../common/cinema.interface';
import { CinemaService } from '../../services/cinema.service';
import { googleMapsStyle } from '../../common/settings';
import { JqueryService } from '../../services/jquery.service';
import { Show } from '../../common/show.interface';
import { ShowService } from '../../services/show.service';
import { DataService } from '../../services/data.service';
import { WindowRef, Scroll } from '../../services/window.service';

declare var google: any;
declare var $: any;

@Component({
	selector: 'frontpage',
	templateUrl: './frontpage.component.html'
})

export class FrontpageComponent implements OnInit{
	cinemas: Cinema[];
	sliderFeauturedMovies : Movie[];
	premierShows: Show[];
	premierMovies: Movie[];

	constructor(private movService: MovieService, 
				private cinService: CinemaService,
				private router: Router,
				private jqueryService: JqueryService,
				private showService: ShowService,
				private scroll : Scroll,
				private dataService: DataService) {
		this.sliderFeauturedMovies = [];
		this.cinemas = [];
		this.premierShows = [];
		this.premierMovies = [];
	}

	getSliderFeaturedMovieFromService() : void {
		this.movService.getSliderFeturedMovies()
		.then( (sliderFeaturedMovies : Movie []) => {
			this.sliderFeauturedMovies = sliderFeaturedMovies;
			for(let i = this.sliderFeauturedMovies.length-1; i >=0 ; i--) {
				let rDateYear = new Date(this.sliderFeauturedMovies[i].release_date).getFullYear();
				this.sliderFeauturedMovies[i].release_date = rDateYear;
				if (this.sliderFeauturedMovies[i].is_featured_in_slider == false) {
					this.sliderFeauturedMovies.splice(i, 1);
				}
			}
			setTimeout(()=>{
				//this.jqueryService.ratingCircle();
				this.jqueryService.mainCarousel();
				//this.jqueryService.animateOnCarouselSlideEvent();
			}, 20);
		} )
		.catch( response => {
			console.log(response);
		});
	}
	getPremiers() : void {
		let showPromise = this.showService.getShows()
		.then( (premierShows: Show[]) => {
			this.premierShows = premierShows;
			for(let i = this.premierShows.length-1; i >= 0; i--) {
				if (this.premierShows[i].is_premiere == false) {
					this.premierShows.splice(i, 1);
				}
			}

		} );
		let moviePromise = this.movService.getMovies()
		.then( (premierMovies: Movie[]) => {
			this.premierMovies = premierMovies;
		});
		Promise.all([showPromise, moviePromise])
		.then( () => {
			for (let i=this.premierMovies.length-1; i>=0; i--) {
				let isPremiere = false;
				for(let j=0; j<this.premierShows.length; j++) {
					if (this.premierShows[j].is_premiere && this.premierShows[j].movie_id == this.premierMovies[i].id) {
						isPremiere = true;
						break;
					}
				}
				if(! isPremiere) {
					this.premierMovies.splice(i,1);
				}
			}
			setTimeout(()=>{
				this.jqueryService.carousel();
			}, 20);
		})
		.catch( response => {
			console.log(response);
		});
	}
	getCinemasForMap() : void {
		this.cinService.getCinemas()
		.then( (cinemas : Cinema[]) => {
			this.cinemas = cinemas;
			setTimeout(()=>{
				let mapDiv = document.getElementById('cinema-map');
				let bounds = new google.maps.LatLngBounds();
				let map = new google.maps.Map(mapDiv, {
					center: { lat: 47.49801, lng: 19.03991 },
					mapType : 'roadmap',
					styles : googleMapsStyle,
					scrollwheel: false,
				});
				let markers : any[] = [];
				for (var i = 0; i < this.cinemas.length; i++) {
					let contentString = '<div class="gmap_content prototype">'+
											'<div class="gmap-item">'+
							                    '<h4>'+ cinemas[i].name +'</h4>'+
							                '</div>'+
							                '<div class="gmap-item">'+
							                    '<label class="label-title">Keep in Touch</label>'+
							                '</div>'+
							                '<div class="gmap-item">'+
							                    '<label>'+
							                        '<i class="fa fa-map-marker"></i>'+
							                    '</label>'+
							                    '<span>Address: '+ cinemas[i].address +'</span>'+
							                '</div>'+
							                '<div class="gmap-item">'+
							                    '<label>'+
							                        '<i class="fa fa-phone"></i>'+
							                    '</label>'+
							                    '<span>Phone: '+ cinemas[i].phone +'</span>'+
							                '</div>'+
							                '<div class="gmap-item">'+
							                    '<label>'+
							                        '<i class="fa fa-envelope"></i>'+
							                    '</label>'+
							                    '<span>Email: '+ cinemas[i].email +'</span>'+
							                '</div>'+
							            '</div>';
					let infowindow = new google.maps.InfoWindow({
          				content: contentString
        			});
					let marker = new google.maps.Marker({
          				position: { lat: this.cinemas[i].gps_lat, lng: this.cinemas[i].gps_lng },
          				map: map,
          				icon: 'http://cinema.project.progmatic.hu/page-assets/img/cinema-logo-new-for-map.png',
          				opacity: 0
          				// animation: google.maps.Animation.DROP
        			});
					bounds.extend(marker.position);
        			marker.addListener('click', function() {
						infowindow.open(map, marker);
					});
					markers.push(marker);
				}
				map.fitBounds(bounds);
				google.maps.event.addDomListener(window, "resize", function() {
				   var center = map.getCenter();
				   google.maps.event.trigger(map, "resize");
				   map.setCenter(center); 
				});
				var $w = $(window),
					$mapContainer = $(mapDiv),
					markersDropped = false;
				$w.scroll(()=>{
					if ($w.scrollTop() + $w.height() > $mapContainer.offset().top + 100) {
						if (! markersDropped) {
							for (var i = 0; i < markers.length; i++) {
								markers[i].setAnimation(google.maps.Animation.DROP);
								markers[i].setOpacity(1);
							}
							markersDropped = true;
						}
					}
				});
			},20);
		} )
		.catch( response => {
			console.log(response);
		} );
	}

	buyThisShow(id: number) : void {
		for (let i = 0; i < this.premierShows.length; i++) {
			if (id == this.premierShows[i].movie_id) {
				this.router.navigate(['/booking', this.premierShows[i].id]);
				break;
			}
		}
	}

	ngOnInit() : void {
		this.jqueryService.scrollToPositionTop(0);
		this.getSliderFeaturedMovieFromService();
		this.getCinemasForMap();
		this.getPremiers();
	}

	watchTrailer(movie : Movie) : void {
		this.movService.setStartPlayingTrailer(true);
		console.log(this.movService);
		this.router.navigate(['/movie', movie.id]);
	}
}