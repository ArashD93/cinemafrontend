"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var movie_service_1 = require("../../services/movie.service");
var cinema_service_1 = require("../../services/cinema.service");
var settings_1 = require("../../common/settings");
var jquery_service_1 = require("../../services/jquery.service");
var show_service_1 = require("../../services/show.service");
var data_service_1 = require("../../services/data.service");
var window_service_1 = require("../../services/window.service");
var FrontpageComponent = (function () {
    function FrontpageComponent(movService, cinService, router, jqueryService, showService, scroll, dataService) {
        this.movService = movService;
        this.cinService = cinService;
        this.router = router;
        this.jqueryService = jqueryService;
        this.showService = showService;
        this.scroll = scroll;
        this.dataService = dataService;
        this.sliderFeauturedMovies = [];
        this.cinemas = [];
        this.premierShows = [];
        this.premierMovies = [];
    }
    FrontpageComponent.prototype.getSliderFeaturedMovieFromService = function () {
        var _this = this;
        this.movService.getSliderFeturedMovies()
            .then(function (sliderFeaturedMovies) {
            _this.sliderFeauturedMovies = sliderFeaturedMovies;
            for (var i = _this.sliderFeauturedMovies.length - 1; i >= 0; i--) {
                var rDateYear = new Date(_this.sliderFeauturedMovies[i].release_date).getFullYear();
                _this.sliderFeauturedMovies[i].release_date = rDateYear;
                if (_this.sliderFeauturedMovies[i].is_featured_in_slider == false) {
                    _this.sliderFeauturedMovies.splice(i, 1);
                }
            }
            setTimeout(function () {
                //this.jqueryService.ratingCircle();
                _this.jqueryService.mainCarousel();
                //this.jqueryService.animateOnCarouselSlideEvent();
            }, 20);
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    FrontpageComponent.prototype.getPremiers = function () {
        var _this = this;
        var showPromise = this.showService.getShows()
            .then(function (premierShows) {
            _this.premierShows = premierShows;
            for (var i = _this.premierShows.length - 1; i >= 0; i--) {
                if (_this.premierShows[i].is_premiere == false) {
                    _this.premierShows.splice(i, 1);
                }
            }
        });
        var moviePromise = this.movService.getMovies()
            .then(function (premierMovies) {
            _this.premierMovies = premierMovies;
        });
        Promise.all([showPromise, moviePromise])
            .then(function () {
            for (var i = _this.premierMovies.length - 1; i >= 0; i--) {
                var isPremiere = false;
                for (var j = 0; j < _this.premierShows.length; j++) {
                    if (_this.premierShows[j].is_premiere && _this.premierShows[j].movie_id == _this.premierMovies[i].id) {
                        isPremiere = true;
                        break;
                    }
                }
                if (!isPremiere) {
                    _this.premierMovies.splice(i, 1);
                }
            }
            setTimeout(function () {
                _this.jqueryService.carousel();
            }, 20);
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    FrontpageComponent.prototype.getCinemasForMap = function () {
        var _this = this;
        this.cinService.getCinemas()
            .then(function (cinemas) {
            _this.cinemas = cinemas;
            setTimeout(function () {
                var mapDiv = document.getElementById('cinema-map');
                var bounds = new google.maps.LatLngBounds();
                var map = new google.maps.Map(mapDiv, {
                    center: { lat: 47.49801, lng: 19.03991 },
                    mapType: 'roadmap',
                    styles: settings_1.googleMapsStyle,
                    scrollwheel: false,
                });
                var markers = [];
                var _loop_1 = function () {
                    var contentString = '<div class="gmap_content prototype">' +
                        '<div class="gmap-item">' +
                        '<h4>' + cinemas[i].name + '</h4>' +
                        '</div>' +
                        '<div class="gmap-item">' +
                        '<label class="label-title">Keep in Touch</label>' +
                        '</div>' +
                        '<div class="gmap-item">' +
                        '<label>' +
                        '<i class="fa fa-map-marker"></i>' +
                        '</label>' +
                        '<span>Address: ' + cinemas[i].address + '</span>' +
                        '</div>' +
                        '<div class="gmap-item">' +
                        '<label>' +
                        '<i class="fa fa-phone"></i>' +
                        '</label>' +
                        '<span>Phone: ' + cinemas[i].phone + '</span>' +
                        '</div>' +
                        '<div class="gmap-item">' +
                        '<label>' +
                        '<i class="fa fa-envelope"></i>' +
                        '</label>' +
                        '<span>Email: ' + cinemas[i].email + '</span>' +
                        '</div>' +
                        '</div>';
                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker = new google.maps.Marker({
                        position: { lat: _this.cinemas[i].gps_lat, lng: _this.cinemas[i].gps_lng },
                        map: map,
                        icon: 'http://cinema.project.progmatic.hu/page-assets/img/cinema-logo-new-for-map.png',
                        opacity: 0
                    });
                    bounds.extend(marker.position);
                    marker.addListener('click', function () {
                        infowindow.open(map, marker);
                    });
                    markers.push(marker);
                };
                for (var i = 0; i < _this.cinemas.length; i++) {
                    _loop_1();
                }
                map.fitBounds(bounds);
                google.maps.event.addDomListener(window, "resize", function () {
                    var center = map.getCenter();
                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center);
                });
                var $w = $(window), $mapContainer = $(mapDiv), markersDropped = false;
                $w.scroll(function () {
                    if ($w.scrollTop() + $w.height() > $mapContainer.offset().top + 100) {
                        if (!markersDropped) {
                            for (var i = 0; i < markers.length; i++) {
                                markers[i].setAnimation(google.maps.Animation.DROP);
                                markers[i].setOpacity(1);
                            }
                            markersDropped = true;
                        }
                    }
                });
            }, 20);
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    FrontpageComponent.prototype.buyThisShow = function (id) {
        for (var i = 0; i < this.premierShows.length; i++) {
            if (id == this.premierShows[i].movie_id) {
                this.router.navigate(['/booking', this.premierShows[i].id]);
                break;
            }
        }
    };
    FrontpageComponent.prototype.ngOnInit = function () {
        this.jqueryService.scrollToPositionTop(0);
        this.getSliderFeaturedMovieFromService();
        this.getCinemasForMap();
        this.getPremiers();
    };
    FrontpageComponent.prototype.watchTrailer = function (movie) {
        this.movService.setStartPlayingTrailer(true);
        console.log(this.movService);
        this.router.navigate(['/movie', movie.id]);
    };
    return FrontpageComponent;
}());
FrontpageComponent = __decorate([
    core_1.Component({
        selector: 'frontpage',
        templateUrl: './frontpage.component.html'
    }),
    __metadata("design:paramtypes", [movie_service_1.MovieService,
        cinema_service_1.CinemaService,
        router_1.Router,
        jquery_service_1.JqueryService,
        show_service_1.ShowService,
        window_service_1.Scroll,
        data_service_1.DataService])
], FrontpageComponent);
exports.FrontpageComponent = FrontpageComponent;
//# sourceMappingURL=frontpage.component.js.map