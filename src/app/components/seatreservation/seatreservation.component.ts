import { Component, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { Row } from '../../common/row.interface';
import { Room } from '../../common/room.interface';
import { Ticket } from '../../common/ticket.interface';

import { JqueryService } from '../../services/jquery.service';
import { RowService } from '../../services/row.service';
import { Cinema } from '../../common/cinema.interface';
import { ReservationMethod } from '../../common/ticketBuying.interface';

@Component({
  selector: 'seatreservation',
  templateUrl: './seatreservation.component.html'
})

export class SeatReservationComponent implements OnInit {

	@Output() complete = new EventEmitter<any>();
	@Output() back = new EventEmitter<any>();
	@Output() ticket = new EventEmitter<Ticket[]>();
	@Output() startMakeTicketBuying = new EventEmitter<any>();
	@Input() room: Room;
	@Input() cinema: Cinema;
	@Input() selectedTickets: Ticket[];
	@Input() reservedSeats: Ticket[];

	done: boolean = false;
	backClicked: boolean = false;
	rows: Row[];
	Arr = Array;
	num:number;
	checkedCounter: number;
	actualSeat : Ticket;
	seatCheck : Ticket[];
	reservationCheck : boolean = true;

	constructor( private jqueryService : JqueryService, private rowService : RowService ){
		this.rows = [];
		this.seatCheck = [];
	}

	makeTicketBuying() {
		this.startMakeTicketBuying.emit();
	}

	sendTicketsData() : void {
		this.ticket.emit(this.selectedTickets);
	}

	onChange(e : any){
		let actualRow = parseInt(e.target.parentElement.dataset.row)+1;
		let actualSeat = parseInt(e.target.dataset.seat)+1;
		this.actualSeat = { rowId : actualRow, seat : actualSeat };
			if (e.target.checked){
					this.checkedCounter++;
					this.jqueryService.seatClicked(actualRow, actualSeat, this.checkedCounter);
					this.selectedTickets.push({ rowId : actualRow, seat : actualSeat });				
				

			} else {
				this.checkedCounter--;
				this.jqueryService.seatUnclicked(actualRow, actualSeat, this.checkedCounter);
				for (var i = 0; i < this.selectedTickets.length; ++i) {
					if(this.selectedTickets[i].rowId == actualRow && this.selectedTickets[i].seat == actualSeat) {
						this.selectedTickets.splice(i, 1);
					}
				}	
			}
	}

	isReservedSeat(rowNumber : number, seatNumber : number) : boolean {
		for (var i = 0; i < this.reservedSeats.length; ++i) {
			if (rowNumber == this.reservedSeats[i].rowNumber-1 && seatNumber == this.reservedSeats[i].seat-1) {
				return true;
			}
		}
		return false;
	}

	generateRowsFromService() : void {

		this.rowService.getRowsByRoomId(this.room.id)
		.then( (rows : Row[]) => {
			this.rows = rows;
		} ).catch(response => {
			console.log(response);
		})

	}

	getSeatsNumber(row:Row) : number {
		return row.seats;
		
	}

	stepsIncrement() : void {
		this.complete.emit();
		this.done = true;
	}

	stepDecrease() : void {
		this.back.emit();
		this.done = false;
		this.backClicked = true;
	}

	isSelectedSeat (row: number, seat: number) : boolean {
		for (var i = 0; i < this.selectedTickets.length; i++) {
			if (this.selectedTickets[i].rowId == row + 1 && this.selectedTickets[i].seat == seat + 1) {
				return true;
			}
		}
		return false;
	}

	ngOnInit() {
		if (!this.selectedTickets) {
			this.selectedTickets = [];
			this.checkedCounter = 0;	
		} else {
			this.checkedCounter = this.selectedTickets.length;
		}
		this.generateRowsFromService();
	}

}