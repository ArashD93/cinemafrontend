"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var jquery_service_1 = require("../../services/jquery.service");
var row_service_1 = require("../../services/row.service");
var SeatReservationComponent = (function () {
    function SeatReservationComponent(jqueryService, rowService) {
        this.jqueryService = jqueryService;
        this.rowService = rowService;
        this.complete = new core_1.EventEmitter();
        this.back = new core_1.EventEmitter();
        this.ticket = new core_1.EventEmitter();
        this.startMakeTicketBuying = new core_1.EventEmitter();
        this.done = false;
        this.backClicked = false;
        this.Arr = Array;
        this.reservationCheck = true;
        this.rows = [];
        this.seatCheck = [];
    }
    SeatReservationComponent.prototype.makeTicketBuying = function () {
        this.startMakeTicketBuying.emit();
    };
    SeatReservationComponent.prototype.sendTicketsData = function () {
        this.ticket.emit(this.selectedTickets);
    };
    SeatReservationComponent.prototype.onChange = function (e) {
        var actualRow = parseInt(e.target.parentElement.dataset.row) + 1;
        var actualSeat = parseInt(e.target.dataset.seat) + 1;
        this.actualSeat = { rowId: actualRow, seat: actualSeat };
        if (e.target.checked) {
            this.checkedCounter++;
            this.jqueryService.seatClicked(actualRow, actualSeat, this.checkedCounter);
            this.selectedTickets.push({ rowId: actualRow, seat: actualSeat });
        }
        else {
            this.checkedCounter--;
            this.jqueryService.seatUnclicked(actualRow, actualSeat, this.checkedCounter);
            for (var i = 0; i < this.selectedTickets.length; ++i) {
                if (this.selectedTickets[i].rowId == actualRow && this.selectedTickets[i].seat == actualSeat) {
                    this.selectedTickets.splice(i, 1);
                }
            }
        }
    };
    SeatReservationComponent.prototype.isReservedSeat = function (rowNumber, seatNumber) {
        for (var i = 0; i < this.reservedSeats.length; ++i) {
            if (rowNumber == this.reservedSeats[i].rowNumber - 1 && seatNumber == this.reservedSeats[i].seat - 1) {
                return true;
            }
        }
        return false;
    };
    SeatReservationComponent.prototype.generateRowsFromService = function () {
        var _this = this;
        this.rowService.getRowsByRoomId(this.room.id)
            .then(function (rows) {
            _this.rows = rows;
        }).catch(function (response) {
            console.log(response);
        });
    };
    SeatReservationComponent.prototype.getSeatsNumber = function (row) {
        return row.seats;
    };
    SeatReservationComponent.prototype.stepsIncrement = function () {
        this.complete.emit();
        this.done = true;
    };
    SeatReservationComponent.prototype.stepDecrease = function () {
        this.back.emit();
        this.done = false;
        this.backClicked = true;
    };
    SeatReservationComponent.prototype.isSelectedSeat = function (row, seat) {
        for (var i = 0; i < this.selectedTickets.length; i++) {
            if (this.selectedTickets[i].rowId == row + 1 && this.selectedTickets[i].seat == seat + 1) {
                return true;
            }
        }
        return false;
    };
    SeatReservationComponent.prototype.ngOnInit = function () {
        if (!this.selectedTickets) {
            this.selectedTickets = [];
            this.checkedCounter = 0;
        }
        else {
            this.checkedCounter = this.selectedTickets.length;
        }
        this.generateRowsFromService();
    };
    return SeatReservationComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], SeatReservationComponent.prototype, "complete", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], SeatReservationComponent.prototype, "back", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], SeatReservationComponent.prototype, "ticket", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], SeatReservationComponent.prototype, "startMakeTicketBuying", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], SeatReservationComponent.prototype, "room", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], SeatReservationComponent.prototype, "cinema", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], SeatReservationComponent.prototype, "selectedTickets", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], SeatReservationComponent.prototype, "reservedSeats", void 0);
SeatReservationComponent = __decorate([
    core_1.Component({
        selector: 'seatreservation',
        templateUrl: './seatreservation.component.html'
    }),
    __metadata("design:paramtypes", [jquery_service_1.JqueryService, row_service_1.RowService])
], SeatReservationComponent);
exports.SeatReservationComponent = SeatReservationComponent;
//# sourceMappingURL=seatreservation.component.js.map