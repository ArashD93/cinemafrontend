"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var router_2 = require("@angular/router");
var cinema_interface_1 = require("../../common/cinema.interface");
var ticketBuying_interface_1 = require("../../common/ticketBuying.interface");
var user_interface_1 = require("../../common/user.interface");
var show_service_1 = require("../../services/show.service");
var cinema_service_1 = require("../../services/cinema.service");
var movie_service_1 = require("../../services/movie.service");
var reservation_service_1 = require("../../services/reservation.service");
var jquery_service_1 = require("../../services/jquery.service");
var data_service_1 = require("../../services/data.service");
var BookingComponent = (function () {
    function BookingComponent(route, showService, router, cinemaService, movieService, reservationService, dataService, jqueryService) {
        this.route = route;
        this.showService = showService;
        this.router = router;
        this.cinemaService = cinemaService;
        this.movieService = movieService;
        this.reservationService = reservationService;
        this.dataService = dataService;
        this.jqueryService = jqueryService;
        this.globalStep = 0;
        this.id = this.route.snapshot.params['id'];
        this.selectedCinema = cinema_interface_1.emptyCinema;
        this.tickets = [];
        this.ticketIds = [];
        this.ticketData = ticketBuying_interface_1.emptyTicketBuying;
        this.user = user_interface_1.emptyUser;
    }
    BookingComponent.prototype.makeTicketBuying = function () {
        this.ticketData.seats = this.tickets;
        this.ticketData.paymentType = this.reservation;
        this.ticketData.showId = this.id;
        this.ticketData.userId = this.user.id;
    };
    BookingComponent.prototype.dataCopy = function (ticket) {
        this.tickets = ticket;
    };
    BookingComponent.prototype.getCurrentUser = function () {
        this.user = this.dataService.signedInUser;
    };
    BookingComponent.prototype.choosedReservation = function (data) {
        this.reservation = data;
    };
    BookingComponent.prototype.copyTicketId = function (e) {
        this.ticketIds = e;
    };
    BookingComponent.prototype.searchShowById = function () {
        for (var i = 0; i < this.shows.length; ++i) {
            if (this.id == this.shows[i].id) {
                this.selectedShow = this.shows[i];
            }
        }
        if (this.selectedShow == null) {
            alert('Invalid show!');
            this.router.navigate(['/movies']);
        }
    };
    BookingComponent.prototype.incrementStep = function () {
        var _this = this;
        setTimeout(function () {
            _this.globalStep++;
        }, 600);
    };
    BookingComponent.prototype.decreaseStep = function () {
        var _this = this;
        setTimeout(function () {
            _this.globalStep--;
        }, 600);
    };
    BookingComponent.prototype.getRoom = function () {
        for (var i = 0; i < this.rooms.length; ++i) {
            if (this.rooms[i].id == this.selectedShow.room_id) {
                this.selectedRoom = this.rooms[i];
            }
        }
    };
    BookingComponent.prototype.getData = function () {
        var _this = this;
        var roomPromise = this.cinemaService.getRooms()
            .then(function (rooms) {
            _this.rooms = rooms;
        });
        var moviePromise = this.movieService.getMovies()
            .then(function (movies) {
            _this.movies = movies;
        });
        var cinemaPromise = this.cinemaService.getCinemas()
            .then(function (cinema) {
            _this.cinemas = cinema;
        });
        var showPromise = this.showService.getShows()
            .then(function (shows) {
            _this.shows = shows;
        });
        var reservationPromise = this.reservationService.getReservedSeats(this.id)
            .then(function (reservedSeats) {
            _this.reservedSeats = reservedSeats;
        });
        Promise.all([roomPromise, moviePromise, cinemaPromise, showPromise, reservationPromise, reservationPromise]).then(function () {
            _this.searchShowById();
            _this.getMovie();
            _this.getRoom();
            setTimeout(function () {
                _this.getCinema();
                _this.getCurrentUser();
            }, 50);
        }).catch(function (response) { console.log(response); });
    };
    BookingComponent.prototype.getCinema = function () {
        for (var i = 0; i < this.cinemas.length; ++i) {
            if (this.cinemas[i].id == this.selectedRoom.cinema_id) {
                this.selectedCinema = this.cinemas[i];
            }
        }
    };
    BookingComponent.prototype.getMovie = function () {
        for (var i = 0; i < this.movies.length; ++i) {
            if (this.movies[i].id == this.selectedShow.movie_id) {
                this.selectedMovie = this.movies[i];
            }
        }
    };
    BookingComponent.prototype.ngOnInit = function () {
        this.jqueryService.scrollToPositionTop(0);
        this.getData();
    };
    BookingComponent.prototype.veryCounter = function (e) {
        this.counter = e;
    };
    BookingComponent.prototype.veryAmount = function (e) {
        this.amount = e;
    };
    return BookingComponent;
}());
BookingComponent = __decorate([
    core_1.Component({
        selector: 'booking',
        templateUrl: './booking.component.html'
    }),
    __metadata("design:paramtypes", [router_2.ActivatedRoute,
        show_service_1.ShowService,
        router_1.Router,
        cinema_service_1.CinemaService,
        movie_service_1.MovieService,
        reservation_service_1.ReservationService,
        data_service_1.DataService,
        jquery_service_1.JqueryService])
], BookingComponent);
exports.BookingComponent = BookingComponent;
//# sourceMappingURL=booking.component.js.map