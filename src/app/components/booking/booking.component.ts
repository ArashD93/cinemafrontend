import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Show } from '../../common/show.interface';
import { Room } from '../../common/room.interface';
import { Cinema, emptyCinema } from '../../common/cinema.interface';
import { Movie } from '../../common/movie.interface';
import { Ticket } from '../../common/ticket.interface';
import { ReservationMethod } from '../../common/ticketBuying.interface';
import { TicketBuying, emptyTicketBuying } from '../../common/ticketBuying.interface';
import { User, emptyUser } from '../../common/user.interface';

import { ShowService } from '../../services/show.service';
import { CinemaService } from '../../services/cinema.service';
import { MovieService } from '../../services/movie.service';
import { ReservationService } from '../../services/reservation.service';
import { JqueryService } from '../../services/jquery.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'booking',
  templateUrl: './booking.component.html'
})

export class BookingComponent implements OnInit {

	globalStep : number = 0;
	id : number;
	shows: Show[];
	rooms: Room[];
	selectedShow: Show;
	selectedRoom: Room;
	cinemas: Cinema[];
	selectedCinema: Cinema;
	movies: Movie[];
	selectedMovie: Movie;
	tickets: Ticket[];
	reservation: ReservationMethod;
	ticketData: TicketBuying;
	ticketIds : number[];
	reservedSeats: Ticket[];
	user: User;
	counter: number;
	amount: number;

	constructor( 
		private route:ActivatedRoute, 
		private showService:ShowService,
		private router:Router,
		private cinemaService:CinemaService,
		private movieService:MovieService,
		private reservationService:ReservationService,
		private dataService:DataService,
		private jqueryService:JqueryService
		 ) {
		this.id = this.route.snapshot.params['id'];
		this.selectedCinema = emptyCinema;
		this.tickets = [];
		this.ticketIds = [];
		this.ticketData = emptyTicketBuying;
		this.user = emptyUser;
	}

	makeTicketBuying() : void {
		this.ticketData.seats = this.tickets;
		this.ticketData.paymentType = this.reservation;
		this.ticketData.showId = this.id;
		this.ticketData.userId = this.user.id;
	}

	dataCopy(ticket : Ticket[]) : void {

		this.tickets = ticket;
	}

	getCurrentUser() : void {
		this.user = this.dataService.signedInUser;
	}

	choosedReservation(data : ReservationMethod) : void {
		this.reservation = data;
	}

	copyTicketId(e:number[]) : void {
		this.ticketIds = e;
	}

	searchShowById() : void {

		for (let i = 0; i < this.shows.length; ++i) {
			if (this.id == this.shows[i].id) {
				this.selectedShow = this.shows[i];
			}
		}
		if (this.selectedShow == null){
			alert('Invalid show!');
			this.router.navigate(['/movies']);
		}
	}

	incrementStep() : void {
		setTimeout(()=>{
			this.globalStep ++;
		}, 600);
	}
	decreaseStep() : void {
		setTimeout(()=>{
			this.globalStep --;
		}, 600);
	}

	getRoom() : void  {
		for (var i = 0; i < this.rooms.length; ++i) {
			if (this.rooms[i].id == this.selectedShow.room_id){
				this.selectedRoom = this.rooms[i];
			}
		}

	}

	getData() : void {
		let roomPromise = this.cinemaService.getRooms()
		.then( (rooms : Room[])=> {
			this.rooms = rooms;
		});
		let moviePromise = this.movieService.getMovies()
		.then( (movies : Movie[]) => {
			this.movies = movies;
		});
		let cinemaPromise = this.cinemaService.getCinemas()
		.then( (cinema : Cinema[]) => {
			this.cinemas = cinema;
		});
		let showPromise = this.showService.getShows()
		.then( (shows : Show[])=> {
			this.shows = shows;
		});
		let reservationPromise = this.reservationService.getReservedSeats(this.id)
		.then( (reservedSeats : Ticket[])=> {
			this.reservedSeats = reservedSeats;
		});
		Promise.all([roomPromise, moviePromise, cinemaPromise, showPromise, reservationPromise, reservationPromise]).then(()=> {
			this.searchShowById();
			this.getMovie();
			this.getRoom();
			setTimeout(()=>{
				this.getCinema();
				this.getCurrentUser();
			},50);
		}).catch((response) => { console.log (response) });	
	}


	getCinema() : void {
		for (var i = 0; i < this.cinemas.length; ++i) {
			if (this.cinemas[i].id == this.selectedRoom.cinema_id) {
				this.selectedCinema = this.cinemas[i];
			}
		}
	}

	getMovie() : void {
		for (var i = 0; i < this.movies.length; ++i) {
			if (this.movies[i].id == this.selectedShow.movie_id) {
				this.selectedMovie = this.movies[i];
			}
		}
	}

	ngOnInit() : void {
		this.jqueryService.scrollToPositionTop(0);
		this.getData();

	}

	veryCounter(e: number) : void {
		this.counter = e;
	}

	veryAmount(e: number) : void {
		this.amount = e;
	}

}