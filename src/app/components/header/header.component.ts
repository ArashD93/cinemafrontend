import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestOptions, Headers, Http } from '@angular/http';
import { Uploader, UploadItem } from 'angular2-http-file-upload';


import { backendBaseUrl } from '../../common/settings';
import { JqueryService } from '../../services/jquery.service';
import { UserService } from '../../services/user.service';
import { DataService } from '../../services/data.service';
import { User, emptyUser } from '../../common/user.interface'

declare var $ : any;

class UploadPicture extends UploadItem {
	constructor( file: any ) {
		super();
		this.file = file;
		this.withCredentials = true;
	}
}

@Component({
	selector: 'cinema-header',
	templateUrl: './header.component.html'
})

export class HeaderComponent implements OnInit {
	isSignInFormSown: boolean = false;
	isRegisterFormShown: boolean = false;
	isMenuVisible : boolean = false;

	isRegisterMailSent: boolean = false;
	isPasswordsMatch: boolean = true;
	isNewPasswordReseted: boolean = false;
	fileUploaded: boolean = false;

	registerFormErrors: any;
	signInFormErrors: any;
	forgotPassErrors: any;
	newPassErrors: any;

	currentUser: User;
	currentSigningUser: User;
	currentForgotPassUser: User;
	newPassUser: User;
	changesUser: User;
	newPassUserSecond: string;
	firstNewPassLength: number = -1;
	secondNewPassLength: number = -1;
	uploadFile : File;
	backendBaseUrl : string;
	cacheBreaker: string;
	fileUploadProgress : number;

	constructor(private jQueryService : JqueryService,
				private userService : UserService,
				private dataService : DataService,
				private router : Router,
				private uploaderService : Uploader,
				private http : Http) {
		this.currentUser = {id : 0,
							email : '',
							password : '',
							full_name : '',
							verify_hash : ''
		};
		this.currentSigningUser = JSON.parse(JSON.stringify(emptyUser));
		this.currentForgotPassUser = JSON.parse(JSON.stringify(emptyUser));
		this.newPassUser = JSON.parse(JSON.stringify(emptyUser));
		this.changesUser = JSON.parse(JSON.stringify(emptyUser));
		this.newPassUserSecond = '';
		this.registerFormErrors = {
			userName : '',
			email : '',
			password : ''
		};
		this.signInFormErrors = {
			email: '',
			password: ''
		}
		this.forgotPassErrors = {
			email: ''
		}
		this.newPassErrors = {
			first: '',
			second: ''
		}
		this.backendBaseUrl = backendBaseUrl;
		this.refreshCacheBreaker();
		this.fileUploadProgress = 0;
	}

	ngOnInit() : void {
		this.jQueryService.initStickyMenu();
		if (this.dataService.newPassAsker.email != '') {
			$('#new-pass-message').modal(
				{
					backdrop: 'static',
					keyboard: false
				});
		}
		this.changesUser = JSON.parse(JSON.stringify(this.dataService.signedInUser));
		if (this.dataService.signedInUser.email == '') {
			this.userService.getCurrentUser()
			.then( (user: User ) => {
				if (user.email !== "anonymousUser") {
					this.dataService.signedInUser = user;
					this.changesUser = JSON.parse(JSON.stringify(this.dataService.signedInUser));
				}
				if (user.email == 'admin@admin.com') {
					this.router.navigate(['/admin/main']);
				}
				this.isSignInFormSown = false;
				this.removeNoScroll();
			} )
			.catch ( (error) => {
				console.log( 'error when getting CurrentUser: ' + error);
			} );
		}
	}

	refreshCacheBreaker() : void {
		this.cacheBreaker = "?cb=" + (new Date()).getTime();
	}

	logOutUser() : void {
		this.userService.logOutUser()
		.then( () => {
			this.dataService.signedInUser = JSON.parse(JSON.stringify(emptyUser));
			this.currentSigningUser = JSON.parse(JSON.stringify(emptyUser));
		})
		.catch( response => {
			console.log(response);
		} );
	}

	showSignInForm() : void {
		this.isSignInFormSown = true;
		document.body.classList.add('no-scroll');
		if (this.currentSigningUser.email.length > 0) {
			this.checkSignInEmail();
		}
		if (this.currentSigningUser.password.length > 0) {
			this.checkSignInPassword();
		}
	}
	showRegisterForm() : void {
		this.isRegisterFormShown = true;
		document.body.classList.add('no-scroll');
		if (this.currentUser.full_name.length > 0) {
			this.checkName();
		}
		if (this.currentUser.email.length > 0) {
			this.checkRegEmail();
		}
		if (this.currentUser.password.length > 0) {
			this.checkRegPassword();
		}
	}

	setMenu() : void {
		if (! this.isMenuVisible && ! this.isRegisterFormShown && ! this.isSignInFormSown) {
			this.isMenuVisible = true;
			document.body.classList.add('no-scroll');
		} else {
			this.isMenuVisible = false;
			this.isRegisterFormShown = false;
			this.isSignInFormSown = false;
			this.removeNoScroll();

			setTimeout( () => {
				this.registerFormErrors.userName = '';
				this.registerFormErrors.email = '';
				this.registerFormErrors.password = '';

				this.signInFormErrors.email = '';
				this.signInFormErrors.password = '';

				this.forgotPassErrors.email = '';

				this.newPassErrors.first = '';
				this.newPassErrors.second = '';
			}, 350);
			
		}
	}

	checkName() : void {
		if (this.currentUser.full_name == '') {
			this.registerFormErrors.userName = 'Name can not be empty !';
		} else {
			this.registerFormErrors.userName = '';
		}
	}
	checkRegEmail() : void {
		if (this.currentUser.email == '') {
			this.registerFormErrors.email = 'E-mail can not be empty !';
		} else {
			var mailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if ( mailRegexp.test(this.currentUser.email) == false ) {
				this.registerFormErrors.email = 'Please insert a valid e-mail address !'
			} else {
				this.registerFormErrors.email = '';
			}
		}
	}
	checkRegPassword() : void {
		if (this.currentUser.password == '') {
			this.registerFormErrors.password = 'Password can not be empty !';
		} else {
			var passRegexp = /^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z]).{8,}$/;
			if ( passRegexp.test(this.currentUser.password) == false ) {
				this.registerFormErrors.password = 'Not good yet'
			} else {
				this.registerFormErrors.password = '';
			}
		}
	}
	registerUser() : void {
		this.checkName();
		this.checkRegEmail();
		this.checkRegPassword();
		if (this.registerFormErrors.userName == '' && this.registerFormErrors.email == '' && this.registerFormErrors.password == '') {
			this.userService.registerUser(this.currentUser)
			.then( (user: User) => {
				this.isRegisterMailSent = true;
				this.isRegisterFormShown = false;
				this.removeNoScroll();
				$('#reg-mail-message').modal();
			} )
			.catch ( (error) => {
				console.log( 'error when registering user: ' + error);
				$('#bad-regin-message').modal();
			} );
		}
	}
	checkSignInEmail() : void {
		if (this.currentSigningUser.email == '') {
			this.signInFormErrors.email = 'E-mail cannot be empty !';
		} else {
			this.signInFormErrors.email = '';
		}
	}
	checkSignInPassword() : void {
		if (this.currentSigningUser.password == '') {
			this.signInFormErrors.password = 'Password cannot be empty !';
		} else {
			this.signInFormErrors.password = '';
		}
	}
	signInUser() : void {
		this.checkSignInEmail();
		this.checkSignInPassword();
		if (this.signInFormErrors.email == '' && this.signInFormErrors.password == '') {
			this.userService.signInUser(this.currentSigningUser)
			.then( () => {
				this.userService.getCurrentUser()
				.then( (user: User ) => {
					if (user.email !== "anonymousUser") {
						this.dataService.signedInUser = user;
						this.changesUser = JSON.parse(JSON.stringify(this.dataService.signedInUser));
					}
					if (user.email == 'admin@admin.com') {
						this.router.navigate(['/admin/main']);
					}
					this.isSignInFormSown = false;
					this.removeNoScroll();
				} )
				.catch ( (error) => {
					console.log( 'error when getting Current User: ' + error);
				} );
			} )
			.catch ( (error) => {
				console.log( 'error when signing in user: ' + error);
				$('#bad-login-message').modal();
			} );
		}
	}
	checkForgotPassEmail() : void {
		if (this.dataService.signedInUser.email != '') {
			this.currentForgotPassUser = JSON.parse(JSON.stringify(this.dataService.signedInUser));
		}
		if (this.currentForgotPassUser.email == '') {
			this.forgotPassErrors.email = 'E-mail cannot be empty !';
		} else {
			this.forgotPassErrors.email = '';
		}
	}
	
	sendResetPasswordMail() : void {
		this.checkForgotPassEmail();
		if (this.forgotPassErrors.email == '') {
			this.userService.sendResetPasswordMail(this.currentForgotPassUser)
			.then( (user: User)  => {
				$('#fp-mail-message').modal();
			})
			.catch ( (error) => {
				console.log( 'error when sending reset mail: ' + error);
				$('#bad-reset-message').modal();
			} );
		}
	}
	checkNewFirstPassword() : void {
		if (this.newPassUser.password == '') {
			this.newPassErrors.first = 'Password cannot be empty !';
		} else {
			var passRegexp = /^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z]).{8,}$/;
			if ( passRegexp.test(this.newPassUser.password) == false ) {
				this.newPassErrors.first = 'Not good yet'
			} else {
				this.newPassErrors.first = '';
			}
		}
	}
	checkNewSecondPassword() : void {
		if (this.newPassUserSecond == '') {
			this.newPassErrors.second = 'Password cannot be empty !';
		} else {
			var passRegexp = /^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z]).{8,}$/;
			if ( passRegexp.test(this.newPassUserSecond) == false ) {
				this.newPassErrors.second = 'Not good yet'
			} else {
				this.newPassErrors.second = '';
			}
		}
	}
	isNewPasswordsMatch(firstLength: number, secondLength: number) : void {
		this.firstNewPassLength = firstLength;
		this.secondNewPassLength = secondLength;
		if (this.newPassErrors.first == '' &&
			this.newPassErrors.second == '' &&
			this.newPassUser.password.length > 0 &&
			this.newPassUserSecond.length > 0 &&
			this.newPassUser.password == this.newPassUserSecond) {
			this.isPasswordsMatch = true;
			this.setNewPassword();
		} else {
			this.isPasswordsMatch = false;
		}
	}
	setLength() : void {
		if (!this.isPasswordsMatch && this.newPassUser.password.length !== this.firstNewPassLength) {
			this.firstNewPassLength = -1;
		}
		if (!this.isPasswordsMatch && this.newPassUserSecond.length !== this.secondNewPassLength) {
			this.secondNewPassLength = -1;
		}
	}
	setNewPassword() : void {
		
		this.dataService.newPassAsker.password = this.newPassUser.password;
		this.userService.putNewPassword(this.dataService.newPassAsker)
		.then( (user:User) => {
			this.isNewPasswordReseted = true;
			this.logOutUser();
			this.dataService.newPassAsker = JSON.parse(JSON.stringify(emptyUser));
		} )
		.catch( response => {
			console.log(response);
		} );
		
	}

	fileChange(e: any) : void {
		this.uploadFile = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
		this.fileUploaded = true;
	}
	uploadAvatar() : void {
		if (this.uploadFile) {
			let uploadPicture = new UploadPicture(this.uploadFile);
			uploadPicture.url = backendBaseUrl + "users/settings/" + this.dataService.signedInUser.id + "/setAvatar";

			this.uploaderService.onSuccessUpload = (item : UploadItem, response: any, status: any, headers: any) => {
				console.log("yeehaw!!!");
				this.userService.getAvatar(this.dataService.signedInUser)
				.then( (imgUrl: any) => {
					this.dataService.signedInUser.file = imgUrl;
				})
				.catch( response => {
					console.log(response);
				});
				this.refreshCacheBreaker();
				this.fileUploaded = false;
				setTimeout(()=>{
					this.fileUploadProgress = 0;
				},1000);
			};
			this.uploaderService.onErrorUpload = (item : UploadItem, response: any, status: any, headers: any) => {
				console.log("error!!!");
				setTimeout(()=>{
					this.fileUploadProgress = 0;
				},1000);
			};
			this.uploaderService.onProgressUpload = (item : UploadItem, percentComplete : number) => {
				this.fileUploadProgress = percentComplete;
			};
			this.uploaderService.upload(uploadPicture);
		}
	}
	saveAccount(): void {
		$('#changes-message').modal();
	}
	changeName(): void {
		this.userService.changeName(this.changesUser) 
			.then( (user: User) => {
				this.dataService.signedInUser = user;
				$('#changes-message').modal('hide');
			})
			.catch (response => {
				console.log(response);
			});
	}

	removeNoScroll() :void {
		document.body.classList.remove('no-scroll');
	}
}
