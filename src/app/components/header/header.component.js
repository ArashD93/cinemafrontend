"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_1 = require("@angular/http");
var angular2_http_file_upload_1 = require("angular2-http-file-upload");
var settings_1 = require("../../common/settings");
var jquery_service_1 = require("../../services/jquery.service");
var user_service_1 = require("../../services/user.service");
var data_service_1 = require("../../services/data.service");
var user_interface_1 = require("../../common/user.interface");
var UploadPicture = (function (_super) {
    __extends(UploadPicture, _super);
    function UploadPicture(file) {
        var _this = _super.call(this) || this;
        _this.file = file;
        _this.withCredentials = true;
        return _this;
    }
    return UploadPicture;
}(angular2_http_file_upload_1.UploadItem));
var HeaderComponent = (function () {
    function HeaderComponent(jQueryService, userService, dataService, router, uploaderService, http) {
        this.jQueryService = jQueryService;
        this.userService = userService;
        this.dataService = dataService;
        this.router = router;
        this.uploaderService = uploaderService;
        this.http = http;
        this.isSignInFormSown = false;
        this.isRegisterFormShown = false;
        this.isMenuVisible = false;
        this.isRegisterMailSent = false;
        this.isPasswordsMatch = true;
        this.isNewPasswordReseted = false;
        this.fileUploaded = false;
        this.firstNewPassLength = -1;
        this.secondNewPassLength = -1;
        this.currentUser = { id: 0,
            email: '',
            password: '',
            full_name: '',
            verify_hash: ''
        };
        this.currentSigningUser = JSON.parse(JSON.stringify(user_interface_1.emptyUser));
        this.currentForgotPassUser = JSON.parse(JSON.stringify(user_interface_1.emptyUser));
        this.newPassUser = JSON.parse(JSON.stringify(user_interface_1.emptyUser));
        this.changesUser = JSON.parse(JSON.stringify(user_interface_1.emptyUser));
        this.newPassUserSecond = '';
        this.registerFormErrors = {
            userName: '',
            email: '',
            password: ''
        };
        this.signInFormErrors = {
            email: '',
            password: ''
        };
        this.forgotPassErrors = {
            email: ''
        };
        this.newPassErrors = {
            first: '',
            second: ''
        };
        this.backendBaseUrl = settings_1.backendBaseUrl;
        this.refreshCacheBreaker();
        this.fileUploadProgress = 0;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.jQueryService.initStickyMenu();
        if (this.dataService.newPassAsker.email != '') {
            $('#new-pass-message').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
        this.changesUser = JSON.parse(JSON.stringify(this.dataService.signedInUser));
        if (this.dataService.signedInUser.email == '') {
            this.userService.getCurrentUser()
                .then(function (user) {
                if (user.email !== "anonymousUser") {
                    _this.dataService.signedInUser = user;
                    _this.changesUser = JSON.parse(JSON.stringify(_this.dataService.signedInUser));
                }
                if (user.email == 'admin@admin.com') {
                    _this.router.navigate(['/admin/main']);
                }
                _this.isSignInFormSown = false;
                _this.removeNoScroll();
            })
                .catch(function (error) {
                console.log('error when getting CurrentUser: ' + error);
            });
        }
    };
    HeaderComponent.prototype.refreshCacheBreaker = function () {
        this.cacheBreaker = "?cb=" + (new Date()).getTime();
    };
    HeaderComponent.prototype.logOutUser = function () {
        var _this = this;
        this.userService.logOutUser()
            .then(function () {
            _this.dataService.signedInUser = JSON.parse(JSON.stringify(user_interface_1.emptyUser));
            _this.currentSigningUser = JSON.parse(JSON.stringify(user_interface_1.emptyUser));
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    HeaderComponent.prototype.showSignInForm = function () {
        this.isSignInFormSown = true;
        document.body.classList.add('no-scroll');
        if (this.currentSigningUser.email.length > 0) {
            this.checkSignInEmail();
        }
        if (this.currentSigningUser.password.length > 0) {
            this.checkSignInPassword();
        }
    };
    HeaderComponent.prototype.showRegisterForm = function () {
        this.isRegisterFormShown = true;
        document.body.classList.add('no-scroll');
        if (this.currentUser.full_name.length > 0) {
            this.checkName();
        }
        if (this.currentUser.email.length > 0) {
            this.checkRegEmail();
        }
        if (this.currentUser.password.length > 0) {
            this.checkRegPassword();
        }
    };
    HeaderComponent.prototype.setMenu = function () {
        var _this = this;
        if (!this.isMenuVisible && !this.isRegisterFormShown && !this.isSignInFormSown) {
            this.isMenuVisible = true;
            document.body.classList.add('no-scroll');
        }
        else {
            this.isMenuVisible = false;
            this.isRegisterFormShown = false;
            this.isSignInFormSown = false;
            this.removeNoScroll();
            setTimeout(function () {
                _this.registerFormErrors.userName = '';
                _this.registerFormErrors.email = '';
                _this.registerFormErrors.password = '';
                _this.signInFormErrors.email = '';
                _this.signInFormErrors.password = '';
                _this.forgotPassErrors.email = '';
                _this.newPassErrors.first = '';
                _this.newPassErrors.second = '';
            }, 350);
        }
    };
    HeaderComponent.prototype.checkName = function () {
        if (this.currentUser.full_name == '') {
            this.registerFormErrors.userName = 'Name can not be empty !';
        }
        else {
            this.registerFormErrors.userName = '';
        }
    };
    HeaderComponent.prototype.checkRegEmail = function () {
        if (this.currentUser.email == '') {
            this.registerFormErrors.email = 'E-mail can not be empty !';
        }
        else {
            var mailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (mailRegexp.test(this.currentUser.email) == false) {
                this.registerFormErrors.email = 'Please insert a valid e-mail address !';
            }
            else {
                this.registerFormErrors.email = '';
            }
        }
    };
    HeaderComponent.prototype.checkRegPassword = function () {
        if (this.currentUser.password == '') {
            this.registerFormErrors.password = 'Password can not be empty !';
        }
        else {
            var passRegexp = /^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z]).{8,}$/;
            if (passRegexp.test(this.currentUser.password) == false) {
                this.registerFormErrors.password = 'Not good yet';
            }
            else {
                this.registerFormErrors.password = '';
            }
        }
    };
    HeaderComponent.prototype.registerUser = function () {
        var _this = this;
        this.checkName();
        this.checkRegEmail();
        this.checkRegPassword();
        if (this.registerFormErrors.userName == '' && this.registerFormErrors.email == '' && this.registerFormErrors.password == '') {
            this.userService.registerUser(this.currentUser)
                .then(function (user) {
                _this.isRegisterMailSent = true;
                _this.isRegisterFormShown = false;
                _this.removeNoScroll();
                $('#reg-mail-message').modal();
            })
                .catch(function (error) {
                console.log('error when registering user: ' + error);
                $('#bad-regin-message').modal();
            });
        }
    };
    HeaderComponent.prototype.checkSignInEmail = function () {
        if (this.currentSigningUser.email == '') {
            this.signInFormErrors.email = 'E-mail cannot be empty !';
        }
        else {
            this.signInFormErrors.email = '';
        }
    };
    HeaderComponent.prototype.checkSignInPassword = function () {
        if (this.currentSigningUser.password == '') {
            this.signInFormErrors.password = 'Password cannot be empty !';
        }
        else {
            this.signInFormErrors.password = '';
        }
    };
    HeaderComponent.prototype.signInUser = function () {
        var _this = this;
        this.checkSignInEmail();
        this.checkSignInPassword();
        if (this.signInFormErrors.email == '' && this.signInFormErrors.password == '') {
            this.userService.signInUser(this.currentSigningUser)
                .then(function () {
                _this.userService.getCurrentUser()
                    .then(function (user) {
                    if (user.email !== "anonymousUser") {
                        _this.dataService.signedInUser = user;
                        _this.changesUser = JSON.parse(JSON.stringify(_this.dataService.signedInUser));
                    }
                    if (user.email == 'admin@admin.com') {
                        _this.router.navigate(['/admin/main']);
                    }
                    _this.isSignInFormSown = false;
                    _this.removeNoScroll();
                })
                    .catch(function (error) {
                    console.log('error when getting Current User: ' + error);
                });
            })
                .catch(function (error) {
                console.log('error when signing in user: ' + error);
                $('#bad-login-message').modal();
            });
        }
    };
    HeaderComponent.prototype.checkForgotPassEmail = function () {
        if (this.dataService.signedInUser.email != '') {
            this.currentForgotPassUser = JSON.parse(JSON.stringify(this.dataService.signedInUser));
        }
        if (this.currentForgotPassUser.email == '') {
            this.forgotPassErrors.email = 'E-mail cannot be empty !';
        }
        else {
            this.forgotPassErrors.email = '';
        }
    };
    HeaderComponent.prototype.sendResetPasswordMail = function () {
        this.checkForgotPassEmail();
        if (this.forgotPassErrors.email == '') {
            this.userService.sendResetPasswordMail(this.currentForgotPassUser)
                .then(function (user) {
                $('#fp-mail-message').modal();
            })
                .catch(function (error) {
                console.log('error when sending reset mail: ' + error);
                $('#bad-reset-message').modal();
            });
        }
    };
    HeaderComponent.prototype.checkNewFirstPassword = function () {
        if (this.newPassUser.password == '') {
            this.newPassErrors.first = 'Password cannot be empty !';
        }
        else {
            var passRegexp = /^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z]).{8,}$/;
            if (passRegexp.test(this.newPassUser.password) == false) {
                this.newPassErrors.first = 'Not good yet';
            }
            else {
                this.newPassErrors.first = '';
            }
        }
    };
    HeaderComponent.prototype.checkNewSecondPassword = function () {
        if (this.newPassUserSecond == '') {
            this.newPassErrors.second = 'Password cannot be empty !';
        }
        else {
            var passRegexp = /^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z]).{8,}$/;
            if (passRegexp.test(this.newPassUserSecond) == false) {
                this.newPassErrors.second = 'Not good yet';
            }
            else {
                this.newPassErrors.second = '';
            }
        }
    };
    HeaderComponent.prototype.isNewPasswordsMatch = function (firstLength, secondLength) {
        this.firstNewPassLength = firstLength;
        this.secondNewPassLength = secondLength;
        if (this.newPassErrors.first == '' &&
            this.newPassErrors.second == '' &&
            this.newPassUser.password.length > 0 &&
            this.newPassUserSecond.length > 0 &&
            this.newPassUser.password == this.newPassUserSecond) {
            this.isPasswordsMatch = true;
            this.setNewPassword();
        }
        else {
            this.isPasswordsMatch = false;
        }
    };
    HeaderComponent.prototype.setLength = function () {
        if (!this.isPasswordsMatch && this.newPassUser.password.length !== this.firstNewPassLength) {
            this.firstNewPassLength = -1;
        }
        if (!this.isPasswordsMatch && this.newPassUserSecond.length !== this.secondNewPassLength) {
            this.secondNewPassLength = -1;
        }
    };
    HeaderComponent.prototype.setNewPassword = function () {
        var _this = this;
        this.dataService.newPassAsker.password = this.newPassUser.password;
        this.userService.putNewPassword(this.dataService.newPassAsker)
            .then(function (user) {
            _this.isNewPasswordReseted = true;
            _this.logOutUser();
            _this.dataService.newPassAsker = JSON.parse(JSON.stringify(user_interface_1.emptyUser));
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    HeaderComponent.prototype.fileChange = function (e) {
        this.uploadFile = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        this.fileUploaded = true;
    };
    HeaderComponent.prototype.uploadAvatar = function () {
        var _this = this;
        if (this.uploadFile) {
            var uploadPicture = new UploadPicture(this.uploadFile);
            uploadPicture.url = settings_1.backendBaseUrl + "users/settings/" + this.dataService.signedInUser.id + "/setAvatar";
            this.uploaderService.onSuccessUpload = function (item, response, status, headers) {
                console.log("yeehaw!!!");
                _this.userService.getAvatar(_this.dataService.signedInUser)
                    .then(function (imgUrl) {
                    _this.dataService.signedInUser.file = imgUrl;
                })
                    .catch(function (response) {
                    console.log(response);
                });
                _this.refreshCacheBreaker();
                _this.fileUploaded = false;
                setTimeout(function () {
                    _this.fileUploadProgress = 0;
                }, 1000);
            };
            this.uploaderService.onErrorUpload = function (item, response, status, headers) {
                console.log("error!!!");
                setTimeout(function () {
                    _this.fileUploadProgress = 0;
                }, 1000);
            };
            this.uploaderService.onProgressUpload = function (item, percentComplete) {
                _this.fileUploadProgress = percentComplete;
            };
            this.uploaderService.upload(uploadPicture);
        }
    };
    HeaderComponent.prototype.saveAccount = function () {
        $('#changes-message').modal();
    };
    HeaderComponent.prototype.changeName = function () {
        var _this = this;
        this.userService.changeName(this.changesUser)
            .then(function (user) {
            _this.dataService.signedInUser = user;
            $('#changes-message').modal('hide');
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    HeaderComponent.prototype.removeNoScroll = function () {
        document.body.classList.remove('no-scroll');
    };
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    core_1.Component({
        selector: 'cinema-header',
        templateUrl: './header.component.html'
    }),
    __metadata("design:paramtypes", [jquery_service_1.JqueryService,
        user_service_1.UserService,
        data_service_1.DataService,
        router_1.Router,
        angular2_http_file_upload_1.Uploader,
        http_1.Http])
], HeaderComponent);
exports.HeaderComponent = HeaderComponent;
//# sourceMappingURL=header.component.js.map