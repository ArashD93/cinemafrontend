"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var movie_service_1 = require("../../services/movie.service");
var MoviesComponent = (function () {
    function MoviesComponent(movieService, router) {
        this.movieService = movieService;
        this.router = router;
        this.movies = [];
    }
    MoviesComponent.prototype.getMoviesFromServiec = function () {
        var _this = this;
        this.movieService.getMovies()
            .then(function (movies) {
            _this.movies = movies;
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    MoviesComponent.prototype.ngOnInit = function () {
        this.getMoviesFromServiec();
    };
    MoviesComponent.prototype.clickPic = function (id) {
        this.router.navigate(['/movie', id]);
    };
    return MoviesComponent;
}());
MoviesComponent = __decorate([
    core_1.Component({
        selector: 'movies',
        templateUrl: './movies.component.html'
    }),
    __metadata("design:paramtypes", [movie_service_1.MovieService, router_1.Router])
], MoviesComponent);
exports.MoviesComponent = MoviesComponent;
//# sourceMappingURL=movies.component.js.map