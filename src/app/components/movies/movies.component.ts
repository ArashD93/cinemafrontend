import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

import { Movie } from '../../common/movie.interface';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'movies',
  templateUrl: './movies.component.html'
})

export class MoviesComponent implements OnInit{

	movies : Movie[];

	constructor(private movieService: MovieService, private router: Router) {
		this.movies=[];
	}	

	getMoviesFromServiec() : void {
		this.movieService.getMovies()
		.then( (movies : Movie[])=> {
			this.movies = movies;
		} )
		.catch(response => {
			console.log(response);
		});
	}

	ngOnInit() : void {
		this.getMoviesFromServiec();
	}

	clickPic(id : number) : void {
		this.router.navigate(['/movie', id]);
	}

}