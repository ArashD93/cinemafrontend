import { Component, OnInit, Input, ApplicationRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { Movie} from '../../common/movie.interface';
import { MovieService } from '../../services/movie.service';
import { WindowRef, Scroll } from '../../services/window.service';
import { JqueryService } from '../../services/jquery.service';
import { DataService } from '../../services/data.service';

declare var YT : any;
declare var $: any;

@Component({
  selector: 'movie',
  templateUrl: './movie.component.html'
})
export class MovieComponent implements OnInit {
	
	id : number;
	movies : Movie[];
	selectedMovie : Movie;
	videoPlay = false;
	videoPlayer : any;

	constructor( 
		private route : ActivatedRoute,
		private applicationRef : ApplicationRef,
		private MovieService: MovieService,
		private scroll : Scroll,
		private domSanitizer : DomSanitizer,
		private jqueryService : JqueryService,
		private dataService: DataService) {
		this.id = this.route.snapshot.params['id'];
		this.movies = [];
	}  

	selectedMovieSearch() : void {
		if (this.movies.length != 0){
			for (let i = 0; i < this.movies.length; ++i) {
				if (this.id == this.movies[i].id){
					this.selectedMovie = this.movies[i];
				}
			}
		}
	}

	smoothScroll() : void {
		this.scroll.smoothScroll();
	}

	showVideo() : void {
		this.videoPlay = true;
		setTimeout(()=>{
			this.videoPlayer = new YT.Player('movie-trailer', {
	          height: '370',
	          width: '736',
	          videoId: this.selectedMovie.trailer_url.substring(this.selectedMovie.trailer_url.lastIndexOf('/')),
	          events: {
	            'onReady': (event: any) => {
					event.target.playVideo();
	            }
	          }
	        });
		}, 20);
		
	}

	CloseVideo() : void {
		this.videoPlay = false;
	}

	getMoviesFromService() : void {
		this.MovieService.getMovies()
		.then( (movies : Movie[])=> {
			this.movies = movies;
			this.selectedMovieSearch();
			setTimeout( () => { this.jqueryService.initOwlCarousel(); }, 100);
			// TODO start trailer, if....
			if (this.MovieService.startPlayingTrailer) {
				this.showVideo();
				this.MovieService.setStartPlayingTrailer(false);
			}
			if (this.dataService.scrollToBuy) {
				this.applicationRef.tick();
				let orderContainerOffset = $('#movie-reservation').position().top;
				this.jqueryService.scrollToPositionTop(orderContainerOffset);
				this.dataService.scrollToBuy = false;
			}
		} )
		.catch(response => {
			console.log(response);
		});
	}	

	ngOnInit() : void {
		
		this.getMoviesFromService();
		this.route.params.subscribe( event => {
			this.id = this.route.snapshot.params['id'];
			this.selectedMovieSearch();
		} );
	}
}