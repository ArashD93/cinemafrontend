"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var platform_browser_1 = require("@angular/platform-browser");
var movie_service_1 = require("../../services/movie.service");
var window_service_1 = require("../../services/window.service");
var jquery_service_1 = require("../../services/jquery.service");
var data_service_1 = require("../../services/data.service");
var MovieComponent = (function () {
    function MovieComponent(route, applicationRef, MovieService, scroll, domSanitizer, jqueryService, dataService) {
        this.route = route;
        this.applicationRef = applicationRef;
        this.MovieService = MovieService;
        this.scroll = scroll;
        this.domSanitizer = domSanitizer;
        this.jqueryService = jqueryService;
        this.dataService = dataService;
        this.videoPlay = false;
        this.id = this.route.snapshot.params['id'];
        this.movies = [];
    }
    MovieComponent.prototype.selectedMovieSearch = function () {
        if (this.movies.length != 0) {
            for (var i = 0; i < this.movies.length; ++i) {
                if (this.id == this.movies[i].id) {
                    this.selectedMovie = this.movies[i];
                }
            }
        }
    };
    MovieComponent.prototype.smoothScroll = function () {
        this.scroll.smoothScroll();
    };
    MovieComponent.prototype.showVideo = function () {
        var _this = this;
        this.videoPlay = true;
        setTimeout(function () {
            _this.videoPlayer = new YT.Player('movie-trailer', {
                height: '370',
                width: '736',
                videoId: _this.selectedMovie.trailer_url.substring(_this.selectedMovie.trailer_url.lastIndexOf('/')),
                events: {
                    'onReady': function (event) {
                        event.target.playVideo();
                    }
                }
            });
        }, 20);
    };
    MovieComponent.prototype.CloseVideo = function () {
        this.videoPlay = false;
    };
    MovieComponent.prototype.getMoviesFromService = function () {
        var _this = this;
        this.MovieService.getMovies()
            .then(function (movies) {
            _this.movies = movies;
            _this.selectedMovieSearch();
            setTimeout(function () { _this.jqueryService.initOwlCarousel(); }, 100);
            // TODO start trailer, if....
            if (_this.MovieService.startPlayingTrailer) {
                _this.showVideo();
                _this.MovieService.setStartPlayingTrailer(false);
            }
            if (_this.dataService.scrollToBuy) {
                _this.applicationRef.tick();
                var orderContainerOffset = $('#movie-reservation').position().top;
                _this.jqueryService.scrollToPositionTop(orderContainerOffset);
                _this.dataService.scrollToBuy = false;
            }
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    MovieComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getMoviesFromService();
        this.route.params.subscribe(function (event) {
            _this.id = _this.route.snapshot.params['id'];
            _this.selectedMovieSearch();
        });
    };
    return MovieComponent;
}());
MovieComponent = __decorate([
    core_1.Component({
        selector: 'movie',
        templateUrl: './movie.component.html'
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute,
        core_1.ApplicationRef,
        movie_service_1.MovieService,
        window_service_1.Scroll,
        platform_browser_1.DomSanitizer,
        jquery_service_1.JqueryService,
        data_service_1.DataService])
], MovieComponent);
exports.MovieComponent = MovieComponent;
//# sourceMappingURL=movie.component.js.map