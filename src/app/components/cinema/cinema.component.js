"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var cinema_interface_1 = require("../../common/cinema.interface");
var cinema_service_1 = require("../../services/cinema.service");
var settings_1 = require("../../common/settings");
var CinemaComponent = (function () {
    function CinemaComponent(route, cinService) {
        this.route = route;
        this.cinService = cinService;
        this.hideShowCinemasBar = true;
        this.currentCinema = cinema_interface_1.emptyCinema;
        this.id = route.snapshot.params['cinema_id'];
        this.roomsTemp = [];
    }
    CinemaComponent.prototype.getCinemaFromService = function () {
        var _this = this;
        var cinemaPromise = this.cinService.getCinema(this.id)
            .then(function (cinema) {
            _this.currentCinema = cinema;
        });
        var roomsPromise = this.cinService.getRoomsByCinemaId(this.id)
            .then(function (rooms) {
            _this.roomsTemp = rooms;
        });
        Promise.all([cinemaPromise, roomsPromise]).then(function () {
            _this.currentCinema.rooms = _this.roomsTemp;
            setTimeout(function () {
                var mapDiv = document.getElementById('cinema-map-' + _this.currentCinema.id);
                var cinemaPosition = { lat: _this.currentCinema.gps_lat, lng: _this.currentCinema.gps_lng };
                var map = new google.maps.Map(mapDiv, {
                    zoom: 14,
                    center: cinemaPosition,
                    mapType: 'roadmap',
                    styles: settings_1.googleMapsStyle
                });
                var marker = new google.maps.Marker({
                    position: cinemaPosition,
                    map: map
                });
            }, 20);
        }).catch(function (error) {
            console.log('error when getting cinema:' + error);
        });
    };
    CinemaComponent.prototype.ngOnInit = function () {
        this.getCinemaFromService();
    };
    return CinemaComponent;
}());
CinemaComponent = __decorate([
    core_1.Component({
        selector: 'cinema',
        templateUrl: './cinema.component.html'
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute, cinema_service_1.CinemaService])
], CinemaComponent);
exports.CinemaComponent = CinemaComponent;
//# sourceMappingURL=cinema.component.js.map