import { Component, OnInit } from '@angular/core';
import { ActivatedRoute }   from '@angular/router';

import { Cinema, emptyCinema } from '../../common/cinema.interface';
import { Room } from '../../common/room.interface';

import { CinemaService } from '../../services/cinema.service';
import { googleMapsStyle } from '../../common/settings';

declare var google: any;

@Component({
  selector: 'cinema',
  templateUrl: './cinema.component.html'
})

export class CinemaComponent implements OnInit{
	id : number;
	currentCinema: Cinema;
	roomsTemp: Room[];
	hideShowCinemasBar: boolean = true;

	constructor(private route: ActivatedRoute, private cinService: CinemaService) {
		this.currentCinema = emptyCinema;
		this.id = route.snapshot.params['cinema_id'];
		this.roomsTemp = [];
	}

	getCinemaFromService() : void {
		let cinemaPromise = this.cinService.getCinema(this.id)
		.then( (cinema : Cinema) => {
			this.currentCinema = cinema;
		} );
		let roomsPromise = this.cinService.getRoomsByCinemaId(this.id)
		.then( (rooms: Room[]) => {
			this.roomsTemp = rooms;
		});
		Promise.all([cinemaPromise, roomsPromise]).then( () => {
			this.currentCinema.rooms = this.roomsTemp;
			setTimeout(()=>{
				let mapDiv = document.getElementById('cinema-map-' + this.currentCinema.id);
				let cinemaPosition = { lat: this.currentCinema.gps_lat, lng: this.currentCinema.gps_lng };
				let map = new google.maps.Map(mapDiv, {
					zoom: 14,
					center: cinemaPosition,
					mapType : 'roadmap',
					styles : googleMapsStyle
				});
				let marker = new google.maps.Marker({
      				position: cinemaPosition,
      				map: map
        			});
			}, 20);
		}).catch( (error) => {
			console.log('error when getting cinema:' + error);
		});
	}

	ngOnInit(): void {
		this.getCinemaFromService();
  	}

}
