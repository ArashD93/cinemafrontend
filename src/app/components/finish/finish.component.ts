import { Component,Input, ViewChild, OnInit } from '@angular/core'; 
import { ReservationMethod } from '../../common/ticketBuying.interface';

@Component({
	selector: 'finish',
	templateUrl: './finish.component.html'
})

export class FinishComponent implements OnInit {
	@ViewChild('form') formElement : any;
	@Input() confirmationIds : number[];
	@Input() soCounter : number;
	@Input() soAmount : number;
	@Input() reservation : ReservationMethod;
	done: boolean = true;

	ngOnInit () : void {
		setTimeout(()=>{
				if (this.formElement.nativeElement) {
					console.log(this.soAmount, this.soCounter);
					this.formElement.nativeElement.submit();
				}
			}, 50);
	}
}