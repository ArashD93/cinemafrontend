"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var ticketBuying_interface_1 = require("../../common/ticketBuying.interface");
var FinishComponent = (function () {
    function FinishComponent() {
        this.done = true;
    }
    FinishComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.formElement.nativeElement) {
                console.log(_this.soAmount, _this.soCounter);
                _this.formElement.nativeElement.submit();
            }
        }, 50);
    };
    return FinishComponent;
}());
__decorate([
    core_1.ViewChild('form'),
    __metadata("design:type", Object)
], FinishComponent.prototype, "formElement", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], FinishComponent.prototype, "confirmationIds", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], FinishComponent.prototype, "soCounter", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], FinishComponent.prototype, "soAmount", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], FinishComponent.prototype, "reservation", void 0);
FinishComponent = __decorate([
    core_1.Component({
        selector: 'finish',
        templateUrl: './finish.component.html'
    })
], FinishComponent);
exports.FinishComponent = FinishComponent;
//# sourceMappingURL=finish.component.js.map