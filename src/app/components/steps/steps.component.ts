import { Component, Input } from '@angular/core';
import { BookingComponent } from '../booking/booking.component';
import { Cinema } from '../../common/cinema.interface';
import { Movie } from '../../common/movie.interface';
import { Room } from '../../common/room.interface';

@Component({
  selector: 'steps',
  templateUrl: './steps.component.html'
})

export class StepsComponent {

	@Input() localStep: number;
	@Input() cinema: Cinema;
	@Input() movie: Movie;
	@Input() room: Room;
	
}