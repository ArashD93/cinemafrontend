import { Component, Output, Input, EventEmitter } from '@angular/core';
import { ReservationMethod } from '../../common/ticketBuying.interface';

@Component({
  selector: 'reservation_booking',
  templateUrl: './reservation_booking.component.html'
})

export class ReservationBookingComponent {

	@Input() user: any;
	@Output() complete = new EventEmitter<any>();
	@Output() reservationExport = new EventEmitter<ReservationMethod>();

	visible: boolean;
	termsCheck: boolean = false;
	scrollLeft: boolean = false;
	ticketSelectionComplete: boolean = false;
	reserve: boolean = false;
	buy: boolean = false;


	constructor(){}

	dataChecker() : boolean {
		return this.visible != null && this.termsCheck != false;
	}

	stepsIncrement() : void {
		this.complete.emit();
	}

	selectReserve() : void {
		if (!this.reserve){
			this.reserve = true;
			this.buy = false;
		}
	}

	selectBuy() : void {
		if (!this.buy) {
			this.buy = true;
			this.reserve = false;
		}
	}

	reservationChoosed() : void {
		if (this.reserve) this.reservationExport.emit(ReservationMethod.reservation);
		if (this.buy) this.reservationExport.emit(ReservationMethod.buy);
	}

	ticketSelectionSet(){
		this.scrollLeft = true;
		setInterval(()=>{
			this.ticketSelectionComplete = true;
			this.scrollLeft = false;
		}, 600);
		this.stepsIncrement();
	}
}

	
