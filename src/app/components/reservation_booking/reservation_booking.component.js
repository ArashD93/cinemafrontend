"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var ticketBuying_interface_1 = require("../../common/ticketBuying.interface");
var ReservationBookingComponent = (function () {
    function ReservationBookingComponent() {
        this.complete = new core_1.EventEmitter();
        this.reservationExport = new core_1.EventEmitter();
        this.termsCheck = false;
        this.scrollLeft = false;
        this.ticketSelectionComplete = false;
        this.reserve = false;
        this.buy = false;
    }
    ReservationBookingComponent.prototype.dataChecker = function () {
        return this.visible != null && this.termsCheck != false;
    };
    ReservationBookingComponent.prototype.stepsIncrement = function () {
        this.complete.emit();
    };
    ReservationBookingComponent.prototype.selectReserve = function () {
        if (!this.reserve) {
            this.reserve = true;
            this.buy = false;
        }
    };
    ReservationBookingComponent.prototype.selectBuy = function () {
        if (!this.buy) {
            this.buy = true;
            this.reserve = false;
        }
    };
    ReservationBookingComponent.prototype.reservationChoosed = function () {
        if (this.reserve)
            this.reservationExport.emit(ticketBuying_interface_1.ReservationMethod.reservation);
        if (this.buy)
            this.reservationExport.emit(ticketBuying_interface_1.ReservationMethod.buy);
    };
    ReservationBookingComponent.prototype.ticketSelectionSet = function () {
        var _this = this;
        this.scrollLeft = true;
        setInterval(function () {
            _this.ticketSelectionComplete = true;
            _this.scrollLeft = false;
        }, 600);
        this.stepsIncrement();
    };
    return ReservationBookingComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], ReservationBookingComponent.prototype, "user", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ReservationBookingComponent.prototype, "complete", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ReservationBookingComponent.prototype, "reservationExport", void 0);
ReservationBookingComponent = __decorate([
    core_1.Component({
        selector: 'reservation_booking',
        templateUrl: './reservation_booking.component.html'
    }),
    __metadata("design:paramtypes", [])
], ReservationBookingComponent);
exports.ReservationBookingComponent = ReservationBookingComponent;
//# sourceMappingURL=reservation_booking.component.js.map