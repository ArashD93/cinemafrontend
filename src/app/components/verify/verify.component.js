"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var user_service_1 = require("../../services/user.service");
var data_service_1 = require("../../services/data.service");
var VerifyComponent = (function () {
    function VerifyComponent(userService, actRoute, router, dataService) {
        this.userService = userService;
        this.actRoute = actRoute;
        this.router = router;
        this.dataService = dataService;
    }
    VerifyComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userId = this.actRoute.snapshot.params['userid'];
        var hash = this.actRoute.snapshot.params['hash'];
        this.userService.verifyUser(userId, hash)
            .then(function (user) {
            _this.router.navigate(['/']);
            _this.dataService.signedInUser = user;
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    return VerifyComponent;
}());
VerifyComponent = __decorate([
    core_1.Component({
        selector: 'verify',
        template: ''
    }),
    __metadata("design:paramtypes", [user_service_1.UserService,
        router_1.ActivatedRoute,
        router_1.Router,
        data_service_1.DataService])
], VerifyComponent);
exports.VerifyComponent = VerifyComponent;
//# sourceMappingURL=verify.component.js.map