import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from '../../services/user.service';
import { DataService } from '../../services/data.service';
import { User, emptyUser } from '../../common/user.interface';

@Component({
	selector: 'verify',
	template: ''
})

export class VerifyComponent implements OnInit {

	constructor(private userService: UserService,
				private actRoute: ActivatedRoute, 
				private router : Router, 
				private dataService: DataService) { }

	ngOnInit() : void {
		let userId = this.actRoute.snapshot.params['userid'];
		let hash = this.actRoute.snapshot.params['hash'];
		this.userService.verifyUser(userId, hash)
		.then((user: User)=>{
			this.router.navigate(['/']);
			this.dataService.signedInUser = user;
		})
		.catch(response=>{
			console.log(response);
		});
	}
}