import { Component,Input,Output,EventEmitter } from '@angular/core';
import { Ticket } from '../../common/ticket.interface';
import { Cinema, emptyCinema } from '../../common/cinema.interface';
import { TicketBuying } from '../../common/ticketBuying.interface';
import { ReservationService } from '../../services/reservation.service';
import { Order,emptyOrder } from '../../common/order.interface';
import { Movie } from '../../common/movie.interface';
import { ReservationMethod } from '../../common/ticketBuying.interface';

@Component({
	selector: 'confirmation',
	templateUrl: './confirmation.component.html'
})

export class ConfirmationComponent {
	@Output() ticketAmountX = new EventEmitter<number>();
	@Output() ticketCounter = new EventEmitter<number>();
	@Output() completeConfi = new EventEmitter<any>();
	@Output() confiBack = new EventEmitter<any>();
	@Output() responseObj = new EventEmitter<number[]>();

	ticketDeatils : TicketBuying;
	confirmationNumber : number ;
	done : boolean = false;
	backClicked : boolean = false;
	orderEmail : Order;

	constructor(private reservationService: ReservationService){
		this.orderEmail = emptyOrder;
	}

	orderConfiMail(): void {
		this.orderEmail.seats = this.ticketDatas.seats;
		this.orderEmail.base_ticket_price = this.cinemaPrise.base_ticket_price;
		this.orderEmail.title = this.movie.title;
	}

	stepsInceremt(){
		this.done = true;
		this.completeConfi.emit();
		setTimeout(()=>{
			this.completeConfi.emit();
		},500);
	}

	stepsDescrese(){
		this.confiBack.emit();
		this.backClicked = true;
	}

	@Input() ticketConfi : Ticket[];
	@Input() cinemaPrise : Cinema;
	@Input() ticketDatas: TicketBuying;
	@Input() movie: Movie;
	@Input() reservation: ReservationMethod;

	totalPrice(): void {
		this.ticketAmountX.emit(this.cinemaPrise.base_ticket_price);
	}

	totalPriceBase() : number {
		return (this.cinemaPrise.base_ticket_price)*(this.ticketConfi.length);
	}

	ticketCount(): void {
		this.ticketCounter.emit(this.ticketConfi.length);
	}

	getConfirmationData() : void {
		this.ticketCount();
		this.totalPrice();
		this.orderConfiMail();
		this.reservationService.setConfirmation(this.ticketDatas, this.orderEmail)
		.then( (responseNumber : number[]) => {
			this.responseObj.emit(responseNumber);
			this.stepsInceremt();
		});
	}

	setPaymethode() : boolean {
		if(this.reservation == 1){
			return false;
		}else{
			return true;
		}
	}
}

