"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var reservation_service_1 = require("../../services/reservation.service");
var order_interface_1 = require("../../common/order.interface");
var ticketBuying_interface_1 = require("../../common/ticketBuying.interface");
var ConfirmationComponent = (function () {
    function ConfirmationComponent(reservationService) {
        this.reservationService = reservationService;
        this.ticketAmountX = new core_1.EventEmitter();
        this.ticketCounter = new core_1.EventEmitter();
        this.completeConfi = new core_1.EventEmitter();
        this.confiBack = new core_1.EventEmitter();
        this.responseObj = new core_1.EventEmitter();
        this.done = false;
        this.backClicked = false;
        this.orderEmail = order_interface_1.emptyOrder;
    }
    ConfirmationComponent.prototype.orderConfiMail = function () {
        this.orderEmail.seats = this.ticketDatas.seats;
        this.orderEmail.base_ticket_price = this.cinemaPrise.base_ticket_price;
        this.orderEmail.title = this.movie.title;
    };
    ConfirmationComponent.prototype.stepsInceremt = function () {
        var _this = this;
        this.done = true;
        this.completeConfi.emit();
        setTimeout(function () {
            _this.completeConfi.emit();
        }, 500);
    };
    ConfirmationComponent.prototype.stepsDescrese = function () {
        this.confiBack.emit();
        this.backClicked = true;
    };
    ConfirmationComponent.prototype.totalPrice = function () {
        this.ticketAmountX.emit(this.cinemaPrise.base_ticket_price);
    };
    ConfirmationComponent.prototype.totalPriceBase = function () {
        return (this.cinemaPrise.base_ticket_price) * (this.ticketConfi.length);
    };
    ConfirmationComponent.prototype.ticketCount = function () {
        this.ticketCounter.emit(this.ticketConfi.length);
    };
    ConfirmationComponent.prototype.getConfirmationData = function () {
        var _this = this;
        this.ticketCount();
        this.totalPrice();
        this.orderConfiMail();
        this.reservationService.setConfirmation(this.ticketDatas, this.orderEmail)
            .then(function (responseNumber) {
            _this.responseObj.emit(responseNumber);
            _this.stepsInceremt();
        });
    };
    ConfirmationComponent.prototype.setPaymethode = function () {
        if (this.reservation == 1) {
            return false;
        }
        else {
            return true;
        }
    };
    return ConfirmationComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ConfirmationComponent.prototype, "ticketAmountX", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ConfirmationComponent.prototype, "ticketCounter", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ConfirmationComponent.prototype, "completeConfi", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ConfirmationComponent.prototype, "confiBack", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ConfirmationComponent.prototype, "responseObj", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], ConfirmationComponent.prototype, "ticketConfi", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], ConfirmationComponent.prototype, "cinemaPrise", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], ConfirmationComponent.prototype, "ticketDatas", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], ConfirmationComponent.prototype, "movie", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], ConfirmationComponent.prototype, "reservation", void 0);
ConfirmationComponent = __decorate([
    core_1.Component({
        selector: 'confirmation',
        templateUrl: './confirmation.component.html'
    }),
    __metadata("design:paramtypes", [reservation_service_1.ReservationService])
], ConfirmationComponent);
exports.ConfirmationComponent = ConfirmationComponent;
//# sourceMappingURL=confirmation.component.js.map