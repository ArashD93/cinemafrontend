"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var cinema_interface_1 = require("../../common/cinema.interface");
var settings_1 = require("../../common/settings");
var movie_service_1 = require("../../services/movie.service");
var show_service_1 = require("../../services/show.service");
var cinema_service_1 = require("../../services/cinema.service");
var jquery_service_1 = require("../../services/jquery.service");
var ShowsComponent = (function () {
    function ShowsComponent(showService, movieService, cinService, jqueryService) {
        this.showService = showService;
        this.movieService = movieService;
        this.cinService = cinService;
        this.jqueryService = jqueryService;
        this.shows = [];
        this.movies = [];
        this.chosenCinema = cinema_interface_1.emptyCinema;
        this.cinemaForTimeTable = [];
        this.cinema = [];
        this.hours = [];
        for (var i = settings_1.openingHour; i <= settings_1.closingHour; i++)
            this.hours.push(i);
        this.showDate = Date.now();
    }
    ShowsComponent.prototype.getShowsForCinema = function () {
        var _this = this;
        console.log("OK");
        var showsPromise;
        if (this.chosenCinema.id) {
            showsPromise = this.showService.getShowsByDateAndCinema(new Date(this.showDate), this.chosenCinema.id);
        }
        else {
            showsPromise = this.showService.getShowsByDateAndCinema(new Date(this.showDate));
        }
        if (this.movies.length > 0) {
            showsPromise.then(function (shows) {
                _this.shows = shows;
                for (var j = 0; j < _this.movies.length; j++) {
                    _this.movies[j].shows = [];
                    for (var i = 0; i < _this.shows.length; i++) {
                        if (_this.shows[i].movie_id == _this.movies[j].id) {
                            _this.movies[j].shows.push(_this.shows[i]);
                        }
                    }
                }
            })
                .catch(function (error) {
                console.log(error);
            });
        }
        else {
            showsPromise.then(function (shows) {
                _this.shows = shows;
            });
            var moviesPromise = this.movieService.getMovies()
                .then(function (movies) {
                _this.movies = movies;
            });
            Promise.all([showsPromise, moviesPromise]).then(function () {
                for (var j = 0; j < _this.movies.length; j++) {
                    _this.movies[j].shows = [];
                    for (var i = 0; i < _this.shows.length; i++) {
                        if (_this.shows[i].movie_id == _this.movies[j].id) {
                            _this.movies[j].shows.push(_this.shows[i]);
                        }
                    }
                }
            }).catch(function (error) {
                console.log('error when getting shows: ' + error);
            });
        }
    };
    ShowsComponent.prototype.getCinemasforTimetableService = function () {
        var _this = this;
        this.cinService.getCinemas()
            .then(function (cinemaForTimeTable) {
            _this.cinemaForTimeTable = cinemaForTimeTable;
        })
            .catch(function (response) {
            console.log(response);
        });
    };
    ShowsComponent.prototype.filterMovies = function () {
        var ms = [];
        for (var i = 0; i < this.movies.length; i++) {
            if (this.movies[i].shows && this.movies[i].shows.length) {
                ms.push(this.movies[i]);
            }
        }
        return ms;
    };
    ShowsComponent.prototype.ngOnInit = function () {
        this.jqueryService.scrollToPositionTop(0);
        this.getShowsForCinema();
        this.getCinemasforTimetableService();
    };
    ShowsComponent.prototype.clickedCinema = function (c) {
        this.chosenCinema = c ? c : cinema_interface_1.emptyCinema;
        this.getShowsForCinema();
    };
    ShowsComponent.prototype.getMovieShowsInHour = function (movie, hour) {
        for (var i = 0; i < movie.shows.length; i++) {
            if (movie.shows[i].timestamp !== null) {
                var hours = new Date(movie.shows[i].timestamp).getHours();
                var minutes = new Date(movie.shows[i].timestamp).getMinutes();
                if (hour == hours) {
                    if (minutes < 10) {
                        return { time: hours + ":0" + minutes, showId: movie.shows[i].id };
                    }
                    else {
                        return { time: hours + ":" + minutes, showId: movie.shows[i].id };
                    }
                }
            }
            else {
                return {};
            }
        }
    };
    return ShowsComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], ShowsComponent.prototype, "chosenCinema", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], ShowsComponent.prototype, "hide", void 0);
ShowsComponent = __decorate([
    core_1.Component({
        selector: 'shows',
        templateUrl: './shows.component.html'
    }),
    __metadata("design:paramtypes", [show_service_1.ShowService,
        movie_service_1.MovieService,
        cinema_service_1.CinemaService,
        jquery_service_1.JqueryService])
], ShowsComponent);
exports.ShowsComponent = ShowsComponent;
//# sourceMappingURL=shows.component.js.map