import { Component, OnInit, Input } from '@angular/core';

import { Movie } from '../../common/movie.interface';
import { Show } from '../../common/show.interface';
import { Cinema, emptyCinema } from '../../common/cinema.interface';
import { openingHour, closingHour } from '../../common/settings'
import { MovieService } from '../../services/movie.service';
import { ShowService } from '../../services/show.service';
import { CinemaService } from '../../services/cinema.service';
import { JqueryService } from '../../services/jquery.service';

@Component({
  selector: 'shows',
  templateUrl: './shows.component.html'
})
export class ShowsComponent implements OnInit{
	shows: Show[];
	selectedShow: Show;
	movies : Movie[];
	@Input()
	chosenCinema : Cinema;
	@Input()
	hide : boolean;
	empty : boolean;
	cinemaForTimeTable : Cinema[];
	cinema : Cinema[];
	hours : number[]; 
	showDate : number;
	


	constructor(private showService: ShowService,
			    private movieService: MovieService,
			    private cinService : CinemaService,
			    private jqueryService: JqueryService) {
		this.shows = [];
		this.movies = [];
		this.chosenCinema = emptyCinema;
		this.cinemaForTimeTable = [];
		this.cinema = [];
		this.hours = [];
		for (let i=openingHour; i<=closingHour; i++) this.hours.push(i);
		this.showDate = Date.now();	
	}	

	getShowsForCinema() : void {
		console.log("OK");
		var showsPromise;
		if (this.chosenCinema.id) {
			showsPromise = this.showService.getShowsByDateAndCinema(new Date(this.showDate), this.chosenCinema.id);
		} else {
			showsPromise = this.showService.getShowsByDateAndCinema(new Date(this.showDate));
		}
		if(this.movies.length > 0) {
			showsPromise.then((shows : Show[]) => {
				this.shows = shows;
				for (var j = 0; j < this.movies.length; j++) {
					this.movies[j].shows = [];
					for (var i = 0; i < this.shows.length; i++) {
						if (this.shows[i].movie_id == this.movies[j].id) {
							this.movies[j].shows.push(this.shows[i]);
						}
					}
				}
			})
			.catch((error) => {
				console.log(error);
			}) ;
		} else {
			showsPromise.then((shows : Show[]) => {
				this.shows = shows;
			} );
			let moviesPromise = this.movieService.getMovies()
			.then( (movies : Movie[])=> {
				this.movies = movies;
			} );

			Promise.all([showsPromise, moviesPromise]).then( () => {
				for (var j = 0; j < this.movies.length; j++) {
					this.movies[j].shows = [];
					for (var i = 0; i < this.shows.length; i++) {
						if (this.shows[i].movie_id == this.movies[j].id) {
							this.movies[j].shows.push(this.shows[i]);
						}
					}
				}
			} ). catch( (error) => {
				console.log( 'error when getting shows: ' + error);
			} );
		}
	}

	getCinemasforTimetableService () : void {
		this.cinService.getCinemas()
		.then((cinemaForTimeTable : Cinema[]) => {
			this.cinemaForTimeTable = cinemaForTimeTable;
		} )
		.catch(response => {
			console.log(response);
		})
	}

	private filterMovies ( ) : Movie[] {
		let ms = [];
		for (var i = 0; i < this.movies.length; i++) {
			if (this.movies[i].shows && this.movies[i].shows.length) {
				ms.push(this.movies[i]);
			}
		}
		return ms;
	}

	ngOnInit() : void {
		this.jqueryService.scrollToPositionTop(0);
		this.getShowsForCinema();
		this.getCinemasforTimetableService();
	}

	clickedCinema( c : Cinema ) : void {
		this.chosenCinema = c ? c : emptyCinema;
		this.getShowsForCinema();

	}

	getMovieShowsInHour( movie : Movie, hour : number ) : any {
		for(var i = 0; i < movie.shows.length; i++) {
			if (movie.shows[i].timestamp !== null) {
				var hours = new Date(movie.shows[i].timestamp).getHours();
				var minutes = new Date(movie.shows[i].timestamp).getMinutes();
				if (hour == hours) {
					if (minutes < 10) {
						return { time : hours + ":0" + minutes, showId : movie.shows[i].id };
					} else {
						return { time : hours + ":" + minutes, showId : movie.shows[i].id };
					}
				}
			} else {
				return {};
			}
		}
	}
	
}
