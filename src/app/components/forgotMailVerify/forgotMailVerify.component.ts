import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from '../../services/user.service';
import { DataService } from '../../services/data.service';
import { User, emptyUser } from '../../common/user.interface';

@Component({
	selector: 'forgotmailverify',
	template: ''
})

export class ForgotMailVerifyComponent implements OnInit {

	constructor(private userService: UserService,
				private actRoute: ActivatedRoute, 
				private router: Router,
				private dataService: DataService) { }

	ngOnInit() : void {
		let userId = this.actRoute.snapshot.params['userid'];
		let hash = this.actRoute.snapshot.params['hash'];
		this.userService.forgotMailVerify(userId, hash)
		.then((user: User)=>{
			this.router.navigate(['/']);
			this.dataService.newPassAsker = user;
		})
		.catch(response=>{
			console.log(response);
		});
	}
}